<div class="page-header-menu">
    <div class="container">
<!--         BEGIN HEADER SEARCH BOX 
        <form class="search-form" action="page_general_search.html" method="GET">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" name="query">
                <span class="input-group-btn">
                    <a href="javascript:;" class="btn submit">
                        <i class="icon-magnifier"></i>
                    </a>
                </span>
            </div>
        </form>
         END HEADER SEARCH BOX -->
        <!-- BEGIN MEGA MENU -->
        <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
        <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
        <?php 
        $moduleId=trim(Yii::app()->controller->module->id);
        if(isset($moduleId)){
            if($moduleId=='admin'){
                $activeDashBoard='active';
            }else{
                $activeDashBoard='';
            }if($moduleId=='admin/ourTeam'){
                $activeOurTeam='active';
            }else{
                $activeOurTeam='';
            }if($moduleId=='admin/slider'){
                $activesiteSettings='active';
                $activeslider='active';
            }else{
                $activeslider='';
                $activesiteSettings='';
            }if($moduleId=='admin/siteSettings'){
                $activesiteSettings='active';
            }else{
                $activesiteSettings='';
            }if($moduleId=='admin/menu'){
                $activesiteSettings='active';
            }else{
                $activemenu='';
                $activesiteSettings='';
            }if($moduleId=='admin/page'){
                $activepage='active';
                //$activesiteSettings='active';
            }else{
                $activepage='';
                //$activesiteSettings='';   activeshipping
            }if($moduleId=='admin/partner'){
                $activepartner='active';
            }else{
                $activepartner='';
            }if($moduleId=='admin/service'){
                $activeservice='active';
            }else{
                $activeservice='';
            }if($moduleId=='admin/shipping'){
                $activeshipping='active';
            }else{
                $activeshipping='';
            }if($moduleId=='admin/newsEvents'){
                $activelatestNews='active';
            }else{
                $activelatestNews='';
            }if($moduleId=='admin/newsLetter'){
                $activenewsLetter='active';
                $activesiteSettings='active';
            }else{
                $activenewsLetter='';
                $activesiteSettings='';
            }
            //echo $activesiteSettings;
            //exit;
        }
        ?>
        <div class="hor-menu ">
            <ul class="nav navbar-nav">
                <li class="<?=$activeDashBoard?>">
                    <a href="<?= Yii::app()->createUrl('//admin/dashboard/index') ?>"><i class="fa fa-dashboard"></i> Dashboard
                        <span class="arrow"></span>
                    </a>
                </li>
                <li class="menu-dropdown classic-menu-dropdown <?=$activepartner?>">
                    <a href="javascript:;"><i class="fa fa-user"></i> Parnter
                        <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">
                        <li class="">
                            <a href="<?= Yii::app()->createUrl('//admin/partner?Partner[category_id]=All&')?>" class="nav-link">
                                <i class="fa fa-users"></i> All Partner
                            </a>
                        </li>
                        <li class="">
                            <a href="<?= Yii::app()->createUrl('//admin/partner/partnerCategory')?>" class="nav-link  ">
                                <i class="fa fa-sitemap"></i> Partner Category </a>
                        </li>
                    </ul>
                </li>
                <li class="<?=$activeservice?>">
                    <a href="<?= Yii::app()->createUrl('//admin/service') ?>"><i class="fa fa-bars"></i> Service
                        <span class="arrow"></span>
                    </a>
                </li>
                <li class="menu-dropdown classic-menu-dropdown <?=$activeshipping?>">
                    <a href="javascript:;"><i class="fa fa-user"></i> Shipping
                        <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">
                        <li class="">   
                            <a href="<?= Yii::app()->createUrl('//admin/shipping')?>" class="nav-link">
                                <i class="fa fa-users"></i> All Shipping
                            </a>
                        </li>
                        <li class="">
                            <a href="<?= Yii::app()->createUrl('//admin/shipping/allShipment')?>" class="nav-link">
                                <i class="fa fa-users"></i> Booking
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="<?=$activelatestNews?>">
                    <a href="<?= Yii::app()->createUrl('//admin/newsEvents') ?>"><i class="fa fa-newspaper-o"></i> Latest News
                        <span class="arrow"></span>
                    </a>
                </li>
                <li class="menu-dropdown classic-menu-dropdown <?=$activeOurTeam?>">
                    <a href="javascript:;"><i class="fa fa-users"></i> Our Team
                        <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">
                        <li class="">
                            <a href="<?= Yii::app()->createUrl('//admin/ourTeam?OurTeam[category_id]=All&')?>" class="nav-link">
                                <i class="fa fa-user"></i> Team Member
                            </a>
                        </li>
                        <li class="">
                            <a href="<?= Yii::app()->createUrl('//admin/ourTeam/teamCategory')?>" class="nav-link  ">
                                <i class="fa fa-sitemap"></i> Team Category </a>
                        </li>
                    </ul>
                </li>

                            <li class="<?=$activepage?>">
                                <a href="<?= Yii::app()->createUrl('//admin/page') ?>" class="nav-link  ">
                                    <i class="fa fa-file-text-o"></i> Page
                                    
                                </a>
                            </li>

                <li class="menu-dropdown classic-menu-dropdown <?=$activesiteSettings?>">
                                            <a href="javascript:;"><i class="fa fa-cogs"></i> Settings
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-left">
                                                <li class="">
                                                    <a href="<?= Yii::app()->createUrl('//admin/siteSettings') ?>" class="nav-link">
                                                        <i class="fa fa-cog"></i> Site Settings
                                                    </a>
                                                </li>
                                                <li class="">
                                                    <a href="<?= Yii::app()->createUrl('//admin/menu?Menu[position]=Top Menu&') ?>" class="nav-link  ">
                                                        <i class="fa fa-sitemap"></i> Menu </a>
                                                </li>
                                                <!-- <li class="">
                                                    <a href="<?//= Yii::app()->createUrl('//admin/page') ?>" class="nav-link  ">
                                                        <i class="fa fa-file-text-o"></i> Page
                                                        
                                                    </a>
                                                </li> -->

                                                <li class="<?=$activeslider?>">
                                                    <a href="<?= Yii::app()->createUrl('//admin/slider?Slideshow[slider_type]=Image Slider&') ?>"><i class="fa fa-sliders"></i> Slider
                                                        <span class="arrow"></span>
                                                    </a>
                                                </li>

                                                <!-- <li class="<?=$activenewsLetter?>">
                                                    <a href="<?//= Yii::app()->createUrl('//admin/newsLetter') ?>"><i class="fa fa-envelope"></i> News Letter
                                                        <span class="arrow"></span>
                                                    </a>
                                                </li> -->
                                            </ul>
                                        </li>
            </ul>
        </div>
        <!-- END MEGA MENU -->
    </div>
</div>