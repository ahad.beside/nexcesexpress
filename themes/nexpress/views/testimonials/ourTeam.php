<section class="page-info">
  <div class="auto-container clearfix">
      <div class="col-md-6">
        <div class="pull-left">
            <h3><?= $data['categoryName']?></h3>
        </div>
    </div>
    <div class="col-md-6">
        <div class="pull-right">
          <ul class="bread-crumb clearfix">
              <li><a href="<?= Yii::app()->homeUrl;?>">Home</a></li>
        
              <li><?= $data['categoryName']?></li>
          </ul>
        </div>
  </div>

  </div>
</section>

<div class="container">
  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <div class="sidebar">
      <div class="widget sidebar-widget styled-nav">
        <nav class="nav-outer">
          <ul>
            <li <?php if($data['categoryName']=='All Team Member'){?> class="active"<?php } ?> >
              <a class="hvr-shutter-in-horizontal" href="<?= Yii::app()->createUrl('//ourTeam')?>">All </a>
            </li>
            <?php 
            if(count($data['teamCategory'])>0){
            foreach($data['teamCategory'] as $teamCategory):
            ?>

              <li <?php if($data['categoryName']==$teamCategory->name){?> class="active"<?php } ?>>
                <a class="hvr-shutter-in-horizontal" href="<?= TeamCategory::model()->makeLink($teamCategory->id);?>">
                    <?= $teamCategory->name;?>
                </a>
              </li>
            <?php endforeach;}?>
          </ul>
            </nav>
        </div>
    </div>
  </div>

  <div class="content-side col-lg-9 col-md-9 col-sm-12 col-xs-12 client_content" style="margin-bottom: 30px;">
    <?php if(count($data['teamCategoryAll'])>0){
      foreach($data['teamCategoryAll'] as $rowCategory):
      ?>
    <div class="sec-title" style="margin-bottom: 15px; background: #f2f2f2; padding-left: 12px;">
        <h2><?= $rowCategory->name?></h2>
      </div>
    <?php 
    $data['teamList']=OurTeam::model()->findAll("category_id=".$rowCategory->id." and status=1");
    if(count($data['teamList'])){

      ?>
    <div class="row">
      <?php 
      foreach($data['teamList'] as $rowMember):
      ?>
      <div class="col-sm-6 col-md-3 col-lg-3">

        <a href="<?= OurTeam::model()->makeLink($rowMember->id)?>">
          <div class="team-box hover-box">
            <div class="team-head" style="max-height: 230px;">
              <img src="<?= Yii::app()->easycode->showOriginalImage($rowMember->image,'/team/')?>" class="img-responsive mauto" alt="">
            </div>
            <div class="team-content">
              <h3><?= $rowMember->name?></h3>
              <b style="color: #666; font-size: 12px; font-style: italic;"><?= $rowMember->designation?></b>
              
              <!-- end team-social --> 
            </div>
          <!-- end team-content --> 
          </div>
        </a>
        <!-- end team-box --> 

      </div>
      <?php endforeach;?>
    </div>
<?php } else{?>
<div class="alert-warning alert">
  Currently No <?= $rowCategory->name?> Avilable!!!
</div>
<?php }?>
<?php endforeach;} ?>
  </div>
</div>
