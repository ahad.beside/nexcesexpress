
<div class="teamContents" style="max-height: 384px; overflow: hidden;">
  <img style="width: 100%;"  src="<?= Yii::app()->easycode->showOriginalImage($model->cover_photo,'/team/') ?>">
</div>


<section class="page-info">
            <div class="auto-container clearfix">
                <div class="col-md-6">
                  <div class="pull-left">
                      <h3>Our Team</h3>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="pull-right">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/hadee/">Home</a></li>
                        <li><a href="<?=$data['catUrl']?>"><?=$data['categoryName']?></a></li>
                  
                        <li><?=$model->name?></li>
                    </ul>
                  </div>
            </div>

            </div>
        </section>

<div class="container mid-content" style="margin-bottom: 40px;">
      
<div class="row">

<div class="content-side col-lg-9 col-md-9 col-sm-12 col-xs-12 client_content" style="margin-bottom: 30px;">
    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
          
            <div class="team_content">
              <h1><?=$model->name?></h1>
              <h4><?= $model->designation?></h4>
              <hr class="large">

<div class="footersec" style="margin-bottom: 20px;">
<div class="social">
            <ul>
              <?php if($model->fb_url!=''){?>
              <li class="facebook-icon"> <a target="_blank" href="<?=$model->fb_url?>" class="facebook"> <span class="fa fa-facebook"></span> </a> </li>
              <?php }?>
              <?php if($model->twitter_url!=''){?>
              <li class="twitter-icon"> <a target="_blank" href="<?=$model->twitter_url?>" class="twitter"> <span class="fa fa-twitter"></span> </a> </li>
              <?php }?>
              <?php if($model->linkedin_url!=''){?>
              <li class="linkedin-icon"> <a target="_blank" href="<?=$model->linkedin_url?>" class="linkedin"> <span class="fa fa-linkedin"></span> </a> </li>
              <?php }?>
              <?php if($model->web_url!=''){?>
              <li class="dribbble-icon"> <a target="_blank" href="<?=$model->web_url?>" class="web"> <span class="fa fa-link"></span> </a> </li>
              <?php }?>
            </ul>
          </div>
</div>


              <div class="phone">
                  <p>
                      <strong style="font-weight: bold;">M:</strong> <?=$model->phone?><br>
                      <strong>E:</strong> <a style="text-decoration: none;" href="mailto:<?=$model->email?>"><?=$model->email?></a>
                  </p>
              </div>
 </div>
              <div class="main-content content_list_design">
                <?= $model->details;?>                
              </div>

              
            
          </div>
        </div>
</div>


<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="border-left:1px solid #ddd">
  <div id="sidebar" class="sidebar">
    <?php if(count($data['otherMember'])>0){?>
<div class="widget widget-categories">
<h4 class="w-title"><?= $data['categoryName']?></h4>
<ul class="cat-list">
  <?php foreach($data['otherMember'] as $rowOtherMember):?>
<li><a href="<?= OurTeam::model()->makeLink($rowOtherMember->id)?>"><?= $rowOtherMember->name;?></a><br><i><?= $rowOtherMember->designation;?></i></li>
<?php endforeach;?>
</ul>
</div>
<?php } ?>
</div>
</div>


      
        </div>
      </div>
      
    </div>

    <style>
.mid-content .row {
    display: table;
}

.mid-content .row [class*="col-"] {
    float: none;
    display: table-cell;
    vertical-align: top;
}
      .teamContents{
         background-color: #fff;
          background-position: 0 100%;
          background-repeat: no-repeat!important;
          background-size: cover;
          min-height: 280px;
          overflow: hidden;
      }
.team_content h1 {
    font-size: 3.5rem;
    letter-spacing: -0.19px;
    margin-bottom: 0;
    font-weight: bold;
    color: #333
}
.team_content h4{
  font-size: 3.55556rem;
  letter-spacing: -0.12px;
  line-height: 1.88889rem;
  margin-bottom: .83333rem;
  }
  hr.large {
    width: 6.66667rem;
}
hr {
    background-color: #E30613;
    border: none;
    height: .4rem;
    margin: 1.11111rem 0;
    margin-bottom: 1.11111rem;
    margin-bottom: 1.22222rem;
    width: 4.44444rem;
}
    </style>

