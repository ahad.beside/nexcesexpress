
<!--Page Info-->
<section class="page-info">
  <div class="auto-container clearfix">
    <div class="col-md-6">
      <div class="pull-left">
        <h3><?=$conatct->title?></h3>
      </div>
    </div>
    <div class="col-md-6">
      <div class="pull-right">
        <ul class="bread-crumb clearfix">
          <li><a href="<?= Yii::app()->homeUrl;?>">Home</a></li>
          <?php /*<li><a href="<?= Yii::app()->createUrl('//contact');?>">Contact</a></li>*/?>
          <li><a href="<?= Yii::app()->homeUrl;?>">Contact</a></li>
          <li><?=$conatct->title?></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<!--Page Info end-->

<section class="welcome_section" id="welcome" style="padding-bottom:0px; padding-top:0px;">                
    <div class="container">
        <div class="row">    
            <div class="col-md-12">
                <div class="whitebg contact-section" style="padding: 30px 0px 30px">
       
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-center">
                                <h3 style="font-style: 18px; margin: 0px; padding: 0px; color: #000">Address</h3>
                            </div>

                            <div class="address-box text-left">
                                <div class="col-md-2 col-ms-6">
                                    <i class="fa fa-map-marker"></i>
                                </div>                    

                                <div class="col-md-10 col-ms-6" style="padding-left: 35px;">   
                                    <?= $conatct->description?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="text-center">
                                <h3 style="font-style: 18px; margin: 0px; padding: 0px; color: #000">Site Mape</h3>
                            </div>

                            <div class="row" style="">
                                <div class="col-md-6">
                                    <?= $conatct->site_map?>
                                </div>
                            </div>
                        </div>
                    </div>
                                 
                </div>
            </div>
        </div>
    </div>
</section>

<div style="width: 100%; display: table; background: #f2f2f2;">
    <div class="container contact-form default-form">
            <div class="col-md-12">
                <div style="padding:25px 0px;">
            <?php if(isset($data['msg']) && $data['msg']!=''){?>
            <div class="alert alert-success">
                <?= $data['msg'];?>
            </div>
            <?php } ?>
                        <div class="dividerHeading text-center" style="margin-bottom: 20px;">
                            <h2><span>Enquiry Form</span></h2>
                            If you have any questions or enquiries on our international couriers or hand carry services, feel free to drop us a line by filling in the details below
                        </div>   
               <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'contact-form',
                    'htmlOptions'=>array('class'=>'contact_form'),
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                ));
                ?>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-lg-6 ">
                                        
                                        <?php echo $form->textField($model, 'name', array('class' => 'form-control','maxlength'=>'100','data-msg-required'=>'Please enter your name','placeholder'=>'Your Name')); ?>
                                    </div>
                                    <div class="col-lg-6 ">
                                        <input type="email" id="email" name="ContactForm[email]" class="form-control" maxlength="100" data-msg-email="Please enter a valid email address." data-msg-required="Please enter your email address." value="" placeholder="Your E-mail" >
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    
                                    <div class="col-md-12">
                                        <input type="text" id="subject" name="ContactForm[subject]" class="form-control" maxlength="100" data-msg-required="Please enter the subject." value="" placeholder="Subject">
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <textarea id="message" class="form-control" name="ContactForm[body]" rows="10" cols="50" data-msg-required="Please enter your message." maxlength="5000" placeholder="Message" style="height: 120px!important;"></textarea>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <input type="submit" class="btn btn-success btn-lg" value="Send Message">
                                </div>
                            </div>
                       <?php $this->endWidget(); ?>
                       </div>
            </div>
        </div>
</div>

<style>
.gmap{
  display: inline-block;
    margin-top: -20px;
    position: relative;
    width: 100%;
}
.form-group {
    margin-bottom: 20px;
    position: relative;
    width: 100%;
    display: inline-block;
}
.contactform .form-group {
    margin-bottom: 4px;
}
.contact{
  padding-left:62px;
}
  .contact table tr td{
    border:1px solid #e8e8e9;
  }

  .service_dtls{ text-align: justify; }
      .service_dtls img{width: 450px; float: left; margin-right: 12px;}
</style>
