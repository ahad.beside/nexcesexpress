<section class="page-info">
            <div class="auto-container clearfix">
                <div class="col-md-6">
                  <div class="pull-left">
                      <h3>Latest News</h3>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="pull-right">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/hadee/">Home</a></li>
                  
                        <li>Latest News</li>
                    </ul>
                  </div>
            </div>

            </div>
        </section>

<div class="container" style="margin-bottom: 50px;">
      <div class="sec-title" style="text-align: left; margin-bottom: 20px;">
        <h2><?=$model->name?></h2>
         <div style="font-size: 14px; color: #888"><i class="fa fa-clock-o"></i>  <?=date('d-m-Y',strtotime($model->news_date))?></div>
         </div>
      <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
            

          <div style="margin-bottom: 20px; text-align: center; background: #f2f2f2;"> <?= Yii::app()->easycode->showImage($model->image,500,400,true,true,Yii::app()->params->newsDir)?></div>


            <div class="">
             
              <?= $model->details;?>
            </div>
          </div>
        </div>
        
      </div>
      
    </div>