<?php if(count($data['news'])>0){?>
<section class="page-info">
            <div class="auto-container clearfix">
                <div class="col-md-6">
                  <div class="pull-left">
                      <h3>Latest News</h3>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="pull-right">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/hadee/">Home</a></li>
                        <li>Latest News</li>
                    </ul>
                  </div>
            </div>
            </div>
        </section>
<div class="container">
  <div class="row"> 
    <!-- News 1 -->
    <?php foreach($data['news'] as $rowNews):?>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="single-blog-post" style="border: 1px solid #ddd">
        <div class="img-box" style="height:204px; overflow:hidden;"><img class="img-responsive" src="<?= Yii::app()->easycode->showOriginalImage($rowNews->image,'/news/')?>" alt="">
          <div class="overlay">
            <div class="box">
              <div class="content">
                <ul>
                  <li><a href="<?= NewsEvents::model()->makeLink($rowNews->id)?>"><i class="fa fa-link"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="content-box" style="min-height:179px; overflow: hidden;">
          <div class="date-box">
            <div class="inner">
              <div class="date"> <b><?= date('d',strtotime($rowNews->news_date));?></b> <?= date('M',strtotime($rowNews->news_date));?>  </div>
            </div>
          </div>
          <div class="content"> <a href="<?= NewsEvents::model()->makeLink($rowNews->id)?>">
            <h3><?= $rowNews->name?></h3>
            </a>
            <p><?= strip_tags(Yii::app()->easycode->getExcerpt($rowNews->details,0,100));?></p>
          </div>
        </div>
      </div>
    </div>
  <?php endforeach;?>
    <!-- News 1 -->
  </div>
      
</div>
<?php } ?>