
<?php if(count($model)>0){?>
<!--Page Info-->
<section class="page-info">
  <div class="auto-container clearfix">
    <div class="col-md-6">
      <div class="pull-left">
        <h3> <?=$model->title;?></h3>
      </div>
    </div>
    <div class="col-md-6">
      <div class="pull-right">
        <ul class="bread-crumb clearfix">
          <li><a href="<?= Yii::app()->homeUrl;?>">Home</a></li>
          <li> <?=$model->title;?></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<!--Page Info end-->

<!--welcome container-->
<div class="welcome-sec" style="padding-top: 20px;">
  <div class="container" style="text-align: justify;">
    <div class="row">
      <div class="col-md-12 margin-bott-50 tableclass">
        <div class="dtls"><?=$model->description;?></div>
          <?php if($model->attachment!=''){?> <br>
          <a target="_blank" href="<?= Yii::app()->easycode->showOriginalImage($model->attachment,'/page/')?>" class="btn btn-info"><?= $model->attachment_title?></a>
          <?php } ?>
        </div>
    </div>
  </div>
</div>
<!--welcome container end-->
<?php } ?>

<style type="text/css">
  .tableclass table> tbody > tr:nth-of-type(2n+1) {
    background-color: #f9f9f9;

}
.dtls img{
  margin-right:12px;
  margin-left:12px;
}
.tableclass table > tbody > tr > td, table > tbody > tr > th, table > tfoot > tr > td, table > tfoot > tr > th, table > thead > tr > td, table > thead > tr > th {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 1px solid #ddd;
}
</style>