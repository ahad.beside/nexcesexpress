<?php if(count($data['clientList'])>0){?>

<!--Page Info-->
        <section class="page-info">
            <div class="auto-container clearfix">
                <div class="col-md-6">
                	<div class="pull-left">
                    	<h3><?= $data['categoryName']?></h3>
                	</div>
            	</div>
            	<div class="col-md-6">
                	<div class="pull-right">
                    <ul class="bread-crumb clearfix">
                        <li><a href="<?= Yii::app()->homeUrl;?>">Home</a></li>
      		     		
                        <li><?= $data['categoryName']?></li>
                    </ul>
                	</div>
            </div>

            </div>
        </section>
        <!--Page Info end-->
<div class="container">
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
	<div class="sidebar">
		<div class="widget sidebar-widget styled-nav">
            <nav class="nav-outer">
                <ul>
                    <li<?php if($data['categoryName']=='All Client'){?> class="active"<?php } ?>>
                        <a class="hvr-shutter-in-horizontal" href="<?= Yii::app()->createUrl('//client')?>">
							All
						</a>
                    </li>
                    <?php 
                    if(count($data['clientCategory'])>0){
                    foreach($data['clientCategory'] as $clientCategory):
                    ?>

                    <li<?php if($data['categoryName']==$clientCategory->name){?> class="active"<?php } ?>>
                        <a class="hvr-shutter-in-horizontal" href="<?= ClientCategory::model()->makeLink($clientCategory->id);?>">
                            <?= $clientCategory->name;?>
                        </a>
                    </li>
                <?php endforeach;}?>
                </ul>
            </nav>
        </div>
	</div>	
</div>

<div class="content-side col-lg-9 col-md-9 col-sm-12 col-xs-12 client_content" style="margin-bottom: 30px;">
	
	
  <div class="row">

    <?php foreach($data['clientList'] as $rowClient):?>
    <div class="col-md-4">
    <div class="image-box" style="margin: 0"> <img class="img-responsive" src="<?= Yii::app()->easycode->showOriginalImage($rowClient->image,'/client/')?>" alt=""> </div>
    <div class="client_content_info">
        <div style="margin-top: 10px;"><b><?=$rowClient->name?></b></div>
        <div><?= $rowClient->details;?></div>
    </div>
</div>
    <?php endforeach;?>

  </div>
</div>

  
</div>
<?php } ?>
