
<!--Page Info-->
<section class="page-info">
  <div class="auto-container clearfix">
    <div class="col-md-6">
      <div class="pull-left">
        <h3>
          <?=$model->name?>
        </h3>
      </div>
    </div>
    <div class="col-md-6">
      <div class="pull-right">
        <ul class="bread-crumb clearfix">
          <li><a href="<?= Yii::app()->homeUrl;?>">Home</a></li>
          <li><a href="<?= Yii::app()->createUrl('//service');?>">Service</a></li>
          <li>
            <?=$model->name?>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>

<!--Page Info end-->
<div class="container">
  <div class="row service_dtls">
    <?php if(count($data['allService'])>0){?>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 service-sidebar">
      <div class="sidebar">
        <div class="widget sidebar-widget styled-nav">
          <!-- Tracking -->
          <div class="sidebar-title">
            <form class="navbar-form"  action="<?= Yii::app()->createUrl('//site/search')?>" method="get" accept-charset="utf-8">
              <div class="input-group add-on">      
                <input class="form-control"  placeholder="Search Tracking Number" type="text" name="searchText" value="<?= $_GET['searchText']?>">
                <div class="input-group-btn">
                  <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
              </div>
            </form>
          </div>
          <!-- Tracking -->
          <div class="sidebar-title">
            <h2>All Services</h2>
          </div>

          <nav class="nav-outer">
            <ul>
              <?php foreach($data['allService'] as $rowService):?>
              <li>
              <a class="hvr-shutter-in-horizontal" href="<?= Service::model()->makeLink($rowService->id);?>">
                  <?= $rowService->name;?>
              </a>
              </li>
            <?php endforeach;?>
            </ul>
          </nav>

        </div>
      </div>
    </div>
<?php } ?>

    <div class="content-side col-lg-9 col-md-9 col-sm-12 col-xs-12 client_content content_list_design" style="margin-bottom: 30px;">
      <?= $model->details;?>
    </div>
    
  </div>
</div>
<style>
  .service_dtls{ text-align: justify; }
  .service_dtls img{width: 450px; float: left; margin-right: 12px;}

</style>
