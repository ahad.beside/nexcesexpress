<?php if(count($data['service'])>0){?>

  
<!--Page Info-->
        <section class="page-info">
            <div class="auto-container clearfix">
                <div class="col-md-6">
                	<div class="pull-left">
                    	<h3>Our Service</h3>
                	</div>
            	</div>
            	<div class="col-md-6">
                	<div class="pull-right">
                    <ul class="bread-crumb clearfix">
                        <li><a href="<?= Yii::app()->homeUrl;?>">Home</a></li>
      		     		
                        <li>Our Service</li>
                    </ul>
                	</div>
            </div>

            </div>
        </section>
        <!--Page Info end-->
       


<div class="container service_page">
      
      <div class="row">
      	
        <?php foreach($data['service'] as $rowService):?>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
         
         
          <div class="single-servicebox">
          	<a href="<?= Service::model()->makeLink($rowService->id)?>">
            <div class="servicebox-image">  <img class="img-responsvie" src="<?= Yii::app()->easycode->showOriginalImage($rowService->image,'/service/')?>" />
              
               </div>
            <div class="servicebox-content">
              <h3><?=$rowService->name?></h3>
              <p><?= strip_tags(Yii::app()->easycode->getExcerpt($rowService->details,0,500));?></p>
            </div>
            </a>
          </div>
          
        </div>
      <?php endforeach;?>
        
      </div>
      
    </div>
    <?php } ?>