<div class="container clearfix"> 
  <!--Top Left-->
  <div class="top-left pull-left">
    <ul class="links-nav clearfix">
      <li><a href="#">Nexces Express<span class="theme_color">is a niche player in airfreight handling and foreign trade</span></a></li>
      <li><a href="#"><span class="fa fa-envelope-o"></span>info@nexcesexpress.com</a></li>
    </ul>
  </div>
  
  <!--Top Right-->
  <div class="top-right pull-right">
    <div class="social-links clearfix"> <a href="#"><span class="fa fa-facebook-f"></span></a> <a href="#"><span class="fa fa-google-plus"></span></a> <a href="#"><span class="fa fa-twitter"></span></a> <a href="#"><span class="fa fa-linkedin"></span></a> </div>
  </div>
</div>
