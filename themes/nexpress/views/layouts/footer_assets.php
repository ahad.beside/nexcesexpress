<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".site-header"><span class="icon fa fa-long-arrow-up"></span></div>

<!-- jquery Liabrary -->  
<!-- bootstrap v3.3.6 js --> 
<script src="<?= Yii::app()->theme->baseUrl?>/assets/js/bootstrap.min.js"></script> 
<!-- fancybox js --> 
<script src="<?= Yii::app()->theme->baseUrl?>/assets/js/jquery.fancybox.pack.js"></script> 
<script src="<?= Yii::app()->theme->baseUrl?>/assets/js/jquery.fancybox-media.js"></script> 
<!-- owl.carousel js --> 
<script src="<?= Yii::app()->theme->baseUrl?>/assets/js/owl.js"></script> 
<!-- counter js --> 
<script src="<?= Yii::app()->theme->baseUrl?>/assets/js/jquery.appear.js"></script> 
<script src="<?= Yii::app()->theme->baseUrl?>/assets/js/jquery.countTo.js"></script> 
<!-- mixitup js --> 
<script src="<?= Yii::app()->theme->baseUrl?>/assets/js/mixitup.js"></script> 
<!-- validate js --> 
<script src="<?= Yii::app()->theme->baseUrl?>/assets/js/validate.js"></script> 

<!-- REVOLUTION JS FILES --> 
<script type="text/javascript" src="<?= Yii::app()->theme->baseUrl?>/assets/js/revolution/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="<?= Yii::app()->theme->baseUrl?>/assets/js/revolution/jquery.themepunch.revolution.min.js"></script> 
<script type="text/javascript" src="<?= Yii::app()->theme->baseUrl?>/assets/js/revolution/extensions/revolution.extension.actions.min.js"></script> 
<script type="text/javascript" src="<?= Yii::app()->theme->baseUrl?>/assets/js/revolution/extensions/revolution.extension.carousel.min.js"></script> 
<script type="text/javascript" src="<?= Yii::app()->theme->baseUrl?>/assets/js/revolution/extensions/revolution.extension.kenburn.min.js"></script> 
<script type="text/javascript" src="<?= Yii::app()->theme->baseUrl?>/assets/js/revolution/extensions/revolution.extension.layeranimation.min.js"></script> 
<script type="text/javascript" src="<?= Yii::app()->theme->baseUrl?>/assets/js/revolution/extensions/revolution.extension.migration.min.js"></script> 
<script type="text/javascript" src="<?= Yii::app()->theme->baseUrl?>/assets/js/revolution/extensions/revolution.extension.navigation.min.js"></script> 
<script type="text/javascript" src="<?= Yii::app()->theme->baseUrl?>/assets/js/revolution/extensions/revolution.extension.parallax.min.js"></script> 
<script type="text/javascript" src="<?= Yii::app()->theme->baseUrl?>/assets/js/revolution/extensions/revolution.extension.slideanims.min.js"></script> 
<script type="text/javascript" src="<?= Yii::app()->theme->baseUrl?>/assets/js/revolution/extensions/revolution.extension.video.min.js"></script> 

<!-- script JS  --> 
<script src="<?= Yii::app()->theme->baseUrl?>/assets/js/script.js"></script>
</body></html>