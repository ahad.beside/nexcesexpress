<nav class="main-menu">
  <div class="navbar-header"> 
    <!-- Toggle Button -->
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
  </div>
  <div class="navbar-collapse collapse clearfix">
    <?php
    $controllerId=strtolower(Yii::app()->controller->id);
    $actionId=strtolower(Yii::app()->controller->action->id);
    if($controllerId=='site' && $actionId=='index'){
      $homeActive='current';
    }else{
      $homeActive='';
    }
    if($controllerId=='service' && $actionId=='index'){
      $serviceActive='current';
    }else{
      $serviceActive='';
    }
    if($controllerId=='clientcategory' || $controllerId=='client'){
      $clientActive='current';
    }else{
      $clientActive='';
    }

    if($controllerId=='teamcategory' || $controllerId=='ourteam'){
      $teamActive='current';
    }else{
      $teamActive='';
    }
    if($controllerId=='networkcategory' || $controllerId=='network'){
      $networkActive='current';
    }else{
      $networkActive='';
    }if($controllerId=='site' && $actionId=='album'){
      $galleryActive='current';
    }else{
      $galleryActive='';
    }
    if($controllerId=='jobcategory' || $controllerId=='joblist'){
      $careerActive='current';
    }else{
      $careerActive='';
    }
    if($controllerId=='site' && $actionId=='contactus'){
      $contactActive='current';
    }else{
      $contactActive='';
    }
    ?>
    <ul class="navigation clearfix">
      <li class="<?=$homeActive;?>"><a href="<?= Yii::app()->homeUrl;?>">Home</a></li>
      <li class="<?=$serviceActive;?>"><a href="<?= Yii::app()->createUrl('//service');?>">Services</a></li>
      <li class="dropdown <?=$clientActive;?>"><a href="#">Client</a>
      	<ul>
          <?php $clientCategory=ClientCategory::model()->findAll("status=1 order by sort_order");
          foreach($clientCategory as $rowClientCat):
          ?>
            <li><a href="<?= ClientCategory::model()->makeLink($rowClientCat->id)?>"><?=$rowClientCat->name;?></a></li>
          <?php endforeach;?>
        </ul>
      </li>
      <li class="<?=$networkActive;?>"><a href="<?= Yii::app()->createUrl('//network');?>">Network</a></li>
      <li class="<?=$galleryActive;?>"><a href="<?= Yii::app()->createUrl('//site/album');?>">Gallery</a></li>
      <li><a href="<?= Yii::app()->createUrl('//page/8');?>">About Us</a></li>
      
      <li class="dropdown <?=$teamActive;?>"><a href="#">Our Team</a>
        <ul>
          <?php $teamCategory=TeamCategory::model()->findAll("status=1 order by sort_order");
          foreach($teamCategory as $rowClientCat):
          ?>
            <li><a href="<?= TeamCategory::model()->makeLink($rowClientCat->id)?>"><?=$rowClientCat->name;?></a></li>
          <?php endforeach;?>
        </ul>
      </li>
      <li class="<?=$careerActive;?>"><a href="<?= Yii::app()->createUrl('//jobCategory/all');?>">Career</a></li>
      
      <li class="<?=$contactActive;?>"><a href="<?= Yii::app()->createUrl('//site/contactUs');?>">Contact Us</a></li>
    </ul>
  </div>
</nav>

<div class="srchbox1" style="float:right">
    <ul class="menusearch">
        <li>
            <div class="bz_search_bar" id="search_button">
                <i class="fa fa-search" aria-hidden="true"></i>
            </div>
            <div id="search_open" class="bz_search_box" style="display: none;">
                <input placeholder="Search here" type="text">
                <button><i class="fa fa-search" aria-hidden="true"></i></button>
            </div>
        </li>
    </ul>
</div>
