<div class="container">
  <div class="sec-title text-center">
    <h2>Latest News</h2>
    <span class="colorborder"></span> </div>
  <div class="row"> 
    <!-- News 1 -->
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="single-blog-post">
        <div class="img-box"> <img src="assets/images/resource/budgect-2017.jpg" alt="">
          <div class="overlay">
            <div class="box">
              <div class="content">
                <ul>
                  <li><a href="#"><i class="fa fa-link"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="content-box" style="min-height:217px;">
          <div class="date-box">
            <div class="inner">
              <div class="date"> <b>04</b> June  </div>
            </div>
          </div>
          <div class="content"> <a href="blog-details.html">
            <h3>Budget 2017</h3>
            </a>
            <p>The draft finance bill has been published on 01 June 2017</p>
          </div>
        </div>
      </div>
    </div>
    <!-- News 2 -->
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="single-blog-post">
        <div class="img-box"> <img src="assets/images/resource/bd_two_business.jpg" alt="">
          <div class="overlay">
            <div class="box">
              <div class="content">
                <ul>
                  <li><a href="#"><i class="fa fa-link"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="content-box">
          <div class="date-box">
            <div class="inner">
              <div class="date"> <b>24</b> apr </div>
            </div>
          </div>
          <div class="content"> <a href="blog-details.html">
            <h3>Bangladesh two notches up in doing business ranking</h3>
            </a>
            <p>Bangladesh moved two position up to 176 in the World Bank’s ranking  of the ease of doing business…</p>
          </div>
        </div>
      </div>
    </div>
    <!-- News 3 -->
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="single-blog-post">
        <div class="img-box"> <img src="assets/images/resource/bida.jpg" alt="">
          <div class="overlay">
            <div class="box">
              <div class="content">
                <ul>
                  <li><a href="#"><i class="fa fa-link"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="content-box">
          <div class="date-box">
            <div class="inner">
              <div class="date"> <b>01</b> Sept </div>
            </div>
          </div>
          <div class="content"> <a href="blog-details.html">
            <h3>Bangladesh Investment Development Authority (BIDA)</h3>
            </a>
            <p> BIDA has commenced its journey abolishing erstwhile BOI and Privatization Commission……</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="row text-center">
      	<a href="#" class="blogbtn">More News &nbsp;<i class="icon fa fa-long-arrow-right"></i></a>
      </div>
      
</div>
