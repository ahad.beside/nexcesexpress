<div class="container">
  <div class="row">
    <div class="col-sm-8"> Copyright &copy; <?=date('Y')?> <?=$this->siteName?> <br class="visible-xs">
      All rights reserved. </div> 
    <div class="text-right col-sm-4">
    	<div class="coder71" style="text-align:right; padding-right:32px;">
            Design &amp; Developed By: <a href="http://www.nexcesexpress.com/" target="_blank">Nexces Express</a>
      </div>
    </div>
  </div>
</div>