
<nav class="main-menu">
  <div class="navbar-header"> 
    <!-- Toggle Button -->
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
    </button>
  </div>

  <div class="navbar-collapse collapse clearfix">
    <ul class="navigation clearfix">
      <li><a href="<?= Yii::app()->homeUrl;?>">Home</a></li>
      <?php 
        $menuTop=Menu::model()->findAll("position='Top Menu' and status=1 and parent is NULL order by sort_order");
        
        foreach($menuTop as $rowMenuTop):
          $menuTopChild=Menu::model()->findAll("position='Top Menu' and status=1 and parent=".$rowMenuTop->id." order by sort_order");
          if(count($menuTopChild)>0)
            $dropDownClass='dropdown';
          else
            $dropDownClass='';

          if($rowMenuTop->new_tab=='Yes')
            $newTabTop='target="_blank"';
          else
            $newTabTop='';
      ?>
      <li class="<?=$dropDownClass?>"><a <?=$newTabTop;?> href="<?= Menu::model()->makeLink($rowMenuTop->id)?>"><?=$rowMenuTop->name;?></a>
        <ul>
          <?php 
            foreach($menuTopChild as $rowMenuTopChild):
              if($rowMenuTopChild->new_tab=='Yes')
                $newTabTopChild='target="_blank"';
              else
                $newTabTopChild='';

              $menuTopChild2=Menu::model()->findAll("position='Top Menu' and status=1 and parent=".$rowMenuTopChild->id." order by sort_order");
              if(count($menuTopChild2)>0)
                $dropDownClass='dropdown';
              else
                $dropDownClass='';
          ?>
          <li class="<?=$dropDownClass?>"><a <?=$newTabTopChild;?> href="<?= Menu::model()->makeLink($rowMenuTopChild->id)?>"><?=$rowMenuTopChild->name;?></a>
            <ul>
              <?php                 
                foreach($menuTopChild2 as $rowMenuTopChild2):
                  if($rowMenuTopChild2->new_tab=='Yes')
                    $newTabTopChild2='target="_blank"';
                  else
                    $newTabTopChild2='';
              ?>
              <li><a <?=$newTabTopChild2;?> href="<?= Menu::model()->makeLink($rowMenuTopChild2->id)?>"><?=$rowMenuTopChild2->name;?></a></li>
              <?php endforeach;?>
            </ul>
          </li>
        <?php endforeach;?>
        </ul>
      </li>
      <?php endforeach;?>
    </ul>
  </div>
</nav>

<div class="srchbox1" style="float:right">
  <form action="<?= Yii::app()->createUrl('//site/search')?>" method="get" accept-charset="utf-8">
    <ul class="menusearch">
      <li>
        <div class="bz_search_bar" id="search_button">
          <i class="fa fa-search" aria-hidden="true"></i>
        </div>
        <div id="search_open" class="bz_search_box" style="display: none;">
          <input placeholder="Search here" type="text" name="searchText" value="<?= $_GET['searchText']?>">
          <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
        </div>
      </li>
    </ul>
  </form>
</div>

