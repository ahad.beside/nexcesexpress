<div class="row">
  <div class="col-sm-2">
    <div class="logo"> <a href="<?= Yii::app()->homeUrl;?>"><img src="<?= $this->adminLogo?>"></a> </div>
  </div>
  <div class="col-sm-4">
    <div class="doing_bd"> <a href="<?= Yii::app()->createUrl('//page/9');?>" class="blogbtn_alt">Worldwide Courier Solution</a> </div>
  </div>
  
  <div class="col-sm-6">
    <div class="header-info">
      <ul>
        <li>
          <div class="hdbox-left"> <span class="flaticon-phone-call"></span> </div>
          <div class="hdbox-right"> <span>Call Us</span><?= $this->adminPhone?> </div>
        </li>
        <li>
          <div class="hdbox-left"> <span class="flaticon-envelope"></span> </div>
          <div class="hdbox-right"> <span>Email</span> <?= $this->adminMail?> </div>
        </li>
        <li>
          <div class="hdbox-right">
            <form class="navbar-form"  action="<?= Yii::app()->createUrl('//site/track')?>" method="get" accept-charset="utf-8">
              <div class="input-group add-on">      
                <input class="form-control"  placeholder="Search Tracking Number" type="text" name="trackNo" value="<?= $_GET['trackNo']?>">
                <div class="input-group-btn">
                  <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
              </div>
            </form>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<style>
  .doing_bd{
    padding-top:50px;
    text-align:center;
  }

  .blogbtn_alt{
    line-height: 7px !important;
  }
</style>