<?php include 'header_assets.php'; ?>
<div class="page-wrapper"> 
  <!-- Main Header -->
  <header class="site-header headerfirst"> 
    <?php include 'header.php'; ?>
  </header>
  <!-- Main Header end --> 
  <!--Main Slider-->
  <section class="rev_slider_wrapper">
    <?php include 'slider.php'; ?>
  </section>
  <!--Main Slider  end--> 
  <!-- DYNAMIC CONTENT SECTION START -->
  <?= $content?>
  <!-- DYNAMIC CONTENT SECTION END -->
  <!-- footer section -->
  <footer class="footer1">
    <?php include 'footer.php'; ?>
    <div class="copyright">
      <?php include 'copyright.php'; ?>
    </div>
  </footer>
  <!-- footer section end --> 
  
</div>
<!--End pagewrapper--> 


<?php include 'footer_assets.php'; ?>