<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <script src="<?= Yii::app()->theme->baseUrl?>/assets/js/jquery-1.12.4.min.js"></script>
    <title><?= $this->pageTitle.' - '.Yii::app()->name;?></title>
    <!-- Stylesheets -->
    <!-- bootstrap v3.3.6 css -->
    <link href="<?= Yii::app()->theme->baseUrl?>/assets/css/bootstrap.css" rel="stylesheet">
    <!-- font-awesome css -->
    <link href="<?= Yii::app()->theme->baseUrl?>/assets/css/font-awesome.css" rel="stylesheet">
    <!-- flaticon css -->
    <link href="<?= Yii::app()->theme->baseUrl?>/assets/css/flaticon.css" rel="stylesheet">
    <!-- animate css -->
    <link href="<?= Yii::app()->theme->baseUrl?>/assets/css/animate.css" rel="stylesheet">
    <!-- owl.carousel css -->
    <link href="<?= Yii::app()->theme->baseUrl?>/assets/css/owl.css" rel="stylesheet">
    <!-- fancybox css -->
    <link href="<?= Yii::app()->theme->baseUrl?>/assets/css/jquery.fancybox.css" rel="stylesheet">
    <link href="<?= Yii::app()->theme->baseUrl?>/assets/css/hover.css" rel="stylesheet">
    <link href="<?= Yii::app()->theme->baseUrl?>/assets/css/style.css" rel="stylesheet">
    <!-- revolution slider css -->
    <link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl?>/assets/css/revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl?>/assets/css/revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl?>/assets/css/revolution/navigation.css">

    <!--Favicon-->
    <link rel="shortcut icon" href="<?= Yii::app()->theme->baseUrl?>/assets/images/nexcessexpress.ico" type="image/x-icon">
    <link rel="icon" href="<?= Yii::app()->theme->baseUrl?>/assets/images/nexcessexpress.ico" type="image/x-icon">
    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link href="<?= Yii::app()->theme->baseUrl?>/assets/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

    