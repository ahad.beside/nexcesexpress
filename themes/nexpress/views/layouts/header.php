<!-- Header Top -->
    <!--<div class="header-top">
      <?php //include 'header-top.php'; ?>
    </div>-->
    <!-- Header Top End -->     
    <!--Header-Main-->
    <div class="header-main">
      <div class="container">
        <?php include 'logo.php'; ?>
      </div>
    </div>
    <!--Header-Lower-->
    <div class="header-lower">
      <div class="container">
        <div class="nav-outer clearfix"> 
          <!-- Main Menu -->
          <?php include 'main_menu.php'; ?>
          <!-- Main Menu End--> 
        </div>
      </div>
    </div>
    <!--Sticky Header-->
    <div class="sticky-header">
      <div class="auto-container clearfix"> 
        <!--Logo-->
        <div class="logo pull-left"> <a href="<?= Yii::app()->homeUrl;?>"><img src="<?= Yii::app()->theme->baseUrl ?>/assets/images/sticky-logo.png" title="Transpo" title="Transpo"></a> </div>        
        <div class="right-col pull-right" style="position:relative"> 
          <!-- Main Menu -->
          <?php include 'main_menu.php'; ?>
          <!-- Main Menu End--> 
        </div>
        <!--Right Col-->        
      </div>  
    </div>
    <!--End Sticky Header--> 

<?php /* <div class="sticky_news">
	<a href="<?= Yii::app()->createUrl('//newsEvents')?>"><img src="<?= Yii::app()->theme->baseUrl?>/assets/images/news.png" /></a>
</div> */ ?>