<div class="container">
  <div class="row">

    <div class="col-md-3 col-sm-6">
      <div class="footersec">
      	<h2 style="font-size:18px; color:#FFF; margin-top:15px; margin-bottom:15px;">Our Address</h2>
        <div class="ftcontact">
          <?=  Page::model()->find("id=16 and status=1")->description;?>          
        </div>
        <ul class="social">          
          <li><a href="<?= $this->facebook;?>" class="facebook"><i class="fa fa-facebook"></i></a></li>
          <li><a href="<?= $this->twitter;?>" class="twitter"><i class="fa fa-twitter"></i></a></li>
          <li><a href="<?= $this->google;?>" class="google-plus"><i class="fa fa-google-plus"></i></a></li>
          <li><a href="<?= $this->pinter;?>" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
          <li><a href="<?= $this->youtube;?>"  class="youtube"><i class="fa fa-youtube"></i></a></li>
        </ul>
      </div>
    </div>

    <!--<div class="col-md-3 col-sm-6">
    <div class="footersec">
      <h2 style="font-size:18px; color:#FFF; margin-top:15px; margin-bottom:15px;">&nbsp;</h2>
    </div>
    </div>  -->    
    <div class="col-md-2 col-sm-6">
      <div class="footer-sec quick-links">
        <h3 class="title">Information</h3>
        <ul>
          <?php $menuLeft=Menu::model()->findAll("position='Footer Menu Left' and status=1 and parent is NULL order by sort_order");
          foreach($menuLeft as $rowMenuLeft):
            if($rowMenuLeft->new_tab=='Yes')
              $newTab='target="_blank"';
            else
              $newTab='';
          ?>
          <li><i class="fa fa-chevron-circle-right"></i><a <?=$newTab?> href="<?= Menu::model()->makeLink($rowMenuLeft->id)?>"><?= $rowMenuLeft->name?></a></li>
        <?php endforeach;?>
        </ul>
      </div>
    </div>
    <div class="col-md-2 col-sm-6">
      <div class="footer-sec quick-links">
        <h3 class="title">Our Services</h3>
        <ul class="footer-service">
          <?php $ourSevice=Service::model()->findAll("status=1 order by sort_order");
          foreach($ourSevice as $rowService):
          ?>
          <li><i class="fa fa-chevron-circle-right"></i><a href="<?= Service::model()->makeLink($rowService->id)?>"><?= $rowService->name?></a></li>
        <?php endforeach;?>         
        </ul>
      </div>
    </div>

    <div class="col-md-4 col-sm-6">
      <div class="footer-sec quick-links">
        <h3 class="title">Why select nexcesexpress</h3>
        <ul class="footer-service">
          <li>Wings International Express has achieved worldwide recognition as one of the world’s leading international air Courier Company specializing in the delivery of urgent, vital and confidential documents and parcels worldwide.</li>          
        </ul>
      </div>
    </div>

  </div>
</div>
