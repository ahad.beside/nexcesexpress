<?php if(count($data['featuredNews'])>0){?>
<div class="container">
  <div class="sec-title text-center">
    <h2>Testimonial</h2>
    <span class="colorborder"></span> </div>
  <div class="row testimonials-slider"> 
    <!-- News 1 -->
    <?php foreach($data['featuredNews'] as $rowNews):?>
    
    <div class="col-lg-12">
      <div class="single-blog-post">
        <div class="content-box" style="min-height:185px;">
          <div class="TestimonialContent"><?= strip_tags(Yii::app()->easycode->getExcerpt($rowNews->details,0,100));?></div>
          <div class="content">
            <div style="float: left; margin-right: 15px;">
              <img class="img-responsive" style="width: 50px; height: 50px; border-radius: 100px;" src="<?= Yii::app()->easycode->showOriginalImage($rowNews->image,'/news/')?>" alt="">
            </div>
            <div style="padding-top: 15px; float: left;"><?= $rowNews->name?></div>
          </div>
        </div>
      </div>
    </div>

   

  

  <?php endforeach;?>
    <!-- News 1 -->
  </div>
</div>
<?php } ?>

<style type="text/css">
  .TestimonialContent{
    width: 100%;
    display: inline-block;
    background: #f2f2f2;
    padding: 8px;
    margin-bottom: 15px;
    position: relative;
  }
  .TestimonialContent:after {
    content:'';
    position: absolute;
    top: 100%;
    left: 15px;
    right: 0;
    margin: 0;
    width: 0;
    height: 0;
    border-top: solid 10px #f2f2f2;
    border-left: solid 10px transparent;
    border-right: solid 10px transparent;
}

</style>