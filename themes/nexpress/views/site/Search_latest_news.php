<?php if(count($data['featuredNews'])>0){?>
<div class="container">
      <div class="sec-title" style="border-bottom:1px solid #ccc; margin-bottom: 25px">
        <h3>Search Result from Latest News</h3>
        
         </div>
      <div class="row">

        <?php foreach($data['featuredNews'] as $rowNews):?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         
         
          <div class="search-box">
            <a href="<?= NewsEvents::model()->makeLink($rowNews->id)?>">
            <div class="servicebox-image col-md-2" style="height: 125px;">  <img class="img-responsvie" src="<?= Yii::app()->easycode->showOriginalImage($rowNews->image,'/news/')?>" />
              
               </div>
            <div class="servicebox-content col-md-10">
              <h3><?=$rowNews->name?></h3>
              <div style="font-style: italic; color: #888"> <?= date('d-m-Y',strtotime($rowNews->news_date));?>  </div>
              <p><?= strip_tags(Yii::app()->easycode->getExcerpt($rowNews->details,0,100));?></p>
            </div>
            </a>
          </div>
          
        </div>
      <?php endforeach;?>
      </div>
    </div>
    <?php } ?>
