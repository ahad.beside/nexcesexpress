<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/cubeportfolio/css/cubeportfolio.css">
<?php if(count($data['album'])>0){?>

<!--Page Info-->
        <section class="page-info">
            <div class="auto-container clearfix">
                <div class="col-md-6">
                  <div class="pull-left">
                      <h3> Gallery</h3>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="pull-right">
                    <ul class="bread-crumb clearfix">
                        <li><a href="<?= Yii::app()->homeUrl;?>">Home</a></li>
                  
                        <li> Gallery</li>
                    </ul>
                  </div>
            </div>

            </div>
        </section>
        <!--Page Info end-->

<div class="container">
     
      <div class="row">

        <div id="js-grid-masonry-projects" class="cbp cbp-l-grid-masonry-projects" style="margin-bottom: 20px;">
        <?php foreach ($data['album'] as $album):?>
        <div class="cbp-item graphic">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <img src="<?= Yii::app()->easycode->showOriginalImage($album->image,Yii::app()->params->albumDir)?>"/>
                    
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <a href="<?php echo Yii::app()->createUrl('//site/gallery',array('id'=>$album->id))?>" class="" rel="">View Gallery</a>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="title_bg"><a href="<?php echo Yii::app()->createUrl('//site/gallery',array('id'=>$album->id))?>"><?= $album->name?></a></div>
        </div>
        
        <?php endforeach; ?>   
    </div>
        
      </div>
      
    </div>
    <?php } ?>



<!-- load cubeportfolio -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/cubeportfolio/js/jquery.cubeportfolio.js"></script>

    <!-- init cubeportfolio -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/cubeportfolio/js/main.js"></script>