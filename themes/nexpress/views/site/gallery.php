<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/cubeportfolio/css/cubeportfolio.css">

<section class="page-info" style="margin-bottom: 0px;">
            <div class="auto-container clearfix">
                <div class="col-md-6">
                  <div class="pull-left">
                      <h3> <?= $data['album']?></h3>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="pull-right">
                    <ul class="bread-crumb clearfix">
                        <li><a href="<?= Yii::app()->homeUrl;?>">Home</a></li>
                        
                        <li><a href="<?= Yii::app()->createUrl('//site/album');?>">Gallery</a></li>
                        <li> <?= $data['album']?></li>
                    </ul>
                  </div>
            </div>
            </div>
        </section>
<div class="cbp-l-project-title" style="font-size: 28px; color: #333; padding: 20px 0px; margin-bottom: 0px;"></div>
<!-- <div class="cbp-l-project-subtitle">by Paul Flavius Nechita</div> -->



<div class="container" style="margin-bottom: 30px;">
<div id="js-grid-masonry-projects" class="cbp cbp-l-grid-masonry-projects">
<?php 
//print_r($data['gallery']);exit;
        foreach ($data['gallery'] as $album):

        ?>
        <div class="cbp-item graphic">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <img src="<?= Yii::app()->easycode->showOriginalImage($album->image,'/album/') ?>" alt="<?= $data['album']?>">
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            
                            <a href="<?= Yii::app()->easycode->showOriginalImage($album->image,'/album/') ?>" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="<?= $data['album']?>">view larger</a>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
        
        <?php endforeach; ?>

        </div>
        
        
       
        
        
        
        
        
        
    </div>


<!-- load cubeportfolio -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/cubeportfolio/js/jquery.cubeportfolio.js"></script>

    <!-- init cubeportfolio -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/cubeportfolio/js/main.js"></script>

