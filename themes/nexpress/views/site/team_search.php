
<?php if(count($data['featuredMember'])>0){?>
<div class="container">
      <div class="sec-title" style="border-bottom:1px solid #ccc; margin-bottom: 25px">
        <h3>Search Result from Member</h3>
        
         </div>
      <div class="row">

        <?php foreach($data['featuredMember'] as $rowMember):?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         
         
          <div class="search-box" style="padding-top: 0px;">
            <a href="<?= OurTeam::model()->makeLink($rowMember->id)?>">
            <div class="servicebox-image col-md-2" style="padding-left: 0;">  <img class="img-responsvies" src="<?= Yii::app()->easycode->showOriginalImage($rowMember->image,'/team/')?>" />
              
               </div>
            <div class="servicebox-content col-md-10">
              <h3><?=$rowMember->name?></h3>
              <p><?= $rowMember->designation?></p>
            </div>
            </a>
          </div>
          
        </div>
      <?php endforeach;?>
      </div>
    </div>
    <?php } ?>
