<?php if(count($data['videos'])){?>
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="sec-title text-center">
        <h2 style="font-weight: bold; font-size: 46px; color: #fff">Discover</h2>
         <h3 style="font-weight: bold; font-size: 36px; color: #19db30">Nexcess Express </h3>
      </div>
    </div>
  </div>
  <div class="row">
    <?php 
    foreach($data['videos'] as $rowMember):
    ?>
    <div class="col-sm-12" style="text-align: center;">
      <h3><?=$rowMember->button_label; ?></h3>
      <iframe title="YouTube video player" src="<?= $rowMember->video_url; ?>" allowfullscreen="" width="750" height="350" frameborder="0"></iframe>
    </div>
  <?php endforeach;?>
  </div>
</div>
<?php } ?>