<!--Page Info-->
<section class="page-info" style="margin-bottom: 0px;">
  <div class="auto-container clearfix">
    <div class="col-md-6">
      <div class="pull-left">
          <h3>Search</h3>
      </div>
    </div>
    <div class="col-md-6">
      <div class="pull-right">
          <ul class="bread-crumb clearfix">
            <li><a href="<?= Yii::app()->homeUrl;?>">Home</a></li>        
            <li><?= $_GET['searchText']?></li>
          </ul>
      </div>
    </div>
  </div>
</section>
<!--Page Info end-->

<div class="container">
  <div class="row">

    <!--First part-->
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
      <div class="about-all-back">
        <div class="about-head-top-text">
        </div>
        <div class="famous-poem">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <form class="navbar-form" action="<?= Yii::app()->createUrl('//site/search')?>" method="get" accept-charset="utf-8">
                <div class="input-group add-on">      
                  <input class="form-control"  placeholder="Search Tracking Number" type="text" name="searchText" value="<?= $_GET['searchText']?>">
                  <div class="input-group-btn">
                    <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--First part-->
    
  </div>
</div>

<?php if(count($data['featuredService'])>0){?>
<!--All services-->
  <div class="services-sec" style="padding: 50px 0px;">
    <?php include 'search_pages_services.php'; ?>
  </div>
  <!--All services end--> 
<?php } ?>
<?php if(count($data['featuredMember'])>0){?>
  <!-- team-section -->
  <section class="ourteamsec sec-padding" style="background-color:#FFF;">
    <?php include 'team_search.php'; ?>
  </section>
  <!-- team section end --> 
  <?php } ?>
<?php if(count($data['featuredNews'])>0){?>
  <!-- blog section end -->
  <section class="blog-home sec-padding" style="padding: 50px 0px;">
    <?php include 'Search_latest_news.php'; ?>
  </section>
  <!-- blog section end --> 
<?php } ?>

<?php if(count($data['bookDetails'])>0){?>
  <!--Carrer -->
  <div class="parterer-sec" style="padding: 50px 0px; background: #f2f2f2; margin-top: 0px; margin-bottom: 0px;">
    <?php include 'search_bookDetails.php'; ?>
  </div>
  <!--Carrer end--> 
<?php } ?>

<?php if(count($data['page'])>0){?>
  <!--Carrer -->
  <div class="parterer-sec" style="padding: 50px 0px; margin-top: 0px; margin-bottom: 0px;">
    <?php include 'search_page.php'; ?>
  </div>
  <!--Carrer end--> 
  <?php } ?>
<!-- 
 <style type="text/css">
   .input-group{width: 100%;}
   .form-control{height: 50px; font-size: 120%;}
   .btn-primary{height: 50px; padding: 10px 20px;}
 </style> -->