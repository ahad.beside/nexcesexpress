<!--Page Info-->
<section class="page-info">
  <div class="auto-container clearfix">
    <div class="col-md-6">
      <div class="pull-left">
          <h3>Search</h3>
      </div>
    </div>
    <div class="col-md-6">
      <div class="pull-right">
          <ul class="bread-crumb clearfix">
            <li><a href="<?= Yii::app()->homeUrl;?>">Home</a></li>        
            <li><?= $_GET['trackNo']?></li>
          </ul>
      </div>
    </div>
  </div>
</section>
<!--Page Info end-->

<!--Page container start-->
<div class="container">
  <div class="row all-color">

    <!--First part-->
    <?php $this->renderPartial('//shipping/trackSearch', array('model'=>$model)); ?>
    <!--First part-->

    <!--second part-->
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
      <div class="about-all-back">
        <div class="about-head-top-text">
        </div>
        <div class="famous-poem">
          <div class="panel panel-primary">
            <div class="panel-heading">

              <?php if(count($data['bookDetails'])>0) {?>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <h2 style="padding-left:12px; text-align: center;">Search Track No: <?= $data['bookDetails']->nex_code;?> is Confirmed</h2>
                </div>
              </div>

              
              <table class="table table-bordered table-responsive" style="color: black;">      
                <tbody>
                  <tr>
                    <td colspan="2">
                        <div class="panel-heading">
                          <h3 style="text-align: center;">
                            <!-- <span class="glyphicon glyphicon-chevron-right"></span> --> 
                            Your Booking with Us
                          </h3>
                        </div>
                    </td>
                  </tr>

                  <tr>
                    <td style="width:50%">
                    <table class="table table-borderless table-responsive">
                      <tbody>
                        <tr>
                          <td style="width:25%; border:0;"><label class="control-label">Sender</label></td>
                          <td style="width:1%; border:0;"><label class="control-label">:</label></td>
                          <td style="border:0;"><label class="control-label"><?= $data['bookDetails']->sender_company;?></label></td>
                        </tr>
                        <tr>
                          <td style="width:14%;"><label class="control-label">Attn</label></td>
                          <td style="width:1%;"><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->sender_attention;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Email</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->sender_email;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Address</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->sender_address;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Tel</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->sender_phone;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">City</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->sender_city;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Country</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->sender_country;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Tracking</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->nex_code;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Booking</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->item_type;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Pay Mode</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->payment_method;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">PCS</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->pices;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Remarks</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->remarks;?></label></td>
                        </tr>
                      </tbody>
                    </table>
                    </td>

                    <td style="width:50%">
                    <table class="table table-borderless table-responsive">
                      <tbody>
                        <tr>
                          <td style="width:25%; border:0;"><label class="control-label">Recipient</label></td>
                          <td style="width:1%; border:0;"><label class="control-label">:</label></td>
                          <td style="border:0;"><label class="control-label"><?= $data['bookDetails']->receiver_company;?></label></td>
                        </tr>  
                        <tr>
                          <td style="width:14%;"><label class="control-label">Attn</label></td>
                          <td style="width:1%;"><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->receiver_attention;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Email</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->receiver_email;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Address</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->receiver_address;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Tel</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->receiver_phone;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">City</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->receiver_city;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Destination</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->dimensions;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Shipper Ref </label></td>
                          <td><label class="control-label">:</label></td>
                          <td style=""><label class="control-label"><?= $data['bookDetails']->shipping_ref;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Weight (KG)</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->billing_weight;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Items</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->receiver_company;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Content</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->receiver_company;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Value</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookDetails']->values;?></label> USD</td>
                        </tr>
                      </tbody>
                    </table>
                    </td>
                  </tr>

                  <tr>
                    <td colspan="2">
                      <a class="btn btn-default" href="<?= Yii::app()->createUrl('//shipping/OnlineBooking');?>"><span class="glyphicon glyphicon-plus"></span> Book Another</a>
                      <a class="btn btn-default" href="<?= $this->createUrl('//shipping/download', array('id' => $data['bookDetails']->id)) ?>" data-url="<?= $this->createUrl('download', array('id' => $data['bookDetails']->id)) ?>" onclick="//downloadInvoice($(this));" target="_blank" class="btn btn-sm" style="text-align:left; font-weight: normal"><span class="glyphicon glyphicon-save"></span> Download</a> 
                      <a class="btn btn-default" href="<?= Yii::app()->createUrl('//shipping/print/' . $data['bookDetails']->id) ?>" target="_blank"><span class="glyphicon glyphicon-print"></span> Print</a>
                    </td>
                  </tr>
                </tbody>
              </table>
              <?php } else { ?>
                <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <h2 style="padding-left:12px; text-align: center;">Search Track No: <?= $_GET['trackNo'];?> is not correct</h2>
                </div>
              </div>
              <?php } ?>
            </div><!--panel-heading-->
          </div><!--panel panel-primary-->
        </div>
      </div>
    </div>
    <!--second part-->

  </div><!--row end-->
</div>
<!--Page container end-->

<style type="text/css">

  /*.all-color {
    background: #f2f2f2;
    margin: 0px;
    border-radius: 6px;
    border: 1px solid #ddd;
    padding: 20px;
    margin-bottom: 30px;
  }*/
  
  .navbar-form {
    margin-left: -15px!important;
  }
</style>