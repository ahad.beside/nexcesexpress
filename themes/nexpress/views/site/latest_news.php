<?php if(count($data['featuredNews'])>0){?>
<div class="container">
  <div class="sec-title text-center">
    <h2>Latest News</h2>
    <span class="colorborder"></span> </div>
  <div class="row"> 
    <!-- News 1 -->
    <?php foreach($data['featuredNews'] as $rowNews):?>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="single-blog-post">
        <div class="img-box" style="height:204px; overflow:hidden;"><img class="img-responsive" src="<?= Yii::app()->easycode->showOriginalImage($rowNews->image,'/news/')?>" alt="">
          <div class="overlay">
            <div class="box">
              <div class="content">
                <ul>
                  <li><a href="<?= NewsEvents::model()->makeLink($rowNews->id)?>"><i class="fa fa-link"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="content-box" style="min-height:185px;">
          <div class="date-box">
            <div class="inner">
              <div class="date"> <b><?= date('d',strtotime($rowNews->news_date));?></b> <?= date('M',strtotime($rowNews->news_date));?>  </div>
            </div>
          </div>
          <div class="content"> <a href="<?= NewsEvents::model()->makeLink($rowNews->id)?>">
            <h3><?= $rowNews->name?></h3>
            </a>
            <p><?= strip_tags(Yii::app()->easycode->getExcerpt($rowNews->details,0,100));?></p>
          </div>
        </div>
      </div>
    </div>
  <?php endforeach;?>
    <!-- News 1 -->
  </div>
  <div class="row">
  <div class="col-md-12 text-center">
      	<a href="<?= Yii::app()->createUrl('//newsEvents')?>" class="blogbtn">More News &nbsp;<i class="icon fa fa-long-arrow-right"></i></a>
      </div>
      </div>
</div>
<?php } ?>