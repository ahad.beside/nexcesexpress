
<?php if(count($data['featuredServices'])>0) { ?>
<div class="container">
  <div class="sec-title text-center">
    <h2>Our Services</h2>
    <span class="colorborder"></span> 
  </div>
  
  <div class="row">
    <?php foreach($data['featuredServices'] as $rowService):?>
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="single-servicebox">
          <a href="<?= Service::model()->makeLink($rowService->id)?>">
            <div class="servicebox-image">  <img class="img-responsvie" src="<?= Yii::app()->easycode->showOriginalImage($rowService->image,'/service/')?>" /></div>
            <div class="servicebox-content">
              <h3><?=$rowService->name?></h3>
              <p><?= strip_tags(Yii::app()->easycode->getExcerpt($rowService->details,0,100));?></p>
            </div>
          </a>
        </div>
      </div>
    <?php endforeach;?>
  </div>
</div>
<?php } ?>