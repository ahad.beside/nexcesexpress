<!--welcome container-->
<div class="welcome-sec">
  <div class="container">
    <div class="sec-title">
      <h2>        
        <?=$data['welcomeText']->title;?>
      </h2>
      <span class="colorborder"></span> 
    </div>        
      <div class="row">
        <div class="col-md-12 margin-bott-30 home-content">
          <?=$data['welcomeText']->description;?>
        </div>
      </div> 
  </div>
</div>
<!--welcome container end--> 
  
<!--All services-->
<div class="services-sec">
  <?php include 'services.php'; ?>
</div>
<!--All services end-->

<!-- videos-section -->
<section class="parallax">
  <?php include 'videos.php';?>
</section>
<!-- videos section end --> 

<!--Partener slider -->
<div class="parterer-sec"  style="background-color:#f2f2f2; margin: 0px; padding: 40px 0px;">
  <?php include 'clients.php'; ?>
</div>
<!--Partener slider end--> 

<style type="text/css">
  .home-content p{
    font-size: 14px;
  }
</style>