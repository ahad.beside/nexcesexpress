<?php if(count($data['featuredService'])>0){?>
<div class="container">
      <div class="sec-title" style="border-bottom:1px solid #ccc; margin-bottom: 25px">
        <h3>Search Result from Services</h3>        
         </div>
      <div class="row">

        <?php foreach($data['featuredService'] as $rowService):?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         
         
          <div class="search-box">
            <a href="<?= Service::model()->makeLink($rowService->id)?>">
            <div class="servicebox-image col-md-2" style="height: 125px;">  <img class="img-responsvie" src="<?= Yii::app()->easycode->showOriginalImage($rowService->image,'/service/')?>" />
              
               </div>
            <div class="servicebox-content col-md-10">
              <h3><?=$rowService->name?></h3>
              <p><?= strip_tags(Yii::app()->easycode->getExcerpt($rowService->details,0,100));?></p>
            </div>
            </a>
          </div>
          
        </div>
      <?php endforeach;?>
      </div>
    </div>
    <?php } ?>
