<div class="sec-title text-center">
        <h2>Our Happy Clients</h2>
        <span class="colorborder"></span> </div>
      <div class="row testmonrow">
        <div class="col-sm-6">
          <div class="testwrp">
            <div class="single-testm">
              <div class="client-image"><img src="assets/images/clients/01.jpg" class="testmimg" alt="" width="100" height="100"> </div>
              <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et.</p>
              <h4>John White<small>Director</small></h4>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="testwrp">
            <div class="single-testm">
              <div class="client-image"><img src="assets/images/clients/01.jpg" class="testmimg" alt="" width="100" height="100"> </div>
              <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et.</p>
              <h4>John White<small>Director</small></h4>
            </div>
          </div>
        </div>
      </div>