
<?php if(count($data['featuredClient'])>0){?>
<div class="container">
  <div class="sec-title text-center">
    <h2>Our Partners</h2>
    <span class="colorborder"></span> 
  </div>

  <div class="partener-slider client_content">
    <?php foreach($data['featuredClient'] as $rowClient):?>
    <div class="image-box">
    	<!-- <a href="<?//= Client::model()->makeLink($rowClient->id)?>"> -->
      <a href="<?= $rowClient->link; ?>">
        <img class="img-responsive" src="<?= Yii::app()->easycode->showOriginalImage($rowClient->image,'/client/')?>" alt="">
      </a>
    </div>
    <?php endforeach;?>
  </div>

</div>
<?php } ?>
