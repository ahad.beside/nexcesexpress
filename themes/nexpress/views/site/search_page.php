<?php if(count($data['page'])>0){?>
<div class="container">
  <div class="sec-title" style="border-bottom:1px solid #ccc; margin-bottom: 25px">
    <h3>Search Result from Page</h3>
    </div>
  <div class="client_content">
    <?php foreach($data['page'] as $rowClient):?>
    <div class="">
    <div class="client_content_info search-box" style="padding: 12px;">
        <div><b><a href="<?= Page::model()->makeLink($rowClient->id)?>"><?=$rowClient->title?></a></b></div>
        <div>
          <?= strip_tags(Yii::app()->easycode->getExcerpt($rowClient->description,0,100));?>

        </div>
    </div>
    </div>
    <?php endforeach;?>
  </div>
</div>
<?php } ?>