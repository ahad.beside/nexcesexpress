<?php if(count($data['featuredNetwork'])>0){?>
<div class="container">
  <div class="sec-title text-center">
    <h2>Our Network</h2>
    <span class="colorborder"></span> </div>
  <div class="partener-slider">
    <?php foreach($data['featuredNetwork'] as $rowNetwork):?>
    <div class="image-box" style="height:107px; overflow:hidden;">  <a href="#"><img class="img-responsive" src="<?= Yii::app()->easycode->showOriginalImage($rowNetwork->image,'/network/')?>" alt=""></a> </div>
    <?php endforeach;?>
  </div>
</div>
<?php } ?>
