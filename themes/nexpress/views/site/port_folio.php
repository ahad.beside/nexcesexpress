<?php if(count($data['featuredMember'])){?>
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="sec-title text-center">
        <h2>Portfolio</h2>
        <span class="colorborder"></span> </div>
    </div>
  </div>
  <div class="row">
    <?php 
    foreach($data['featuredMember'] as $rowMember):
    ?>
    <div class="col-sm-6 col-md-3 col-lg-3">
      <!-- <a href="<?//= OurTeam::model()->makeLink($rowMember->id)?>" target="_blank" > -->
      <a href="<?= $rowMember->web_url; ?>" target="_blank" >
      <div class="team-box hover-box">
        <div class="team-head">
          <img src="<?= Yii::app()->easycode->showOriginalImage($rowMember->image,'/team/')?>" class="img-responsive mauto" alt="">
        </div>
        <?php /*
        <div class="team-content">
          <h3><?= $rowMember->name?></h3>
          <b style="color: #666; font-size: 12px; font-style: italic;"><?= $rowMember->designation?></b> 
          <!-- end team-social --> 
        </div>
        <!-- end team-content --> 
        <*/ ?>
      </div>
    </a>
      <!-- end team-box --> 
    </div>
  <?php endforeach;?>

  </div>
  
  <?php /*
  <div class="row text-center" style="margin-top:40px;">
        <a href="<?= Yii::app()->createUrl('//ourTeam')?>" class="blogbtn">More Team &nbsp;<i class="icon fa fa-long-arrow-right"></i></a>
  </div>
  < */ ?>
</div>
<?php } ?>