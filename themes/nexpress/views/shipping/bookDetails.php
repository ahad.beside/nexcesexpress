<!--Page Info-->
<section class="page-info">
  <div class="auto-container clearfix">
    <div class="col-md-6">
      <div class="pull-left">
        <h3>Shipping</h3>
      </div>
    </div>
    <div class="col-md-6">
      <div class="pull-right">
        <ul class="bread-crumb clearfix">
          <li><a href="<?= Yii::app()->homeUrl;?>">Home</a></li>
          <?php /*<li><a href="<?= Yii::app()->createUrl('//contact');?>">Contact</a></li>*/?>
          <li><a href="<?= Yii::app()->homeUrl;?>">Shipping</a></li>
          <li><?=$shipping->name?></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<!--Page Info end-->


<!--Page container start-->
<div class="container">
  <div class="row all-color">
    <!--First part-->
    <?php $this->renderPartial('trackSearch', array('model'=>$model)); ?>
    <!--First part-->

    <!--second part-->
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
      <div class="about-all-back">
        <div class="about-head-top-text">
        </div>
        <div class="famous-poem">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><h2 style="padding-left:12px;">Online Booking Form</h2></div>
                <div class="col-md-6"><h2 style="padding-right:12px; text-align: right;">Track: <?= $data['challan_number'];?></h2></div>
              </div>
              <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'category-form',
                    //'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'htmlOptions' => array(
                        'class' => 'inner-form', 
                        'role' => 'form', 
                        'enctype' => 'multipart/form-data'),
               ));
              ?>

              <table class="table table-responsive formtable">
                <thead>
                  <tr>
                    <th colspan="2">Shipper Information</th>
                    <th colspan="2">Consignee Information</th>
                  </tr>
                </thead>
                <tbody>
                  <input type="hidden" name="BookDetails['nex_code']" value="<?=$model->nex_code?>">
                  <tr>
                    <td colspan="2"><!-- first colspan="2" start-->
                      <div class="form-group">
                        <?php echo $form->labelEx($model, 'sender_attention', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model, 'sender_attention', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('sender_attention'))); ?>
                        <?php echo $form->error($model, 'sender_attention'); ?>
                      </div>            
                      <div class="form-group">
                        <?php echo $form->labelEx($model, 'sender_company', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model, 'sender_company', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('sender_company'))); ?>
                        <?php echo $form->error($model, 'sender_company'); ?>
                      </div>
                      <div class="form-group">
                        <?php echo $form->labelEx($model, 'sender_address', array('class' => 'control-label')); ?>
                        <?php echo $form->textArea($model, 'sender_address', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('sender_address'))); ?>
                        <?php echo $form->error($model, 'sender_address'); ?>
                      </div>
                      <table>
                      <tr>
                        <td>
                          <div class="form-group" style="padding: 2px;">
                        <?php echo $form->labelEx($model, 'sender_city', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model, 'sender_city', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('sender_city'))); ?>
                        <?php echo $form->error($model, 'sender_city'); ?>
                      </div>
                        </td>
                        <td>
                          <div class="form-group">
                        <?php echo $form->labelEx($model, 'sender_zip', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model, 'sender_zip', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('sender_zip'))); ?>
                        <?php echo $form->error($model, 'sender_zip'); ?>
                      </div>
                        </td>
                      </tr>
                      </table>
                      <div class="form-group">
                        <?php echo $form->labelEx($model, 'sender_country', array('class' => 'control-label')); ?>
                        <?php echo $form->dropDownList($model, 'sender_country', CHtml::listData(Country::model()->findAll(),'name','name'), array('class' => 'form-control','placeholder' => $model->getAttributeLabel('sender_country'))); ?>
                        <?php echo $form->error($model, 'sender_country'); ?>
                      </div>
                      <div class="form-group">
                        <?php echo $form->labelEx($model, 'sender_phone', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model, 'sender_phone', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('sender_phone'))); ?>
                        <?php echo $form->error($model, 'sender_phone'); ?>
                      </div>

                      <div class="form-group">
                        <?php echo $form->labelEx($model, 'sender_email', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model, 'sender_email', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('sender_email'))); ?>
                        <?php echo $form->error($model, 'sender_email'); ?>
                      </div>
                    </td><!-- first colspan="2" end-->

                    <td colspan="2"><!--second colspan="2" start-->
                      <div class="form-group">
                        <?php echo $form->labelEx($model, 'receiver_attention', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model, 'receiver_attention', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('receiver_attention'))); ?>
                        <?php echo $form->error($model, 'receiver_attention'); ?>
                      </div>            
                      <div class="form-group">
                        <?php echo $form->labelEx($model, 'receiver_company', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model, 'receiver_company', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('receiver_company'))); ?>
                        <?php echo $form->error($model, 'receiver_company'); ?>
                      </div>
                      <div class="form-group">
                        <?php echo $form->labelEx($model, 'receiver_address', array('class' => 'control-label')); ?>
                        <?php echo $form->textArea($model, 'receiver_address', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('receiver_address'))); ?>
                        <?php echo $form->error($model, 'receiver_address'); ?>
                      </div>
                      <table>
                      <tr>
                        <td>
                          <div class="form-group" style="padding: 2px;">
                        <?php echo $form->labelEx($model, 'receiver_city', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model, 'receiver_city', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('receiver_city'))); ?>
                        <?php echo $form->error($model, 'receiver_city'); ?>
                      </div>
                        </td>
                        <td>
                          <div class="form-group">
                        <?php echo $form->labelEx($model, 'receiver_zip', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model, 'receiver_zip', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('receiver_zip'))); ?>
                        <?php echo $form->error($model, 'receiver_zip'); ?>
                      </div>
                        </td>
                      </tr>
                      </table>
                      <div class="form-group">
                        <?php echo $form->labelEx($model, 'receiver_country', array('class' => 'control-label')); ?>
                        <?php echo $form->dropDownList($model, 'receiver_country', CHtml::listData(Country::model()->findAll(),'name','name'), array('class' => 'form-control','placeholder' => $model->getAttributeLabel('receiver_country'))); ?>
                        <?php echo $form->error($model, 'receiver_country'); ?>
                      </div>    
                      <div class="form-group">
                        <?php echo $form->labelEx($model, 'receiver_phone', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model, 'receiver_phone', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('receiver_phone'))); ?>
                        <?php echo $form->error($model, 'receiver_phone'); ?>
                      </div>
                      <div class="form-group">
                        <?php echo $form->labelEx($model, 'receiver_email', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model, 'receiver_email', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('receiver_email'))); ?>
                        <?php echo $form->error($model, 'receiver_email'); ?>
                      </div>
                    </td><!--second colspan="2" end-->                      
                  </tr>
                </tbody>

                <tfoot>
                  <tr>
                    <th colspan="2">Shipment Information</th>
                    <th colspan="2"></th>
                  </tr>
                  <tr>
                    <td colspan="2"><!--third colspan="2" start-->
                      <table>
                        <tr>
                          <td>
                            <div class="form-group" style="padding: 2px;">
                              <?php echo $form->labelEx($model, 'payment_method', array('class' => 'control-label')); ?>
                              <?php echo $form->dropDownList($model, 'payment_method', array('prepaid' => 'Prepaid','collect' => 'Collect'), array('class' => 'form-control select2', 'empty'=>'Please Type')); ?>
                              <?php echo $form->error($model, 'payment_method'); ?>
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              <?php echo $form->labelEx($model, 'shipping_ref', array('class' => 'control-label')); ?>
                              <?php echo $form->textField($model, 'shipping_ref', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('shipping_ref'))); ?>
                              <?php echo $form->error($model, 'shipping_ref'); ?>
                            </div>
                          </td>
                        </tr>
                      </table>
                      <table>
                        <tr>
                          <td>
                            <div class="form-group" style="padding: 2px;">
                              <?php echo $form->labelEx($model, 'pices', array('class' => 'control-label')); ?>
                              <?php echo $form->textField($model, 'pices', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('pices'))); ?>
                              <?php echo $form->error($model, 'pices'); ?>
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              <?php echo $form->labelEx($model, 'values', array('class' => 'control-label')); ?>
                              <?php echo $form->textField($model, 'values', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('values'))); ?>
                              <?php echo $form->error($model, 'values'); ?>
                            </div>
                          </td>
                        </tr>
                      </table>
                      <div class="form-group">
                          <?php echo $form->labelEx($model, 'remarks', array('class' => 'control-label')); ?>
                          <?php echo $form->textArea($model, 'remarks', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('remarks'))); ?>
                          <?php echo $form->error($model, 'remarks'); ?>
                      </div>
                    </td><!--third colspan="2" end-->

                    <td colspan="2"><!--four colspan="2" start-->
                      <table>
                        <tr>
                          <td>
                            <div class="form-group" style="padding: 2px;">
                              <?php echo $form->labelEx($model, 'billing_weight', array('class' => 'control-label')); ?>
                              <?php echo $form->textField($model, 'billing_weight', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('billing_weight'))); ?>
                              <?php echo $form->error($model, 'billing_weight'); ?>
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              <?php echo $form->labelEx($model, 'item_type', array('class' => 'control-label')); ?>
                              <?php echo $form->dropDownList($model, 'item_type', array('sample' => 'Sample','document' => 'Document'), array('class' => 'form-control select2', 'empty'=>'Please Type')); ?>
                              <?php echo $form->error($model, 'item_type'); ?>
                            </div>
                          </td>
                        </tr>
                      </table>
                      <div class="form-group">        
                        <?php echo $form->labelEx($model, 'dimensions', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model, 'dimensions', array('class' => 'form-control','placeholder' => 'H: X L: X W: at a time please use (X) as a separator')); ?>
                        <?php echo $form->error($model, 'dimensions'); ?>
                      </div>
                      <div class="form-group">
                        <?php echo $form->labelEx($model, 'item_description', array('class' => 'control-label')); ?>
                        <?php echo $form->textArea($model, 'item_description', array('class' => 'form-control','placeholder' => $model->getAttributeLabel('item_description'))); ?>
                        <?php echo $form->error($model, 'item_description'); ?>
                      </div>
                    </td><!--four colspan="2" end-->
                  </tr>
                </tfoot>
              </table>
              <div class="form-group text-center">        
                <input type="submit" class="btn btn-success btn-lg" value="Book Now">
              </div>
              <?php $this->endWidget(); ?>
            </div><!--panel-heading-->
          </div><!--panel panel-primary-->
        </div>
      </div>
    </div>
    <!--second part-->
  </div><!--row end-->
</div>
<!--Page container end-->

<style type="text/css">

  /*.all-color {
    background: #f2f2f2;
    margin: 0px;
    border-radius: 6px;
    border: 1px solid #ddd;
    padding: 20px;
    margin-bottom: 30px;
  }*/
  
  .navbar-form {
    margin-left: -15px!important;
  }
</style>