<page size="A4" >
<table width="99%" align="center" bordercolor="#D3D3D3" cellspacing="0" cellpadding="0" style="border-top:0px solid #D3D3D3;">
    <tr>
    <td width="30%">
        <img src="<?php echo $this->siteLogo ?>" width="190px" height="45px"><br/>
        <div style="padding:0 5px 3px 5px;">
            <i><?php echo $this->siteName ?></i><br/>
            <b>Cell : <?php echo $this->adminPhone ?></b>
        </div>
    </td>
    <td valign="top" align="center" width="30%">
        <div style="padding:10px 0 0 0; font:bold 11px verdana; color:#202F41;"></div>
    </td>
    <td align="center" width="30%">
        <?php echo BookDetails::model()->getBarCode($bookDetails->nex_code, '250px', 13) ?><br>
    </td>
    </tr>
</table>

<table width="99%" align="center" bordercolor="#D3D3D3" cellspacing="0" cellpadding="0" style="border:1px solid #D3D3D3;">
  <tr> 
    <td colspan="2"><div align="center"> 
        <table width="100%" cellspacing="0" cellpadding="0" align="center" style="border:1px solid #D3D3D3;">
          <tr bgcolor="#525252"> 
            <td width="20%" height="20" style="border:1px solid #D3D3D3;"><div align="center"><strong><font color="#FFFFFF">A/C NO.</font></strong></div></td>
            <td width="14%" style="border:1px solid #D3D3D3;"><div align="center"><strong><font color="#FFFFFF">ORIGIN</font></strong></div></td>
            <td width="14%" style="border:1px solid #D3D3D3;"><div align="center"><strong><font color="#FFFFFF">DESTINATION</font></strong></div></td>
            <td width="13%" style="border:1px solid #D3D3D3;"><div align="center"><strong><font color="#FFFFFF">SERVICE</font></strong></div></td>
            <td width="8%" style="border:1px solid #D3D3D3;"><div align="center"><strong><font color="#FFFFFF">DOX/SPX</font></strong></div></td>
            <td width="8%" style="border:1px solid #D3D3D3;"><div align="center"><strong><font color="#FFFFFF">PIECES</font></strong></div></td>
            <td width="8%" style="border:1px solid #D3D3D3;"><div align="center"><strong><font color="#FFFFFF">WEIGHT(KG)</font></strong></div></td>
            <td width="15%" style="border:1px solid #D3D3D3;"><div align="center"><strong><font color="#FFFFFF">DATE</font></strong></div></td>
          </tr>
          <tr> 
            <td width="20%" height="22" style="border:1px solid #D3D3D3;"><div align="center"><span id="n">&nbsp;</span></div></td>
            <td width="14%" style="border:1px solid #D3D3D3;"><div align="center"><span id="n"><?=$bookDetails->sender_country;?></span></div></td>
            <td width="14%" style="border:1px solid #D3D3D3;"><div align="center"><span id="n"><?=$bookDetails->receiver_country;?></span></div></td>
            <td width="13%" style="border:1px solid #D3D3D3;"><div align="center"><span id='n'><b></b></span></div></td>
            <td width="8%" style="border:1px solid #D3D3D3;"><div align="center"><span id="n">SPX</span></div></td>
            <td width="8%" style="border:1px solid #D3D3D3;"><div align="center"><span id="n"><?=$bookDetails->pices;?></span></div></td>
            <td width="8%" style="border:1px solid #D3D3D3;"><div align="center"><span id="n"><?=$bookDetails->billing_weight;?></span></div></td>
            <td width="15%" style="border:1px solid #D3D3D3;"><div align="center">&nbsp;<span id="n"><?=date("d-m-Y g:i:s A",strtotime($bookDetails->entry_date))?></span></div></td>
          </tr>
        </table>
      </div></td>
    <!--380-->
  </tr>
  <tr style="padding-left:3px;"> 
    <td width="49%" height="25" style="border:1px solid #D3D3D3;">
        <center><span id="b">Sender Details</span></center>
    </td>
    <td width="51%" style="border:1px solid #D3D3D3;">
        <center><span id="b">Recepint Details</span></center>
    </td>
  </tr>
  <tr style="padding-left:3px;"> 
    <td width="49%" height="25" style="border:1px solid #D3D3D3;">
        <span id="c">Company :</span>&nbsp;<span id="n"><?=$bookDetails->sender_company;?></span>
    </td>
    <td width="51%" style="border:1px solid #D3D3D3;">
        <span id="c">Company :</span>&nbsp;<span id="n"><?=$bookDetails->receiver_company;?></span>
    </td>
  </tr>
  <tr style="padding-left:3px;">
    <td  height="25" style="border:1px solid #D3D3D3;">
        <span id="c">Address :</span>&nbsp;<span id="n"><?=$bookDetails->sender_address;?></span>
    </td>
    <td  style="border:1px solid #D3D3D3;">
        <span id="c">Address :</span>&nbsp;<span id="n"><span id="n"><?=$bookDetails->receiver_address;?></span></td>
  </tr>
  <tr style="padding-left:3px;"> 
    <td height="25" style="border:1px solid #D3D3D3;">
        <span id="c">Address :</span>&nbsp;<span id="n"><?=$bookDetails->sender_address;?></span></td>
    <td style="border:1px solid #D3D3D3;">
        <span id="c">Address :</span>&nbsp;<span id="n"><?=$bookDetails->receiver_address;?></span>
    </td>
  </tr>

  <tr style="padding-left:3px;"> 
    <td height="25">
      <table width="100%" height="100%"cellspacing="0" cellpadding="0">
        <tr style="padding-left:3px;"> 
            <td width="50%" style="border:1px solid #D3D3D3;">
                <span id="c">City :</span>&nbsp;<span id="n"><?=$bookDetails->sender_city;?></span>
            </td>
            <td width="50%" style="border:1px solid #D3D3D3;">
                <span id="c">ZIP Code :</span>&nbsp;<span id="n"><?=$bookDetails->sender_zip;?></span>
            </td>
        </tr>
      </table>
     </td>
    <td height="25">
      <table width="100%" height="100%" cellspacing="0" cellpadding="0">
        <tr style="padding-left:3px;"> 
            <td width="50%" style="border:1px solid #D3D3D3;">
                <span id="c">City :</span>&nbsp;<span id="n"><?=$bookDetails->receiver_city;?></span>
            </td>
            <td width="50%" style="border:1px solid #D3D3D3;">
                <span id="c">ZIP Code :</span>&nbsp;<span id="n"><?=$bookDetails->receiver_zip;?></span>
            </td>
        </tr>
      </table>
     </td>
    </tr>

  <tr style="padding-left:3px;"> 
    <td height="25"  style="border:1px solid #D3D3D3;">
        <span id="c">Country :</span>&nbsp;<span id="n"><?=$bookDetails->sender_country;?></span>
    </td>
    <td style="border:1px solid #D3D3D3;">
        <span id="c">Country :</span>&nbsp;<span id="n"><?=$bookDetails->receiver_country;?></span>
    </td>
  </tr>
  <tr style="padding-left:3px;"> 
    <td height="25" style="border:1px solid #D3D3D3;">
        <span id="c">Sender's Name</span>&nbsp;<span id="n"><?=$bookDetails->sender_attention;?></span>
    </td>
    <td style="border:1px solid #D3D3D3;">
        <span id="c">Recipient's Name</span>&nbsp;<span id="n"><?=$bookDetails->receiver_attention;?></span>
    </td>
  </tr>
  <tr style="padding-left:3px;"> 
    <td height="25">
        <table width="100%" height="100%" cellspacing="0" cellpadding="0">
        <tr style="padding-left:3px;"> 
            <td width=50% style="border:1px solid #D3D3D3;">
                <span id="c">TEL :</span>&nbsp;<span id="n"><?=$bookDetails->sender_phone;?></span>
            </td>
            <td width=50% style="border:1px solid #D3D3D3;">
                <span id="c">Cel :</span>&nbsp;<span id="n"></span>
            </td>
        </tr>
        </table>
    </td>
    <td> 
        <table width="100%" height="100%" cellspacing="0" cellpadding="0">
        <tr style="padding-left:3px;"> 
          <td width="50%" style="border:1px solid #D3D3D3;">
            <span id="c">TEL :</span>&nbsp;<span id="n"><?=$bookDetails->receiver_phone;?></span>
        </td>
          <td width="50%" style="border:1px solid #D3D3D3;">
            <span id="c">Cel :</span>&nbsp;<span id="n"></span>
        </td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td  height="100" colspan="2">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:0px solid #D3D3D3;">
        <tr> 
            <td width="84%" height="55" valign="top" style="border:1px solid #D3D3D3;">
                <span id="c">REMARKS/ DESCRIPTION OF GOODS</span><br/><span id="n"><?=$bookDetails->remarks;?></span>
            </td>
            <td width="16%" rowspan="3" valign="top">
                <table width="100%" border="0"cellspacing="0" cellpadding="0">
                <tr> 
                    <td width="50%" height="20" colspan="2" style="border:1px solid #D3D3D3;">
                        <center><span id="b">CHARGES</span></center>
                    </td>
                </tr>
                <tr> 
                    <td width="50%" height="20" style="border:1px solid #D3D3D3;">
                        <span id="c">VALUE</span>
                    </td>
                    <td style="border:1px solid #D3D3D3;">&nbsp;
                        <span id="n">$&nbsp;<?=$bookDetails->values;?></span>
                    </td>
                </tr>
                <tr> 
                    <td height="20" style="border:1px solid #D3D3D3;">
                        <span id="c">CC/PP</span></td>
                    <td style="border:1px solid #D3D3D3;">&nbsp;
                        <span id="n"><?=$bookDetails->payment_method;?></span>
                    </td>
                </tr>
                <tr> 
                    <td height="30" style="border:1px solid #D3D3D3;">
                        <span id="c">BILL AMNT</span>
                    </td>
                    <td style="border:1px solid #D3D3D3;">&nbsp;
                        <span id="n"><b></b></span>
                    </td>
                </tr>
                <tr> 
                    <td colspan="2" height="25" style="border:1px solid #D3D3D3;">
                        <div align="center">
                            <strong><font color="#FF0000" size="1.2">COLLECT</font></strong>
                        </div>
                    </td>
                </tr>
                <tr> 
                    <td colspan="2" height="25" style="border:1px solid #D3D3D3;">&nbsp;
                        <span id="n">$=&nbsp;0</span>
                    </td>
                </tr>
                </table>
            </td>
        </tr>
        <tr> 
            <td height="100%">
                <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #D3D3D3;">
                    <tr>
                        <td width="49%" height="100%">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" height="50" style="border-right:2px solid #D3D3D3;">
                            <tr> 
                                <td colspan="7" valign="top">
                                    <div align="center">
                                    <font size="1.2">
                                        <strong>DIMENSIONS<br/>(VOLUMETRIC WEIGHT)</strong>
                                    </font>
                                    </div>
                                </td>
                            </tr>
                            <tr> 
                                <td>
                                    <div align="right">15 
                                        <font size="1.2"><strong>Cm</strong></font>
                                    </div>
                                </td>
                                <td>
                                    <div align="center"> 
                                        <font size="1.2"><strong>X</strong></font>
                                    </div>
                                </td>
                                <td>
                                    <div align="right">40 
                                        <font size="1.2"><strong>Cm</strong></font>
                                    </div>
                                </td>
                                <td>
                                    <div align="center"> 
                                        <font size="1.2"><strong>X</strong></font>
                                    </div>
                                </td>
                                <td>
                                    <div align="right">20 
                                        <font size="1.2"><strong>Cm</strong></font>
                                    </div>
                                </td>
                                <td>
                                    <div align="center"> 
                                        <font size="1.2"><strong>=</strong></font>
                                    </div>
                                </td>
                                <td>
                                    <div align="right">2.40 
                                        <font size="1.2"><strong>KGS</strong></font>
                                    </div>
                                </td>
                            </tr>
                            </table>
                        </td>
                        <td width="51%" height="100%">
                            <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr> 
                                <td width="27%" style="font-size:10px;" valign="top">
                                    <strong>&nbsp;CONSIGNEE</strong>
                                </td>
                                <td width="24%" rowspan="2" valign="top">&nbsp;</td>
                                <td  width="24%" rowspan="2" valign="top" style="font-size:10px;">
                                    <strong>DATE</strong>
                                </td>      
                                <td  width="21%" valign="top" style="font-size:10px;">
                                    <strong>TIME</strong>
                                </td>
                            </tr>
                            </table>
    
                        </td>
                    </tr>
                </table>          
            </td>
        </tr>
        <tr> 
            <td height="100%">
                <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #D3D3D3;">
                    <tr> 
                        <td width="15%" align="left">
                            <strong>&nbsp;SHIPPER:</strong><br>
                            <strong>&nbsp;DATE/TIME:</strong>
                        </td>
                        <td width="34%" rowspan="2" style="border-right:2px solid #D3D3D3;">&nbsp;
                        </td>
                        <td width="51%" rowspan="2">
                            <strong>&nbsp;PICK UP BY :</strong>&nbsp;
                            <span id="n"></span><br>
                            <strong>&nbsp;DATE/TIME :</strong>&nbsp;
                            <span id="n"><?=date("d-m-Y g:i:s A",strtotime($bookDetails->entry_date))?></span></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </td>
  </tr>
</table>

<br/><br/>

<table width="99%" align="center" bordercolor="#D3D3D3" cellspacing="0" cellpadding="0" style="border-top:0px solid #D3D3D3;">
    <tr>
    <td width="30%">
        <img src="<?php echo $this->siteLogo ?>" width="190px" height="45px"><br/>
        <div style="padding:0 5px 3px 5px;">
            <i><?php echo $this->siteName ?></i><br/>
            <b>Cell : <?php echo $this->adminPhone ?></b>
        </div>
    </td>
    <td valign="top" align="center" width="30%">
        <div style="padding:10px 0 0 0; font:bold 11px verdana; color:#202F41;"></div>
    </td>
    <td align="center" width="30%">
        <?php echo BookDetails::model()->getBarCode($bookDetails->nex_code, '250px', 13) ?><br>
    </td>
    </tr>
</table>

<table width="99%" align="center" bordercolor="#D3D3D3" cellspacing="0" cellpadding="0" style="border:1px solid #D3D3D3;">
  <tr> 
    <td colspan="2"><div align="center"> 
        <table width="100%" cellspacing="0" cellpadding="0" align="center" style="border:1px solid #D3D3D3;">
          <tr bgcolor="#525252"> 
            <td width="20%" height="20" style="border:1px solid #D3D3D3;"><div align="center"><strong><font color="#FFFFFF">A/C NO.</font></strong></div></td>
            <td width="14%" style="border:1px solid #D3D3D3;"><div align="center"><strong><font color="#FFFFFF">ORIGIN</font></strong></div></td>
            <td width="14%" style="border:1px solid #D3D3D3;"><div align="center"><strong><font color="#FFFFFF">DESTINATION</font></strong></div></td>
            <td width="13%" style="border:1px solid #D3D3D3;"><div align="center"><strong><font color="#FFFFFF">SERVICE</font></strong></div></td>
            <td width="8%" style="border:1px solid #D3D3D3;"><div align="center"><strong><font color="#FFFFFF">DOX/SPX</font></strong></div></td>
            <td width="8%" style="border:1px solid #D3D3D3;"><div align="center"><strong><font color="#FFFFFF">PIECES</font></strong></div></td>
            <td width="8%" style="border:1px solid #D3D3D3;"><div align="center"><strong><font color="#FFFFFF">WEIGHT(KG)</font></strong></div></td>
            <td width="15%" style="border:1px solid #D3D3D3;"><div align="center"><strong><font color="#FFFFFF">DATE</font></strong></div></td>
          </tr>
          <tr> 
            <td width="20%" height="22" style="border:1px solid #D3D3D3;"><div align="center"><span id="n">&nbsp;</span></div></td>
            <td width="14%" style="border:1px solid #D3D3D3;"><div align="center"><span id="n"><?=$bookDetails->sender_country;?></span></div></td>
            <td width="14%" style="border:1px solid #D3D3D3;"><div align="center"><span id="n"><?=$bookDetails->receiver_country;?></span></div></td>
            <td width="13%" style="border:1px solid #D3D3D3;"><div align="center"><span id='n'><b></b></span></div></td>
            <td width="8%" style="border:1px solid #D3D3D3;"><div align="center"><span id="n">SPX</span></div></td>
            <td width="8%" style="border:1px solid #D3D3D3;"><div align="center"><span id="n"><?=$bookDetails->pices;?></span></div></td>
            <td width="8%" style="border:1px solid #D3D3D3;"><div align="center"><span id="n"><?=$bookDetails->billing_weight;?></span></div></td>
            <td width="15%" style="border:1px solid #D3D3D3;"><div align="center">&nbsp;<span id="n"><?=date("d-m-Y g:i:s A",strtotime($bookDetails->entry_date))?></span></div></td>
          </tr>
        </table>
      </div></td>
    <!--380-->
  </tr>
  <tr style="padding-left:3px;"> 
    <td width="49%" height="25" style="border:1px solid #D3D3D3;">
        <center><span id="b">Sender Details</span></center>
    </td>
    <td width="51%" style="border:1px solid #D3D3D3;">
        <center><span id="b">Recepint Details</span></center>
    </td>
  </tr>
  <tr style="padding-left:3px;"> 
    <td width="49%" height="25" style="border:1px solid #D3D3D3;">
        <span id="c">Company :</span>&nbsp;<span id="n"><?=$bookDetails->sender_company;?></span>
    </td>
    <td width="51%" style="border:1px solid #D3D3D3;">
        <span id="c">Company :</span>&nbsp;<span id="n"><?=$bookDetails->receiver_company;?></span>
    </td>
  </tr>
  <tr style="padding-left:3px;">
    <td  height="25" style="border:1px solid #D3D3D3;">
        <span id="c">Address :</span>&nbsp;<span id="n"><?=$bookDetails->sender_address;?></span>
    </td>
    <td  style="border:1px solid #D3D3D3;">
        <span id="c">Address :</span>&nbsp;<span id="n"><span id="n"><?=$bookDetails->receiver_address;?></span></td>
  </tr>
  <tr style="padding-left:3px;"> 
    <td height="25" style="border:1px solid #D3D3D3;">
        <span id="c">Address :</span>&nbsp;<span id="n"><?=$bookDetails->sender_address;?></span></td>
    <td style="border:1px solid #D3D3D3;">
        <span id="c">Address :</span>&nbsp;<span id="n"><?=$bookDetails->receiver_address;?></span>
    </td>
  </tr>

  <tr style="padding-left:3px;"> 
    <td height="25">
      <table width="100%" height="100%"cellspacing="0" cellpadding="0">
        <tr style="padding-left:3px;"> 
            <td width="50%" style="border:1px solid #D3D3D3;">
                <span id="c">City :</span>&nbsp;<span id="n"><?=$bookDetails->sender_city;?></span>
            </td>
            <td width="50%" style="border:1px solid #D3D3D3;">
                <span id="c">ZIP Code :</span>&nbsp;<span id="n"><?=$bookDetails->sender_zip;?></span>
            </td>
        </tr>
      </table>
     </td>
    <td height="25">
      <table width="100%" height="100%" cellspacing="0" cellpadding="0">
        <tr style="padding-left:3px;"> 
            <td width="50%" style="border:1px solid #D3D3D3;">
                <span id="c">City :</span>&nbsp;<span id="n"><?=$bookDetails->receiver_city;?></span>
            </td>
            <td width="50%" style="border:1px solid #D3D3D3;">
                <span id="c">ZIP Code :</span>&nbsp;<span id="n"><?=$bookDetails->receiver_zip;?></span>
            </td>
        </tr>
      </table>
     </td>
    </tr>

  <tr style="padding-left:3px;"> 
    <td height="25"  style="border:1px solid #D3D3D3;">
        <span id="c">Country :</span>&nbsp;<span id="n"><?=$bookDetails->sender_country;?></span>
    </td>
    <td style="border:1px solid #D3D3D3;">
        <span id="c">Country :</span>&nbsp;<span id="n"><?=$bookDetails->receiver_country;?></span>
    </td>
  </tr>
  <tr style="padding-left:3px;"> 
    <td height="25" style="border:1px solid #D3D3D3;">
        <span id="c">Sender's Name</span>&nbsp;<span id="n"><?=$bookDetails->sender_attention;?></span>
    </td>
    <td style="border:1px solid #D3D3D3;">
        <span id="c">Recipient's Name</span>&nbsp;<span id="n"><?=$bookDetails->receiver_attention;?></span>
    </td>
  </tr>
  <tr style="padding-left:3px;"> 
    <td height="25">
        <table width="100%" height="100%" cellspacing="0" cellpadding="0">
        <tr style="padding-left:3px;"> 
            <td width=50% style="border:1px solid #D3D3D3;">
                <span id="c">TEL :</span>&nbsp;<span id="n"><?=$bookDetails->sender_phone;?></span>
            </td>
            <td width=50% style="border:1px solid #D3D3D3;">
                <span id="c">Cel :</span>&nbsp;<span id="n"></span>
            </td>
        </tr>
        </table>
    </td>
    <td> 
        <table width="100%" height="100%" cellspacing="0" cellpadding="0">
        <tr style="padding-left:3px;"> 
          <td width="50%" style="border:1px solid #D3D3D3;">
            <span id="c">TEL :</span>&nbsp;<span id="n"><?=$bookDetails->receiver_phone;?></span>
        </td>
          <td width="50%" style="border:1px solid #D3D3D3;">
            <span id="c">Cel :</span>&nbsp;<span id="n"></span>
        </td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td  height="100" colspan="2">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:0px solid #D3D3D3;">
        <tr> 
            <td width="84%" height="55" valign="top" style="border:1px solid #D3D3D3;">
                <span id="c">REMARKS/ DESCRIPTION OF GOODS</span><br/><span id="n"><?=$bookDetails->remarks;?></span>
            </td>
            <td width="16%" rowspan="3" valign="top">
                <table width="100%" border="0"cellspacing="0" cellpadding="0">
                <tr> 
                    <td width="50%" height="20" colspan="2" style="border:1px solid #D3D3D3;">
                        <center><span id="b">CHARGES</span></center>
                    </td>
                </tr>
                <tr> 
                    <td width="50%" height="20" style="border:1px solid #D3D3D3;">
                        <span id="c">VALUE</span>
                    </td>
                    <td style="border:1px solid #D3D3D3;">&nbsp;
                        <span id="n">$&nbsp;<?=$bookDetails->values;?></span>
                    </td>
                </tr>
                <tr> 
                    <td height="20" style="border:1px solid #D3D3D3;">
                        <span id="c">CC/PP</span></td>
                    <td style="border:1px solid #D3D3D3;">&nbsp;
                        <span id="n"><?=$bookDetails->payment_method;?></span>
                    </td>
                </tr>
                <tr> 
                    <td height="30" style="border:1px solid #D3D3D3;">
                        <span id="c">BILL AMNT</span>
                    </td>
                    <td style="border:1px solid #D3D3D3;">&nbsp;
                        <span id="n"><b></b></span>
                    </td>
                </tr>
                <tr> 
                    <td colspan="2" height="25" style="border:1px solid #D3D3D3;">
                        <div align="center">
                            <strong><font color="#FF0000" size="1.2">COLLECT</font></strong>
                        </div>
                    </td>
                </tr>
                <tr> 
                    <td colspan="2" height="25" style="border:1px solid #D3D3D3;">&nbsp;
                        <span id="n">$=&nbsp;0</span>
                    </td>
                </tr>
                </table>
            </td>
        </tr>
        <tr> 
            <td height="100%">
                <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #D3D3D3;">
                    <tr>
                        <td width="49%" height="100%">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" height="50" style="border-right:2px solid #D3D3D3;">
                            <tr> 
                                <td colspan="7" valign="top">
                                    <div align="center">
                                    <font size="1.2">
                                        <strong>DIMENSIONS<br/>(VOLUMETRIC WEIGHT)</strong>
                                    </font>
                                    </div>
                                </td>
                            </tr>
                            <tr> 
                                <td>
                                    <div align="right">15 
                                        <font size="1.2"><strong>Cm</strong></font>
                                    </div>
                                </td>
                                <td>
                                    <div align="center"> 
                                        <font size="1.2"><strong>X</strong></font>
                                    </div>
                                </td>
                                <td>
                                    <div align="right">40 
                                        <font size="1.2"><strong>Cm</strong></font>
                                    </div>
                                </td>
                                <td>
                                    <div align="center"> 
                                        <font size="1.2"><strong>X</strong></font>
                                    </div>
                                </td>
                                <td>
                                    <div align="right">20 
                                        <font size="1.2"><strong>Cm</strong></font>
                                    </div>
                                </td>
                                <td>
                                    <div align="center"> 
                                        <font size="1.2"><strong>=</strong></font>
                                    </div>
                                </td>
                                <td>
                                    <div align="right">2.40 
                                        <font size="1.2"><strong>KGS</strong></font>
                                    </div>
                                </td>
                            </tr>
                            </table>
                        </td>
                        <td width="51%" height="100%">
                            <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr> 
                                <td width="27%" style="font-size:10px;" valign="top">
                                    <strong>&nbsp;CONSIGNEE</strong>
                                </td>
                                <td width="24%" rowspan="2" valign="top">&nbsp;</td>
                                <td  width="24%" rowspan="2" valign="top" style="font-size:10px;">
                                    <strong>DATE</strong>
                                </td>      
                                <td  width="21%" valign="top" style="font-size:10px;">
                                    <strong>TIME</strong>
                                </td>
                            </tr>
                            </table>
    
                        </td>
                    </tr>
                </table>          
            </td>
        </tr>
        <tr> 
            <td height="100%">
                <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #D3D3D3;">
                    <tr> 
                        <td width="15%" align="left">
                            <strong>&nbsp;SHIPPER:</strong><br>
                            <strong>&nbsp;DATE/TIME:</strong>
                        </td>
                        <td width="34%" rowspan="2" style="border-right:2px solid #D3D3D3;">&nbsp;
                        </td>
                        <td width="51%" rowspan="2">
                            <strong>&nbsp;PICK UP BY :</strong>&nbsp;
                            <span id="n"></span><br>
                            <strong>&nbsp;DATE/TIME :</strong>&nbsp;
                            <span id="n"><?=date("d-m-Y g:i:s A",strtotime($bookDetails->entry_date))?></span></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </td>
  </tr>
</table>

<table width="99%" align="center" bordercolor="#D3D3D3" cellspacing="0" cellpadding="0" style="border:0px solid #D3D3D3;">
    <tr>
        <td>
            <center>
                <span style="font-family:verdana; font-weight:normal; font-size:8px; color:#d6d6d6;">Powered By Nexces Express</span>
            </center>
        </td>
    </tr>
</table>
</page>


<style type="text/css">
td{font-family:verdana;font-size:10px;color:#000000;}
#b{font-family:verdana;font-weight:bold;font-size:10px;color:#C00000;}
#c{font-family:verdana;font-weight:bold;font-size:10px;color:#000000;padding-left:5px;}
#p{font-family:verdana;font-weight:bold;font-size:10px;color:#000000;padding-left:5px;border-right:1px;}
#n{font-family:verdana;font-weight:normal;font-size:10px;color:#0033CC;padding-left:2px;}

page {
  background: white;
  display: block;
  /* margin: 0 auto; */
  margin: 0.0cm;
  box-shadow: 0 0 0.2cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
  width: 21.8cm;
  height: 27.9cm; 
  margin-left: -0.1cm;
}
page[size="A4"][layout="portrait"] {
  width: 29.7cm;
  /*  height: 10cm;  */
  height: 21cm;  
}
@media print {
  body, page {
    margin: 0;
    box-shadow: 0;
  }
}
</style> 