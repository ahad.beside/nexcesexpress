<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <div class="about-all-back">
    <div class="about-head-top-text">
    </div>
    <div class="famous-poem">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <form class="navbar-form" action="<?= Yii::app()->createUrl('//site/track')?>" method="get" accept-charset="utf-8">
            <div class="input-group add-on">      
              <input class="form-control"  placeholder="Search Tracking Number" type="text" name="trackNo" value="<?= $_GET['trackNo']?>">
              <div class="input-group-btn">
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>