<!--Page Info-->
<section class="page-info">
  <div class="auto-container clearfix">
    <div class="col-md-6">
      <div class="pull-left">
        <h3>Shipping</h3>
      </div>
    </div>
    <div class="col-md-6">
      <div class="pull-right">
        <ul class="bread-crumb clearfix">
          <li><a href="<?= Yii::app()->homeUrl;?>">Home</a></li>
          <?php /*<li><a href="<?= Yii::app()->createUrl('//contact');?>">Contact</a></li>*/?>
          <li><a href="<?= Yii::app()->homeUrl;?>">Shipping</a></li>
          <li><?=$shipping->name?></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<!--Page Info end-->


<!--Page container start-->
<div class="container">
  <div class="row all-color">
    <!--First part-->
    <?php $this->renderPartial('trackSearch', array('model'=>$model)); ?>
    <!--First part-->

    <!--second part-->
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
      <div class="about-all-back">
        <div class="about-head-top-text">
        </div>
        <div class="famous-poem">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <h2 style="padding-left:12px; text-align: center;">Your Booking No. <?= $data['bookingInfo']->nex_code;?> is Confirmed</h2>
                </div>
              </div>

              <table class="table table-bordered table-responsive" style="color: black;">      
                <tbody>
                  <tr>
                    <td colspan="2">
                        <div class="panel-heading">
                          <h3 style="text-align: center;">
                            <!-- <span class="glyphicon glyphicon-chevron-right"></span> --> 
                            Thanks for your Booking No. <?= $data['bookingInfo']->nex_code;?> is Confirmed
                          </h3>
                        </div>
                    </td>
                  </tr>

                  <tr>
                    <td style="width:50%">
                    <table class="table table-borderless table-responsive">
                      <tbody>
                        <tr>
                          <td style="width:25%; border:0;"><label class="control-label">Sender</label></td>
                          <td style="width:1%; border:0;"><label class="control-label">:</label></td>
                          <td style="border:0;"><label class="control-label"><?= $data['bookingInfo']->sender_company;?></label></td>
                        </tr>
                        <tr>
                          <td style="width:14%;"><label class="control-label">Attn</label></td>
                          <td style="width:1%;"><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->sender_attention;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Email</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->sender_email;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Address</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->sender_address;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Tel</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->sender_phone;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">City</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->sender_city;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Country</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->sender_country;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Tracking</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->nex_code;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Booking</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->item_type;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Pay Mode</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->payment_method;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">PCS</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->pices;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Remarks</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->remarks;?></label></td>
                        </tr>
                      </tbody>
                    </table>
                    </td>

                    <td style="width:50%">
                    <table class="table table-borderless table-responsive">
                      <tbody>
                        <tr>
                          <td style="width:25%; border:0;"><label class="control-label">Recipient</label></td>
                          <td style="width:1%; border:0;"><label class="control-label">:</label></td>
                          <td style="border:0;"><label class="control-label"><?= $data['bookingInfo']->receiver_company;?></label></td>
                        </tr>  
                        <tr>
                          <td style="width:14%;"><label class="control-label">Attn</label></td>
                          <td style="width:1%;"><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->receiver_attention;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Email</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->receiver_email;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Address</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->receiver_address;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Tel</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->receiver_phone;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">City</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->receiver_city;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Destination</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->dimensions;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Shipper Ref </label></td>
                          <td><label class="control-label">:</label></td>
                          <td style=""><label class="control-label"><?= $data['bookingInfo']->shipping_ref;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Weight (KG)</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->billing_weight;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Items</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->receiver_company;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Content</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->receiver_company;?></label></td>
                        </tr>
                        <tr>
                          <td><label class="control-label">Value</label></td>
                          <td><label class="control-label">:</label></td>
                          <td><label class="control-label"><?= $data['bookingInfo']->values;?></label> USD</td>
                        </tr>
                      </tbody>
                    </table>
                    </td>
                  </tr>

                  <tr>
                    <td colspan="2">
                      <a class="btn btn-default" href="<?= Yii::app()->createUrl('//shipping/OnlineBooking');?>"><span class="glyphicon glyphicon-plus"></span> Book Another</a>
                      <a class="btn btn-default" href="<?= $this->createUrl('downloadBooking', array('id' => $data['bookingInfo']->id)) ?>" data-url="<?= $this->createUrl('downloadBooking', array('id' => $data['bookingInfo']->id)) ?>" onclick="//downloadInvoice($(this));" target="_blank" class="btn btn-sm" style="text-align:left; font-weight: normal"><span class="glyphicon glyphicon-save"></span> Download</a> 
                      <a class="btn btn-default" href="<?= Yii::app()->createUrl('//shipping/print/' . $data['bookingInfo']->id) ?>" target="_blank"><span class="glyphicon glyphicon-print"></span> Print</a>
                    </td>
                  </tr>
                </tbody>
              </table>

            </div><!--panel-heading-->
          </div><!--panel panel-primary-->
        </div>
      </div>
    </div>
    <!--second part-->
  </div><!--row end-->
</div>
<!--Page container end-->

<style type="text/css">

  /*.all-color {
    background: #f2f2f2;
    margin: 0px;
    border-radius: 6px;
    border: 1px solid #ddd;
    padding: 20px;
    margin-bottom: 30px;
  }*/
  
  .navbar-form {
    margin-left: -15px!important;
  }
</style>