<div class="container">
      <div class="sec-title text-center">
        <h2><?=$model->name?></h2>
        <span class="colorborder"></span> </div>
      <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
          <div class="single-servicebox">
            <div class=""> <?= Yii::app()->easycode->showImage($model->image,500,400,true,true,Yii::app()->params->clientDir)?></div>
              </a> </div>
            <div class="servicebox-content">
              <h3><a href="<?= Service::model()->makeLink($model->id)?>"><?=$model->name?></a></h3>
              <?= $model->details;?>
            </div>
          </div>
        </div>
        
      </div>
      
    </div>