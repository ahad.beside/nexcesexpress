-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2020 at 07:56 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nexpress`
--

-- --------------------------------------------------------

--
-- Table structure for table `book_details`
--

CREATE TABLE `book_details` (
  `id` int(11) NOT NULL,
  `nex_code` varchar(255) NOT NULL,
  `sender_attention` varchar(255) NOT NULL,
  `sender_company` varchar(255) NOT NULL,
  `sender_address` text NOT NULL,
  `sender_city` varchar(100) NOT NULL,
  `sender_zip` varchar(50) NOT NULL,
  `sender_country` varchar(100) NOT NULL,
  `sender_phone` varchar(50) NOT NULL,
  `sender_email` varchar(255) NOT NULL,
  `receiver_attention` varchar(255) NOT NULL,
  `receiver_company` varchar(255) NOT NULL,
  `receiver_address` text NOT NULL,
  `receiver_city` varchar(100) NOT NULL,
  `receiver_zip` varchar(50) NOT NULL,
  `receiver_country` varchar(100) NOT NULL,
  `receiver_phone` varchar(50) NOT NULL,
  `receiver_email` varchar(255) NOT NULL,
  `payment_method` varchar(100) NOT NULL,
  `shipping_ref` varchar(255) NOT NULL,
  `pices` int(11) NOT NULL,
  `values` int(11) NOT NULL,
  `remarks` text NOT NULL,
  `billing_weight` decimal(11,2) NOT NULL,
  `item_type` varchar(50) NOT NULL,
  `item_description` text NOT NULL,
  `dimensions` varchar(100) NOT NULL,
  `ready_datetime` datetime NOT NULL,
  `arrival_datetime` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `entry_by` int(11) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book_details`
--

INSERT INTO `book_details` (`id`, `nex_code`, `sender_attention`, `sender_company`, `sender_address`, `sender_city`, `sender_zip`, `sender_country`, `sender_phone`, `sender_email`, `receiver_attention`, `receiver_company`, `receiver_address`, `receiver_city`, `receiver_zip`, `receiver_country`, `receiver_phone`, `receiver_email`, `payment_method`, `shipping_ref`, `pices`, `values`, `remarks`, `billing_weight`, `item_type`, `item_description`, `dimensions`, `ready_datetime`, `arrival_datetime`, `status`, `entry_by`, `entry_date`, `update_by`, `update_time`) VALUES
(1, 'NX201909220', 'ahad', 'mayagroup', '54/A, Sontongor', 'Jikatola', '1209', '18', '01911160045', 'mayagroupint@gmail.com', 'MD. Saiful Islam', 'Self Compnay', 'Bhurian', 'Bhurian', 'BF120', '17', '01735121121', 'saiful@gmail.com', 'prepaid', 'Monir', 1, 20, 'Good', '10.00', 'document', 'Excellent', '12X10X5', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0, '2019-09-22 10:53:49', 0, '2019-09-22 10:53:50'),
(2, 'NX201909221', 'asas', '', '', '', '', 'American Samoa', '', '', '', '', '', '', '', 'Albania', '', '', '', '', 0, 0, '', '0.00', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, '2019-09-22 11:28:24', 0, '2019-09-22 11:28:24'),
(3, 'NX201909222', 'MD. kamal', 'KPS', 'Hazaribag', 'Hazaribag', '1205', 'Afghanistan', '01919806114', 'kps@gmail.com', 'MD. Sujon', 'Talioffice', 'Tali', 'Dhanmondi', '1201', 'Bahamas', '01735121121', 'sujon@gmail.com', 'prepaid', 'Monir', 1, 11, 'Good', '2.00', 'sample', 'Excellent', '9X12X7', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '2019-09-22 14:00:55', 0, '2019-09-22 14:00:55'),
(4, 'NX201909223', 'as', 'ass', '', '', '', 'Afghanistan', '', '', '', '', '', '', '', 'Afghanistan', '', '', '', '', 0, 0, '', '0.00', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '2019-09-22 14:53:26', 0, '2019-09-22 14:53:26');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `title` varchar(300) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `site_map` longtext NOT NULL,
  `slug` text NOT NULL,
  `meta_keyword` varchar(500) NOT NULL,
  `meta_description` varchar(500) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `entry_by` int(11) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `attachment_title` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `title`, `name`, `description`, `site_map`, `slug`, `meta_keyword`, `meta_description`, `status`, `entry_by`, `entry_date`, `update_by`, `update_time`, `attachment_title`, `attachment`) VALUES
(1, 'Bangladesh Office', 'Bangladesh', '<p>House # 35(Fast Floor), <br/>Road # 17, Nikunja-2,<br/> Khilkhet, Dhaka-1229</p>\r\n\r\n<p>+88 01955300065<br />\r\nSales : amrjtrade.mq@gmail.com</p>\r\n', '<p><iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3068.908209410486!2d90.41361962442834!3d23.833974709291017!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c65e2760a6cf%3A0x6c7e13ee45159078!2s1229%20Rd%20No.%2017%2C%20Dhaka%201230!5e0!3m2!1sen!2sbd!4v1567223746916!5m2!1sen!2sbd\" width=\"600\" height=\"200\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe></p>\r\n', 'bangladesh-office', '', '', 1, 0, '2017-12-22 22:52:32', 1, '2019-07-05 11:00:11', '', '0000-00-00 00:00:00'),
(2, 'China Office', 'China', '<p>Dallas/Fort Worth and Houston 106<br />\r\nN. Denton Tap Rd Suite 210-147<br />\r\nCoppell, TX 75019<br />\r\n972-379-8317 TX<br />\r\nSales : contact@khsitsolutions.com</p>\r\n', '<p><iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3023.7905570653197!2d-73.56744308515925!3d40.72262694490322!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c27de37d7753ed%3A0x25d8888e33b584bb!2s1936+Hempstead+Turnpike+%23237%2C+East+Meadow%2C+NY+11554%2C+USA!5e0!3m2!1sen!2sbd!4v1564210113632!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe></p>\r\n', 'china-office', '', '', 1, 0, '2017-12-22 22:52:58', 1, '2019-07-05 11:02:14', '', '0000-00-00 00:00:00'),
(3, 'Hongkong Office', 'Hongkong', '<p>Quick Link Global Logistics Limited<br/>\r\nFlat F, 18/F, Southtex Building,<br/>\r\n51 Tsun Yip Street, Kwun Tong,<br/>\r\nKowloon, Hong Kong.</p>\r\n\r\n<p>Tel: (852) 2651 3030<br />\r\nFax: (852) 2651 3018<br/>\r\nMobile: (852) 9034 6588<br/>\r\nWebsite: www.quicklink-g.com<br/>\r\nE-mail: sammy.so@quicklink-g.com</p>\r\n', '<p><iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3023.7905570653197!2d-73.56744308515925!3d40.72262694490322!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c27de37d7753ed%3A0x25d8888e33b584bb!2s1936+Hempstead+Turnpike+%23237%2C+East+Meadow%2C+NY+11554%2C+USA!5e0!3m2!1sen!2sbd!4v1564210113632!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe></p>\r\n', 'hongkong-office', '', '', 1, 0, '2017-12-22 22:52:32', 1, '2019-07-05 11:00:11', '', '0000-00-00 00:00:00'),
(4, 'USA Office', 'USA', '<p>Dallas/Fort Worth and Houston 106<br />\r\nN. Denton Tap Rd Suite 210-147<br />\r\nCoppell, TX 75019<br />\r\n972-379-8317 TX<br />\r\nSales : contact@khsitsolutions.com</p>\r\n', '<p><iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3023.7905570653197!2d-73.56744308515925!3d40.72262694490322!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c27de37d7753ed%3A0x25d8888e33b584bb!2s1936+Hempstead+Turnpike+%23237%2C+East+Meadow%2C+NY+11554%2C+USA!5e0!3m2!1sen!2sbd!4v1564210113632!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe></p>\r\n', 'usa-office', '', '', 1, 0, '2017-12-22 22:52:58', 1, '2019-07-05 11:02:14', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `country_code` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `country_code`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '93', 0),
(2, 'Albania', 'AL', 'ALB', '0', 0),
(3, 'Algeria', 'DZ', 'DZA', '0', 0),
(4, 'American Samoa', 'AS', 'ASM', '0', 0),
(5, 'Andorra', 'AD', 'AND', '0', 0),
(6, 'Angola', 'AO', 'AGO', '0', 0),
(7, 'Anguilla', 'AI', 'AIA', '0', 0),
(8, 'Antarctica', 'AQ', 'ATA', '0', 0),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '0', 0),
(10, 'Argentina', 'AR', 'ARG', '0', 0),
(11, 'Armenia', 'AM', 'ARM', '0', 0),
(12, 'Aruba', 'AW', 'ABW', '0', 0),
(13, 'Australia', 'AU', 'AUS', '0', 0),
(14, 'Austria', 'AT', 'AUT', '0', 0),
(15, 'Azerbaijan', 'AZ', 'AZE', '0', 0),
(16, 'Bahamas', 'BS', 'BHS', '0', 0),
(17, 'Bahrain', 'BH', 'BHR', '0', 0),
(18, 'Bangladesh', 'BD', 'BGD', '88', 1),
(19, 'Barbados', 'BB', 'BRB', '0', 0),
(20, 'Belarus', 'BY', 'BLR', '0', 0),
(21, 'Belgium', 'BE', 'BEL', '0', 0),
(22, 'Belize', 'BZ', 'BLZ', '0', 0),
(23, 'Benin', 'BJ', 'BEN', '0', 0),
(24, 'Bermuda', 'BM', 'BMU', '0', 0),
(25, 'Bhutan', 'BT', 'BTN', '0', 0),
(26, 'Bolivia', 'BO', 'BOL', '0', 0),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '0', 0),
(28, 'Botswana', 'BW', 'BWA', '0', 0),
(29, 'Bouvet Island', 'BV', 'BVT', '0', 0),
(30, 'Brazil', 'BR', 'BRA', '0', 0),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '0', 0),
(32, 'Brunei Darussalam', 'BN', 'BRN', '0', 0),
(33, 'Bulgaria', 'BG', 'BGR', '0', 0),
(34, 'Burkina Faso', 'BF', 'BFA', '0', 0),
(35, 'Burundi', 'BI', 'BDI', '0', 0),
(36, 'Cambodia', 'KH', 'KHM', '0', 0),
(37, 'Cameroon', 'CM', 'CMR', '0', 0),
(38, 'Canada', 'CA', 'CAN', '0', 0),
(39, 'Cape Verde', 'CV', 'CPV', '0', 0),
(40, 'Cayman Islands', 'KY', 'CYM', '0', 0),
(41, 'Central African Republic', 'CF', 'CAF', '0', 0),
(42, 'Chad', 'TD', 'TCD', '0', 0),
(43, 'Chile', 'CL', 'CHL', '0', 0),
(44, 'China', 'CN', 'CHN', '0', 0),
(45, 'Christmas Island', 'CX', 'CXR', '0', 0),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '0', 0),
(47, 'Colombia', 'CO', 'COL', '0', 0),
(48, 'Comoros', 'KM', 'COM', '0', 0),
(49, 'Congo', 'CG', 'COG', '0', 0),
(50, 'Cook Islands', 'CK', 'COK', '0', 0),
(51, 'Costa Rica', 'CR', 'CRI', '0', 0),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', '0', 0),
(53, 'Croatia', 'HR', 'HRV', '0', 0),
(54, 'Cuba', 'CU', 'CUB', '0', 0),
(55, 'Cyprus', 'CY', 'CYP', '0', 0),
(56, 'Czech Republic', 'CZ', 'CZE', '0', 0),
(57, 'Denmark', 'DK', 'DNK', '0', 0),
(58, 'Djibouti', 'DJ', 'DJI', '0', 0),
(59, 'Dominica', 'DM', 'DMA', '0', 0),
(60, 'Dominican Republic', 'DO', 'DOM', '0', 0),
(61, 'East Timor', 'TL', 'TLS', '0', 0),
(62, 'Ecuador', 'EC', 'ECU', '0', 0),
(63, 'Egypt', 'EG', 'EGY', '0', 0),
(64, 'El Salvador', 'SV', 'SLV', '0', 0),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '0', 0),
(66, 'Eritrea', 'ER', 'ERI', '0', 0),
(67, 'Estonia', 'EE', 'EST', '0', 0),
(68, 'Ethiopia', 'ET', 'ETH', '0', 0),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '0', 0),
(70, 'Faroe Islands', 'FO', 'FRO', '0', 0),
(71, 'Fiji', 'FJ', 'FJI', '0', 0),
(72, 'Finland', 'FI', 'FIN', '0', 0),
(74, 'France, Metropolitan', 'FR', 'FRA', '1', 0),
(75, 'French Guiana', 'GF', 'GUF', '0', 0),
(76, 'French Polynesia', 'PF', 'PYF', '0', 0),
(77, 'French Southern Territories', 'TF', 'ATF', '0', 0),
(78, 'Gabon', 'GA', 'GAB', '0', 0),
(79, 'Gambia', 'GM', 'GMB', '0', 0),
(80, 'Georgia', 'GE', 'GEO', '0', 0),
(81, 'Germany', 'DE', 'DEU', '1', 0),
(82, 'Ghana', 'GH', 'GHA', '0', 0),
(83, 'Gibraltar', 'GI', 'GIB', '0', 0),
(84, 'Greece', 'GR', 'GRC', '0', 0),
(85, 'Greenland', 'GL', 'GRL', '0', 0),
(86, 'Grenada', 'GD', 'GRD', '0', 0),
(87, 'Guadeloupe', 'GP', 'GLP', '0', 0),
(88, 'Guam', 'GU', 'GUM', '0', 0),
(89, 'Guatemala', 'GT', 'GTM', '0', 0),
(90, 'Guinea', 'GN', 'GIN', '0', 0),
(91, 'Guinea-Bissau', 'GW', 'GNB', '0', 0),
(92, 'Guyana', 'GY', 'GUY', '0', 0),
(93, 'Haiti', 'HT', 'HTI', '0', 0),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '0', 0),
(95, 'Honduras', 'HN', 'HND', '0', 0),
(96, 'Hong Kong', 'HK', 'HKG', '0', 0),
(97, 'Hungary', 'HU', 'HUN', '0', 0),
(98, 'Iceland', 'IS', 'ISL', '0', 0),
(99, 'India', 'IN', 'IND', '91', 1),
(100, 'Indonesia', 'ID', 'IDN', '0', 0),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '98', 1),
(102, 'Iraq', 'IQ', 'IRQ', '0', 0),
(103, 'Ireland', 'IE', 'IRL', '0', 0),
(104, 'Israel', 'IL', 'ISR', '0', 0),
(105, 'Italy', 'IT', 'ITA', '0', 0),
(106, 'Jamaica', 'JM', 'JAM', '0', 0),
(107, 'Japan', 'JP', 'JPN', '0', 0),
(108, 'Jordan', 'JO', 'JOR', '0', 0),
(109, 'Kazakhstan', 'KZ', 'KAZ', '0', 0),
(110, 'Kenya', 'KE', 'KEN', '0', 0),
(111, 'Kiribati', 'KI', 'KIR', '0', 0),
(112, 'North Korea', 'KP', 'PRK', '0', 0),
(113, 'Korea, Republic of', 'KR', 'KOR', '0', 0),
(114, 'Kuwait', 'KW', 'KWT', '0', 0),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '0', 0),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '0', 0),
(117, 'Latvia', 'LV', 'LVA', '0', 0),
(118, 'Lebanon', 'LB', 'LBN', '0', 0),
(119, 'Lesotho', 'LS', 'LSO', '0', 0),
(120, 'Liberia', 'LR', 'LBR', '0', 0),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '0', 0),
(122, 'Liechtenstein', 'LI', 'LIE', '0', 0),
(123, 'Lithuania', 'LT', 'LTU', '0', 0),
(124, 'Luxembourg', 'LU', 'LUX', '0', 0),
(125, 'Macau', 'MO', 'MAC', '0', 0),
(126, 'FYROM', 'MK', 'MKD', '0', 0),
(127, 'Madagascar', 'MG', 'MDG', '0', 0),
(128, 'Malawi', 'MW', 'MWI', '0', 0),
(129, 'Malaysia', 'MY', 'MYS', '0', 0),
(130, 'Maldives', 'MV', 'MDV', '0', 0),
(131, 'Mali', 'ML', 'MLI', '0', 0),
(132, 'Malta', 'MT', 'MLT', '0', 0),
(133, 'Marshall Islands', 'MH', 'MHL', '0', 0),
(134, 'Martinique', 'MQ', 'MTQ', '0', 0),
(135, 'Mauritania', 'MR', 'MRT', '0', 0),
(136, 'Mauritius', 'MU', 'MUS', '0', 0),
(137, 'Mayotte', 'YT', 'MYT', '0', 0),
(138, 'Mexico', 'MX', 'MEX', '0', 0),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '0', 0),
(140, 'Moldova, Republic of', 'MD', 'MDA', '0', 0),
(141, 'Monaco', 'MC', 'MCO', '0', 0),
(142, 'Mongolia', 'MN', 'MNG', '0', 0),
(143, 'Montserrat', 'MS', 'MSR', '0', 0),
(144, 'Morocco', 'MA', 'MAR', '0', 0),
(145, 'Mozambique', 'MZ', 'MOZ', '0', 0),
(146, 'Myanmar', 'MM', 'MMR', '0', 0),
(147, 'Namibia', 'NA', 'NAM', '0', 0),
(148, 'Nauru', 'NR', 'NRU', '0', 0),
(149, 'Nepal', 'NP', 'NPL', '0', 0),
(150, 'Netherlands', 'NL', 'NLD', '0', 0),
(151, 'Netherlands Antilles', 'AN', 'ANT', '0', 0),
(152, 'New Caledonia', 'NC', 'NCL', '0', 0),
(153, 'New Zealand', 'NZ', 'NZL', '0', 0),
(154, 'Nicaragua', 'NI', 'NIC', '0', 0),
(155, 'Niger', 'NE', 'NER', '0', 0),
(156, 'Nigeria', 'NG', 'NGA', '0', 0),
(157, 'Niue', 'NU', 'NIU', '0', 0),
(158, 'Norfolk Island', 'NF', 'NFK', '0', 0),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '0', 0),
(160, 'Norway', 'NO', 'NOR', '0', 0),
(161, 'Oman', 'OM', 'OMN', '0', 0),
(162, 'Pakistan', 'PK', 'PAK', '92', 1),
(163, 'Palau', 'PW', 'PLW', '0', 0),
(164, 'Panama', 'PA', 'PAN', '0', 0),
(165, 'Papua New Guinea', 'PG', 'PNG', '0', 0),
(166, 'Paraguay', 'PY', 'PRY', '0', 0),
(167, 'Peru', 'PE', 'PER', '0', 0),
(168, 'Philippines', 'PH', 'PHL', '0', 0),
(169, 'Pitcairn', 'PN', 'PCN', '0', 0),
(170, 'Poland', 'PL', 'POL', '0', 0),
(171, 'Portugal', 'PT', 'PRT', '0', 0),
(172, 'Puerto Rico', 'PR', 'PRI', '0', 0),
(173, 'Qatar', 'QA', 'QAT', '0', 0),
(174, 'Reunion', 'RE', 'REU', '0', 0),
(175, 'Romania', 'RO', 'ROM', '0', 0),
(176, 'Russian Federation', 'RU', 'RUS', '0', 0),
(177, 'Rwanda', 'RW', 'RWA', '0', 0),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '0', 0),
(179, 'Saint Lucia', 'LC', 'LCA', '0', 0),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '0', 0),
(181, 'Samoa', 'WS', 'WSM', '0', 0),
(182, 'San Marino', 'SM', 'SMR', '0', 0),
(183, 'Sao Tome and Principe', 'ST', 'STP', '0', 0),
(184, 'Saudi Arabia', 'SA', 'SAU', '0', 0),
(185, 'Senegal', 'SN', 'SEN', '0', 0),
(186, 'Seychelles', 'SC', 'SYC', '0', 0),
(187, 'Sierra Leone', 'SL', 'SLE', '0', 0),
(188, 'Singapore', 'SG', 'SGP', '0', 0),
(189, 'Slovak Republic', 'SK', 'SVK', '0', 0),
(190, 'Slovenia', 'SI', 'SVN', '0', 0),
(191, 'Solomon Islands', 'SB', 'SLB', '0', 0),
(192, 'Somalia', 'SO', 'SOM', '0', 0),
(193, 'South Africa', 'ZA', 'ZAF', '0', 0),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '0', 0),
(195, 'Spain', 'ES', 'ESP', '0', 0),
(196, 'Sri Lanka', 'LK', 'LKA', '0', 0),
(197, 'St. Helena', 'SH', 'SHN', '0', 0),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '0', 0),
(199, 'Sudan', 'SD', 'SDN', '0', 0),
(200, 'Suriname', 'SR', 'SUR', '0', 0),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '0', 0),
(202, 'Swaziland', 'SZ', 'SWZ', '0', 0),
(203, 'Sweden', 'SE', 'SWE', '1', 0),
(204, 'Switzerland', 'CH', 'CHE', '0', 0),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '0', 0),
(206, 'Taiwan', 'TW', 'TWN', '0', 0),
(207, 'Tajikistan', 'TJ', 'TJK', '0', 0),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '0', 0),
(209, 'Thailand', 'TH', 'THA', '0', 0),
(210, 'Togo', 'TG', 'TGO', '0', 0),
(211, 'Tokelau', 'TK', 'TKL', '0', 0),
(212, 'Tonga', 'TO', 'TON', '0', 0),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '0', 0),
(214, 'Tunisia', 'TN', 'TUN', '0', 0),
(215, 'Turkey', 'TR', 'TUR', '0', 0),
(216, 'Turkmenistan', 'TM', 'TKM', '0', 0),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '0', 0),
(218, 'Tuvalu', 'TV', 'TUV', '0', 0),
(219, 'Uganda', 'UG', 'UGA', '0', 0),
(220, 'Ukraine', 'UA', 'UKR', '0', 0),
(221, 'United Arab Emirates', 'AE', 'ARE', '0', 0),
(222, 'United Kingdom', 'GB', 'GBR', '1', 0),
(223, 'United States', 'US', 'USA', '0', 0),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '0', 0),
(225, 'Uruguay', 'UY', 'URY', '0', 0),
(226, 'Uzbekistan', 'UZ', 'UZB', '0', 0),
(227, 'Vanuatu', 'VU', 'VUT', '0', 0),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '0', 0),
(229, 'Venezuela', 'VE', 'VEN', '0', 0),
(230, 'Viet Nam', 'VN', 'VNM', '0', 0),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '0', 0),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '0', 0),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '0', 0),
(234, 'Western Sahara', 'EH', 'ESH', '0', 0),
(235, 'Yemen', 'YE', 'YEM', '0', 0),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '0', 0),
(238, 'Zambia', 'ZM', 'ZMB', '0', 0),
(239, 'Zimbabwe', 'ZW', 'ZWE', '0', 0),
(242, 'Montenegro', 'ME', 'MNE', '0', 0),
(243, 'Serbia', 'RS', 'SRB', '0', 0),
(244, 'Aaland Islands', 'AX', 'ALA', '0', 0),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '0', 0),
(246, 'Curacao', 'CW', 'CUW', '0', 0),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '0', 0),
(248, 'South Sudan', 'SS', 'SSD', '0', 0),
(249, 'St. Barthelemy', 'BL', 'BLM', '0', 0),
(250, 'St. Martin (French part)', 'MF', 'MAF', '0', 0),
(251, 'Canary Islands', 'IC', 'ICA', '0', 0),
(252, 'Ascension Island (British)', 'AC', 'ASC', '0', 0),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '0', 0),
(254, 'Isle of Man', 'IM', 'IMN', '0', 0),
(255, 'Tristan da Cunha', 'TA', 'SHN', '0', 0),
(256, 'Guernsey', 'GG', 'GGY', '0', 0),
(257, 'Jersey', 'JE', 'JEY', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `albumId` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `gallery_type` varchar(255) NOT NULL,
  `video_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `albumId`, `image`, `status`, `gallery_type`, `video_url`) VALUES
(3, 5, '1512474597784.jpg', 1, '', ''),
(6, 5, '1512474658617.jpg', 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `link_lable` varchar(50) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `type` varchar(30) NOT NULL,
  `additional_id` varchar(200) NOT NULL,
  `position` varchar(20) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `update_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `new_tab` varchar(50) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `icon`, `link_lable`, `parent`, `type`, `additional_id`, `position`, `sort_order`, `status`, `update_by`, `update_time`, `new_tab`) VALUES
(1, 'About', '', '', NULL, 'page', '6', 'Top Menu', 1, 1, 1, '2019-08-23 13:43:14', 'No'),
(2, 'Client', '', '', NULL, 'custom', 'http://localhost:8080/nexcesexpress/client/', 'Top Menu', 2, 0, 1, '2019-09-04 10:15:16', 'No'),
(3, 'Services', '', '', NULL, 'custom', 'http://localhost:8080/nexcesexpress/our-services/', 'Top Menu', 3, 1, 1, '2019-09-04 10:15:37', 'No'),
(4, 'Our Team', '', '', NULL, 'custom', 'http://localhost:8080/nexcesexpress/ourTeam/', 'Top Menu', 4, 1, 1, '2019-09-04 10:15:44', 'No'),
(5, 'Shipping', '', '', NULL, 'custom', 'http://localhost:8080/nexcesexpress/shipping/', 'Top Menu', 5, 1, 1, '2019-09-04 10:15:50', 'No'),
(6, 'Contact', '', '', NULL, 'custom', '\r\nhttp://localhost/nexcesexpress/contactUs/', 'Top Menu', 6, 1, 1, '2019-09-04 10:15:54', 'No'),
(7, 'login', '', '', NULL, 'custom', 'http://localhost:8080/nexcesexpress/login', 'Top Menu', 7, 0, 1, '2019-09-04 10:16:00', 'No'),
(8, 'Home', '', '', NULL, 'custom', 'http://localhost:8080/nexcesexpress/', 'Footer Menu Left', 8, 1, 1, '2019-09-07 03:50:00', 'No'),
(9, 'About Us', '', '', NULL, 'page', '6', 'Footer Menu Left', 9, 1, 1, '2019-09-07 04:02:48', 'No'),
(10, 'Our Services', '', '', NULL, 'custom', 'http://localhost:8080/nexcesexpress/', 'Footer Menu Left', 10, 0, 1, '2019-09-07 04:26:18', 'No'),
(11, 'Shipping', '', '', NULL, 'custom', 'http://localhost:8080/nexcesexpress/', 'Footer Menu Left', 11, 0, 1, '2019-09-07 04:26:29', 'No'),
(12, 'Latest News', '', '', NULL, 'custom', 'http://localhost:8080/nexcesexpress/newsEvents', 'Footer Menu Left', 12, 1, 1, '2019-09-07 04:19:15', 'No'),
(13, 'Contact Us', '', '', NULL, 'custom', 'http://localhost:8080/nexcesexpress/site/contactUs/', 'Footer Menu Left', 13, 1, 1, '2019-09-07 04:14:53', 'No'),
(14, 'Online Booking', '', '', 5, 'custom', 'http://localhost:8080/nexcesexpress/shipping/onlineBooking', 'Top Menu', 14, 1, 1, '2019-09-19 08:57:48', 'No'),
(15, 'Bangladesh', '', '', 6, 'contact address', '1', 'Top Menu', 15, 1, 1, '2019-09-07 04:15:36', 'No'),
(16, 'China', '', '', 6, 'contact address', '2', 'Top Menu', 16, 1, 1, '2019-09-07 04:15:51', 'No'),
(17, 'Hongkong', '', '', 6, 'contact address', '3', 'Top Menu', 17, 1, 1, '2019-09-07 04:16:11', 'No'),
(18, 'USA', '', '', 6, 'contact address', '4', 'Top Menu', 18, 1, 1, '2019-09-07 04:16:29', 'No'),
(19, 'Logistics Services', '', '', 3, 'our services', '1', 'Top Menu', 19, 1, 1, '2019-09-07 04:16:47', 'No'),
(20, 'Custom Clearance', '', '', 3, 'our services', '2', 'Top Menu', 20, 1, 1, '2019-09-07 04:17:03', 'No'),
(21, 'Air Freight', '', '', 3, 'our services', '3', 'Top Menu', 21, 1, 1, '2019-09-07 04:17:16', 'No'),
(22, 'Inland Freight', '', '', 3, 'our services', '4', 'Top Menu', 22, 1, 1, '2019-09-07 14:21:47', 'No'),
(23, 'Transportation', '', '', 3, 'our services', '5', 'Top Menu', 23, 1, 1, '2019-09-07 14:21:47', 'No'),
(24, 'Warehousing', '', '', 3, 'our services', '6', 'Top Menu', 24, 1, 1, '2019-09-07 14:21:47', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_category`
--

CREATE TABLE `newsletter_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsletter_category`
--

INSERT INTO `newsletter_category` (`id`, `name`) VALUES
(3, 'Director'),
(4, 'CEO'),
(6, 'Accounts Head'),
(7, 'Custom');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_client`
--

CREATE TABLE `newsletter_client` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsletter_client`
--

INSERT INTO `newsletter_client` (`id`, `category_id`, `name`, `email`) VALUES
(2, 3, 'Anup Roy', 'anup@coder71.com'),
(3, 4, 'Mehedi', 'anupist726@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `news_events`
--

CREATE TABLE `news_events` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `news_date` date NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `featured` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news_events`
--

INSERT INTO `news_events` (`id`, `name`, `slug`, `image`, `details`, `news_date`, `sort_order`, `status`, `featured`) VALUES
(1, 'Bhargavi Arrunategui', 'Bhargavi-Arrunategui', '1512471281797.jpg', '<p>I highly recommend the services of KHS to my colleagues who are interested in transitioning to an electronic medical records-based practice. After successfully making this transition, we are pleased with the time-efficiency and cost-savings that have resulted. Clinicians in our practice are able to spend more time with patients and less time on paperwork. Office personnel are pleased with the increased level of efficiency with regards to documentation, communication and workflow processes. I would not hesitate to consult with the knowledgeable and courteous staff at KHS for future projects and therefore wholeheartedly recommend them to my colleagues.</p>\r\n', '2019-06-22', 1, 1, 1),
(2, 'Abdullah', 'abdullah-nexcesexpress', '1561281685274.jpg', '<div class=\"col-md-12 margin-bott-30\">\r\n<p>KHS IT Solutions provides support across the entire IT spectrum, from optimizing resources to maintaining infrastructure to migrating and managing the public and private cloud.<br />\r\nFrom initial discussion, concept, design, architecture, implementation to ongoing support we provide support across the entire IT spectrum.<br />\r\nOptimizing resources to maintaining infrastructure to migrating and managing the public and private cloud, we are your single point of contact for all your technology needs.</p>\r\n</div>\r\n', '2019-09-17', 2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `our_team`
--

CREATE TABLE `our_team` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `cover_photo` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `sort_order` int(11) NOT NULL,
  `fb_url` varchar(255) NOT NULL,
  `twitter_url` varchar(255) NOT NULL,
  `linkedin_url` varchar(255) NOT NULL,
  `web_url` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `featured` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `our_team`
--

INSERT INTO `our_team` (`id`, `category_id`, `name`, `email`, `phone`, `slug`, `image`, `cover_photo`, `designation`, `details`, `sort_order`, `fb_url`, `twitter_url`, `linkedin_url`, `web_url`, `status`, `featured`) VALUES
(1, 1, 'AbdullAh Al-Ahad', 'ahad.beside@gmail.com', '+880 1911160045', 'abdullAh-al-ahad', '151246892536.jpg', '151246892536.jpg', 'Director', '<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Mr. Raisul has the privilege to work with different organizations in providing services related to direct tax, international tax, indirect tax, transfer pricing consultancy, secretarial, etc. Before joining HLC, Mr. Raisul worked as Deputy Manager of Taxation &amp; Legal Compliance Department of Howladar Yunus &amp; Co. (HYC), Chartered Accountants, a correspondent firm of Grant Thornton International Ltd. in Bangladesh. He completed articleship from HYC&nbsp; and has completed 1300 marks under ICAB&nbsp;&nbsp; His major working experience lies in computing corporate and individual (local and expatriates) tax, providing guidance on advance tax, withholding tax, deferred tax, providing corporate tax planning, consultancy on transfer pricing, services related to value added tax (VAT), secretarial, etc. He also attended training on Transfer Pricing conducted by GT India in Noida, India. Mr. Raisul also has experience in planning, supervising, co-coordinating, monitoring the auditing and accounting works and was involved in various business advisory services and management consultancy services on various corporate financial matters.</span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"color: #000000;\">As a specialist in direct tax, indirect tax and international tax, Mr. Raisul conducted several in-house and out-house training.</span></p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column \">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">Specialization&nbsp;</span></h4>\r\n\r\n<p><span style=\"font-size: 12pt; font-family: arial, helvetica, sans-serif; color: #000000;\">International Tax, Corporate and Individual Tax, Transfer Pricing, Payroll Tax, VAT, Setting up and Doing Business in Bangladesh, Foreign Investment, Company Matters, etc.</span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column\">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"color: #000000;\">Clients</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"color: #000000;\">Grameen Phone Ltd. (a subsidiary of Telenor, Norway).</span></li>\r\n	<li><span style=\"color: #000000;\">Robi Axiata Limited (a subsidiary of Axiata Group Berhad, Malaysia).</span></li>\r\n	<li><span style=\"color: #000000;\">Airtel Bangladesh Ltd. (a subsidiary of Bharti Airtel Ltd., India).</span></li>\r\n	<li><span style=\"color: #000000;\">edotco Bangladesh&nbsp;Co. Ltd. (a subsidiary of edotco Group).</span></li>\r\n	<li><span style=\"color: #000000;\">Augere Wireless Broadband Bangladesh Limited.</span></li>\r\n	<li><span style=\"color: #000000;\">Bangla Trac Communications Limited (IGW Operator).</span></li>\r\n	<li><span style=\"color: #000000;\">EC Securities Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">IDLC Securities Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">IDLC Investments Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Prime Bank Investments Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">IDLC Finance Limited.</span></li>\r\n	<li><span style=\"color: #000000;\">Gramtech Knit Dyeing Finishing &amp; Garments Industries Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Southend Sweater Company Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">4A Yarn Dyeing Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Brothers Fashion Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Fountain Garments Manfacturing Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Fountain Garments Washing Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Everway Yarn Dyeing Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Team Sourcing Co. Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">El Corte Ingles, S.A.</span></li>\r\n	<li><span style=\"color: #000000;\">Bangla Trac Limited (exclusive dealer of Caterpillar in Bangladesh).</span></li>\r\n	<li><span style=\"color: #000000;\">Acorn Infrastructure Services Ltd. (HFO operated power generation company).</span></li>\r\n	<li><span style=\"color: #000000;\">Bangla Trac Power Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Khoniz Bangladesh Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Bangla Trac Oil &amp; Gas Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">THANE Technology Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Bangla Kapita Limited.</span></li>\r\n	<li><span style=\"color: #000000;\">Btrac Engineering Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Sreepur Agro Industries Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Acorn Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Asian Gate Limited.</span></li>\r\n	<li><span style=\"color: #000000;\">Maxwell Stamp Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Accenture Communications Infrastructure Solutions Ltd.</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column \">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"color: #000000;\">Education</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"color: #000000;\">Bachelor of Business Studies (BBS) &amp; Bachelor of Laws (LLB)</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', 1, '#', '#', '#', '#', 1, 1),
(2, 1, 'Abdullah Al-Ahad(Sohel)', 'ahad.beside@gmail.com', '+880 1911160045', 'abdullAh-al-ahad', '1512469257144.jpg', '1512469257144.jpg', 'CEO', '<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 10pt; color: #000000; font-family: arial, helvetica, sans-serif;\">Mr Hadee is the founder and CEO of HLC. He qualified as a Chartered Accountant in 2007 and has 17 years&rsquo; work experience in reputed companies/firm. Before starting own practice, he was Partner-in-Charge of Taxation &amp; Legal Compliance Department of Howladar Yunus &amp; Co. (HYC), Chartered Accountants, from January 2011 to October 2016. HYC is a correspondent firm of Grant Thornton International Ltd. in Bangladesh. The assignments relating to income tax (corporate, personal and expatriates), international tax, tax planning, value added tax (VAT), transfer pricing, customs, setting up and doing business in Bangladesh, company secretarial affairs, regulatory advices (such as BSEC, Bangladesh Bank, BTRC, BIDA/BOI, RJSC, etc.), due diligence, compliance audit, etc., were carried out under the direct supervision of Mr Hadee. Before joining HYC, he worked in Pacific Bangladesh Telecom Limited (Citycell), Grameen Phone Limited (GP) and MFH Financial Services Limited for more than 10 years in the department of Finance, Taxation, Legal, Corporate and Secretarial Affairs. During his tenure with the companies, Mr. Hadee has gained wide range of experiences in corporate tax, VAT, customs, foreign investment, regulatory affairs, secretarial affairs and legal matters. He is also well versed in Auditing, Accounting and Reporting.</span></p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column \">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Specialization&nbsp;</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column \">\r\n<ul>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">International Tax</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Corporate and Individual Tax</span></li>\r\n	<li>VAT</li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Payroll Tax</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Customs</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Setting up and Doing Business in Bangladesh</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Foreign Investment</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Company Matters</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Legal Affairs and Agreement Vetting</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Regulatory Affairs (BSEC, BOI/BIDA, BB, BTRC, RJSC, etc.)</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Name of Major Clients handled&nbsp;</span></h4>\r\n\r\n<ul>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Grameen Phone Ltd. (a subsidiary of Telenor, Norway)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Robi Axiata Limited (a subsidiary of Axiata Group Berhad, Malaysia)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Airtel Bangladesh Ltd. (a subsidiary of Bharti Airtel Ltd., India)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">edotco Bangladesh&nbsp;Co. Ltd. (a subsidiary of edotco Group)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Augere Wireless Broadband Bangladesh Limited</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Mango Teleservices Limited (IIG Operator)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Bangla Trac Communications Limited (IGW Operator)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Beximco Communications Ltd</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Voith Hydro Pvt. Ltd., India (a subsidiary of Voith Hydro, Germany)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">1224 MW (Net) Coal fired power plant in Banshkhali, Chittagong (Project)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">GE India Business Services Pvt. Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Summit Bibiyana II Power Company Ltd. (Gas Fired Combined Cycle Power Plant)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Alstom Bangladesh Branch</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Penspen (UK)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Acorn Infrastructure Services Ltd. (HFO operated power generation company)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Maxwell Stamp Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Jones Lang LaSalle (JLL)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Accenture Communications Infrastructure Solutions Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Lead Soft Bangladesh Limited</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Bangladesh Hardland Ceramics Co. Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">LSV Frontier Markets Equity Fund (USA)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">East Coast Group (one of the fastest growing business conglomerates in Bangladesh)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">MJL Bangladesh Limited (formerly Mobil Jamuna Lubricants Limited)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Bangla Trac Limited (exclusive dealer of Caterpillar in Bangladesh)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Sonali Bank Limited (one of the state owned commercial banks)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Janata Bank Limited (one of the state owned commercial banks)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Citi Bank N.A., Bangladesh (a major International Bank)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Lowe Clothing International Limited (UK)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">IDLC Finance Limited</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Emerging Credit Rating Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">IDLC Investments Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Prime Bank Investments Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">IFIC Securities Ltd</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Southeast Bank Capital Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Management and Resources Development Initiative (MRDI)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Oxfam GB</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">National Bank of Pakistan Limited (Branch of NBP)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Woori Bank (Branch of Woori Bank, Korea)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">El Corte Ingles, S.A. Liaison Office Bangladesh</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Team Sourcing Co. Ltd. (a Joint Venture Buying House)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Gramtech Knit Dyeing Finishing &amp; Garments Industries Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Team Group (100% export oriented garments companies)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Crystal Industry Bangladesh Limited (a wholly owned subsidiary of Crystal Group, UK)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Building Technology Ideas Limited (one of the leading Real Estate company in Bangladesh)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Aker Solutions Singapore Pte, Ltd. (a Singaporean Company)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Nestle Bangladesh Limited (a wholly owned Subsidiary of Nestl&eacute; S.A.)</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column\">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">Training/Conferences</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column\">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Building transfer pricing practice in Bangladesh&rdquo; organized by GT India in Noida, India</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;2015 Asia Pacific tax conference and training&rdquo; organized by GTIL in Manila, Philippines</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;2015 Global tax and outsourcing conference and tax training&rdquo; organized by GTIL in Dublin, Ireland</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;2014 APAC tax conference, outsourcing meeting and tax training&rdquo; organized by GTIL in Sydney, Australia</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;2014 Global tax and outsourcing conference and tax training&rdquo; organized by GTIL in Frankfurt, Germany</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Successful Talent Selection&rdquo; organized by GTIL in Bangkok, Thailand</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Audit Procedures and Methodologies&rdquo; organized by GTIL in Kathmandu, Nepal</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;MGI Asia Area Conference&rdquo; organized by MGI in Kuala Lumpur, Malaysia</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Corporate Legal Counsel&rdquo; organized by Marcus Evans in Dubai, UAE</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Corporate Performance and Financial Analysis&rdquo; organized by Euromoney in Singapore</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Problem Solving and Decision Making&rdquo; held in Kathmandu, Nepal</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Managerial Effectiveness&rdquo; conducted by Maps n Grow Consulting Pvt Ltd., India</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Professional IT Training&rdquo; conducted by Informatics Bangladesh Ltd.</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Share Management&rdquo; conducted by ICSMB</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Tax and VAT Management&rdquo; conducted by ICSMB</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Corporate Management&rdquo; conducted by ICSMB</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Tax and VAT&rdquo; conducted by ICMAB</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Team Building&rdquo; conducted by Grameen Phone Ltd.</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column\">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Professional qualifications and memberships&nbsp;</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Fellow Member of the Institute of Chartered Accountants of Bangladesh (ICAB)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Member of the Institute of Internal Auditors, Bangladesh (IIAB)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">The Institute of Supply Management &ndash; Bangladesh</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Bachelor of Laws (LLB)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">VAT Consultant Licensed by NBR</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Dhaka Taxes Bar Association</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Taxation and Corporate Laws Committee of ICAB</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column \">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Education</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">Master of Commerce (Accounting).</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', 2, '#', '#', '#', '#', 1, 1),
(3, 4, 'Abdullah Al-Ahad', 'ahad.beside@gmail.com', '+880 1911160045', 'abdullAh-al-ahad', '1512469435477.jpg', '1512469435477.jpg', 'Senior Consultant', '<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Mr Baki is a business minded finance professional with wide breadth of experience in end-to-end financial management in SMEs, large proprietary and ASX listed companies. He is a fellow member of ICAB and CPA Australia. Mr Baki&rsquo;s core strength lies in his abilities to plan, manage and execute all aspects of finance function. These include management reporting on key business drivers, ensuring compliance, streamlining systems and procedures, implementing accounting systems, establishment of appropriate internal controls, identifying cost rationalization opportunities, stakeholder management and communications, budgeting and planning, cash flow and treasury management, building and coaching the finance team, financial proposals, business case development, commercial management, acting as the source of technical and insightful operational advice.</span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Over the last 15 years, he has played key roles in M&amp;A, integration and related change management activities several times (both from buy and sale side). Mr Baki is a professional services sector expert with a passion for ODA project management. He has an in-depth understanding of operational challenges and reporting requirements of major ODA clients like DFAT, World Bank, ADB, DFID, USAID, etc.</span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">&nbsp;</span></p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column \">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"color: #000000;\">Specialization&nbsp;</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column \">\r\n<ul>\r\n	<li><span style=\"color: #000000;\">Full function financial control and management- technical, commercial and operational.</span></li>\r\n	<li><span style=\"color: #000000;\">Financial analysis, budgeting and forecasting, introducing and monitoring KPIs, costing.</span></li>\r\n	<li><span style=\"color: #000000;\">Identifying key business drivers, cost rationalisation, margin and efficiency improvement.</span></li>\r\n	<li><span style=\"color: #000000;\">Foreign currency, local and international compliance risk management.</span></li>\r\n	<li><span style=\"color: #000000;\">Statutory reporting in multi-company and international group structures.</span></li>\r\n	<li><span style=\"color: #000000;\">Managing external audit and other compliance processes including IFRS, company tax, BAS, payroll tax, workers&rsquo; compensation, FBT, international tax and transfer pricing.</span></li>\r\n	<li><span style=\"color: #000000;\">Financial modelling, preparing business cases and proposals, business valuation for M&amp;A.</span></li>\r\n	<li><span style=\"color: #000000;\">Process mapping, improvement, simplification, accounting system implementation (ERPs), internal controls, setting up operations and processes for start-ups, shared services.</span></li>\r\n	<li><span style=\"color: #000000;\">Developing and managing finance team. Providing advice and training to non-accounting peers.</span></li>\r\n	<li><span style=\"color: #000000;\">Project management, monitoring and evaluation in complex international environment.</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column\">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Professional qualifications and memberships&nbsp;</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"color: #000000;\">Fellow Member of the Institute of Chartered Accountants of Bangladesh (ICAB).</span></li>\r\n	<li><span style=\"color: #000000;\">Member of the CPA Australia.</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column \">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Education</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"color: #000000;\">Master of Commerce (Accounting).</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Training/Conferences</span></h4>\r\n\r\n<p class=\"wpb_wrapper\" style=\"text-align: justify;\"><span style=\"color: #000000;\">Mr Baki has travelled extensively in Asia, Pacific and Middle Eastern region for setting up offices, implementing financial reporting system, introducing risk management and control framework, preparing manuals and guidelines, auditing accounts, forensic investigations, training and capacity building of locally engaged support staff. He is widely commended as natural team player and for his ability to overcome the cross cultural and communication barrier and for building rapport with local staff quickly.</span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', 3, '#', '#', '#', '#', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `title` varchar(300) NOT NULL,
  `description` longtext NOT NULL,
  `slug` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `meta_keyword` varchar(500) NOT NULL,
  `meta_description` varchar(500) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `entry_by` int(11) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `attachment_title` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `title`, `description`, `slug`, `image`, `meta_keyword`, `meta_description`, `status`, `entry_by`, `entry_date`, `update_by`, `update_time`, `attachment_title`, `attachment`) VALUES
(1, 'New York Office', '<p>NYC / Long Island / Tri State 1936<br />\r\nHempstead TPKE Suite 237<br />\r\nEast Meadow, NY 11554</p>\r\n\r\n<p>516-336-8142 NY<br />\r\nSales : contact@khsitsolutions.com</p>\r\n', 'new-york-office', '', '', '', 1, 0, '2017-12-23 10:52:32', 1, '2019-09-16 22:16:32', '', ''),
(2, 'Texas Office', '<p>Dallas/Fort Worth and Houston 106<br />\r\nN. Denton Tap Rd Suite 210-147<br />\r\nCoppell, TX 75019<br />\r\n972-379-8317 TX<br />\r\nSales : contact@khsitsolutions.com</p>\r\n', 'texas-office', '', '', '', 1, 0, '2017-12-23 10:52:58', 1, '2019-07-05 23:02:14', '', ''),
(3, 'New York Office Map', '<p><iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3023.7905570653197!2d-73.56744308515925!3d40.72262694490322!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c27de37d7753ed%3A0x25d8888e33b584bb!2s1936+Hempstead+Turnpike+%23237%2C+East+Meadow%2C+NY+11554%2C+USA!5e0!3m2!1sen!2sbd!4v1564210113632!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe></p>\r\n', 'new-york-office-map', '', '', '', 1, 0, '2017-12-23 10:54:24', 1, '2019-07-05 23:02:40', '', ''),
(4, 'Texas Office Map', '<p><iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3347.2540503651508!2d-96.99408368540145!3d32.97069858139296!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864c294c6009dea1%3A0x9cddde53a1abc25d!2s106+N+Denton+Tap+Rd+Suite+210-147%2C+Coppell%2C+TX+75019%2C+USA!5e0!3m2!1sen!2sbd!4v1564209981053!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe></p>\r\n', 'texas-office-map', '', '', '', 1, 0, '2017-12-23 10:54:53', 1, '2019-07-05 23:03:22', '', ''),
(5, 'Contact Us Phone', '<p>516-336-8142 NY<br />\r\n972-379-8317 TX</p>\r\n', 'contact-us-phone', '', '', '', 1, 0, '2017-12-23 04:55:18', 1, '2019-07-06 00:09:42', '', ''),
(6, 'About Us', '<p><a href=\"http://localhost:8080/nexcesexpress/mediaLibrary/Test/viber_image_2019_06_25_10_18_50.jpg\"><img alt=\"\" src=\"http://localhost:8080/nexcesexpress/mediaLibrary/Test/about_ur_group_history.jpg\" style=\"float: left; width: 777px; height: 449px;\" /></a></p>\r\n\r\n<h2>Our Company</h2>\r\n\r\n<p>Nexces Express provides solutions tailored to meet challenges you face and deliver, no matter where in the worldwide courier Business takes you. We are known to be the fastest courier providing door to door delivery across the globe. Nexces Express provides both importing and exporting packages to worldwide destinations. our network has its presence globally &amp; Operations in all the major destinations with delivery to most countries in 2-4 business days.</p>\r\n', 'about-us', '', 'About Us', 'About Us', 1, 0, '2017-11-28 00:02:27', 1, '2019-09-07 02:43:07', '', ''),
(8, '<span style=\"font-weight:normal!important;\">Welcome To</span> <b>Nexcess Express</b>.', '<p>Nexcess Express services has professional team in understanding Bangladesh export / Import regulations enables us offer comprehensive Customs Clearance service for shipments exporting / Importing to all countries and helps clients faster to clear all customs related matters, they know as well rules and regulation of local custom and accommodate customers accordingly. We provides solutions tailored to meet challenges you face and deliver, no matter where in the worldwide courier Business takes you. We are known to be the fastest courier providing door to door delivery across the globe. Nexcess Express provides both importing and exporting packages to worldwide destinations. our network has its presence globally &amp; Operations in all the major destinations with delivery to most countries in 2-4 business days.</p>\r\n', '%3Cspan-style%3D%22font-weight%3Anormal%21important%3B%22%3Ewelcome-to%3Cspan%3E-%3Cb%3Enexcess-express%3Cb%3E', '', 'Welcome To KHS IT Solutions', 'Welcome To KHS IT Solutions', 1, 0, '2017-11-25 05:05:48', 1, '2019-09-06 22:10:45', '', ''),
(9, 'Worldwide Courier Solution', '<p>We Provide International Express Courier services & Parcel Service.We also offer our shipments through DHL / FedEx / Aramex / UPS with a commitment of safe and on-time delivery.We offer best service with Low Rate.(বিশ্বের যে কোন দেশে আপনার মূল্যবান ডকুমেন্ট ও পার্সেল পাঠাতেআজই যোগাযোগ করুন। একমাত্র আমরাই DHL,FEDEX ,TNT ARAMEX এর মাধ্যমে কম খরচে ৩ থেকে ৪ দিনের মধ্যে ডকুমেন্ট ও পার্সেল বিশ্বের যে কোন স্থানে ১০০% নিশ্চয়তার সাথে পাঠিয়ে থাকি)<br/>\r\nPlease call our sales assistant:\r\n01955300065, 01620603086</p>\r\n', '', '', 'Worldwide Courier Solution', 'Worldwide Courier Solution', 1, 0, '2017-11-29 17:02:43', 1, '2017-12-09 02:03:20', 'Click download file', '1512893000533.pdf'),
(15, 'Contact Us Phone', '<p>+880 2-9820543<br />\r\n+880 191 399 7273<br />\r\n+880 191 792 3778</p>\r\n', '', '', '', '', 1, 0, '2017-12-23 04:55:18', 1, '2017-12-23 04:55:18', '', ''),
(16, 'Contact Us', '<ul>\r\n            <li> <i class=\"fa fa-map-marker\"></i>\r\n              <div class=\"continfo\">\r\n              	<span>\r\n                	House # 35(Fast Floor), <br/>Road # 17, Nikunja-2,<br/> Khilkhet, Dhaka-1229\r\n                </span>\r\n              \r\n                 </div>\r\n            </li>\r\n            <li> <i class=\"fa fa-phone\"></i>\r\n              <div class=\"continfo\"> <a href=\"tel:+88 01955300065\">+88 01955300065 </a></div>\r\n            </li>\r\n            <li> <i class=\"fa fa-envelope-o\"></i>\r\n              <div class=\"continfo\"> <a href=\"#\"> Sales 	: 	amrjtrade.mq@gmail.com </a> </div>\r\n            </li>\r\n            \r\n          </ul>', '', '', 'About Us', 'About Us', 1, 0, '2017-11-27 18:02:27', 1, '2017-11-27 08:51:49', '', ''),
(17, 'Contact Us', '\r\n<ul>\r\n            <li> <i class=\"fa fa-map-marker\"></i>\r\n              <div class=\"continfo\">\r\n              	<span>\r\n                	NYC / Long Island / Tri State 1936 Hempstead TPKE Suite 237 East Meadow, NY 11554\r\n                </span>\r\n              \r\n                 </div>\r\n            </li>\r\n            <li> <i class=\"fa fa-phone\"></i>\r\n              <div class=\"continfo\"><a href=\"tel:+1 516-336-8142\">516-336-8142 NY </a></div>\r\n            </li>\r\n            <li> <i class=\"fa fa-envelope-o\"></i>\r\n              <div class=\"continfo\"> <a href=\"#\"> Sales : contact@khsitsolutions.com</a> </div>\r\n            </li>\r\n            \r\n          </ul>\r\n', '', '', 'Contact Us', 'Contact Us', 1, 0, '2017-11-27 08:51:49', 0, '2017-11-27 08:51:49', '', ''),
(18, 'Products', '<p>KHS IT Solutions provides support across the entire IT spectrum, from optimizing resources to maintaining infrastructure to migrating and managing the public and private cloud. From initial discussion, concept, design, architecture, implementation to ongoing support we provide support across the entire IT spectrum. Optimizing resources to maintaining infrastructure to migrating and managing the public and private cloud, we are your single point of contact for all your technology needs.</p>\r\n', 'products', '', 'Products', 'Products', 1, 0, '2019-06-22 20:00:45', 1, '2019-06-22 16:00:45', '', ''),
(19, 'Testimonials', '<p>KHS IT Solutions provides support across the entire IT spectrum, from optimizing resources to maintaining infrastructure to migrating and managing the public and private cloud. From initial discussion, concept, design, architecture, implementation to ongoing support we provide support across the entire IT spectrum. Optimizing resources to maintaining infrastructure to migrating and managing the public and private cloud, we are your single point of contact for all your technology needs.</p>\r\n', 'testimonials', '', 'Testimonials', 'Testtmonials', 1, 0, '2019-06-22 20:04:08', 1, '2019-06-22 16:04:08', '', ''),
(20, 'Portfolio', '<p>KHS IT Solutions provides support across the entire IT spectrum, from optimizing resources to maintaining infrastructure to migrating and managing the public and private cloud. From initial discussion, concept, design, architecture, implementation to ongoing support we provide support across the entire IT spectrum. Optimizing resources to maintaining infrastructure to migrating and managing the public and private cloud, we are your single point of contact for all your technology needs.</p>\r\n', 'portfolio', '', 'Portfolio', 'Portfolio', 1, 0, '2019-06-22 20:05:33', 1, '2019-06-22 16:05:33', '', ''),
(21, 'Our Services', '<p>KHS IT Solutions provides support across the entire IT spectrum, from optimizing resources to maintaining infrastructure to migrating and managing the public and private cloud. From initial discussion, concept, design, architecture, implementation to ongoing support we provide support across the entire IT spectrum. Optimizing resources to maintaining infrastructure to migrating and managing the public and private cloud, we are your single point of contact for all your technology needs.</p>\r\n', 'our-services', '', 'Our Services', 'Our Servies', 1, 0, '2019-06-22 20:07:24', 1, '2019-06-22 16:07:24', '', ''),
(22, 'asa asas asas', '<p>asa</p>\r\n', 'asa-asas-asas', '', '', '', 1, 0, '2019-09-17 14:18:01', 1, '2019-09-16 22:18:29', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `partner`
--

CREATE TABLE `partner` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `link` varchar(300) NOT NULL,
  `image` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `featured` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partner`
--

INSERT INTO `partner` (`id`, `category_id`, `name`, `slug`, `link`, `image`, `details`, `sort_order`, `status`, `featured`) VALUES
(1, 1, 'A.R. APPARELS LTD', 'a-r-apparels-ltd-nexcessexpress', 'http://www.duvalny.com/', '156874976065.jpg', '<p>A.R. APPARELS LTD</p>\r\n', 1, 1, 1),
(2, 2, 'Dhaka Medical College', 'dhaka-medical-college-nexcessexpress', 'https://www.google.com/', '156775894956.jfif', '<p>Dhaka Medical College</p>\r\n', 2, 1, 1),
(3, 2, 'Group', 'group-nexcesexpress', 'https://www.google.com/', '1568749719850.jpg', '<p>Eon Group</p>\r\n', 3, 1, 1),
(11, 2, 'asasasa asasas asasasa', 'asasasa-asasas-asasasa', 'asas', '', '<p>asasasa asasas asasasa</p>\r\n', 4, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `partner_category`
--

CREATE TABLE `partner_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partner_category`
--

INSERT INTO `partner_category` (`id`, `name`, `slug`, `status`, `sort_order`) VALUES
(1, 'Airlines', 'airlines-nexcesexpress', 1, 1),
(2, 'Logistic', 'logistic-nexcesexpress', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE `portfolio` (
  `id` int(11) NOT NULL,
  `productCat_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `cover_photo` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `sort_order` int(11) NOT NULL,
  `fb_url` varchar(255) NOT NULL,
  `twitter_url` varchar(255) NOT NULL,
  `linkedin_url` varchar(255) NOT NULL,
  `web_url` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `featured` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`id`, `productCat_id`, `name`, `email`, `phone`, `slug`, `image`, `cover_photo`, `designation`, `details`, `sort_order`, `fb_url`, `twitter_url`, `linkedin_url`, `web_url`, `status`, `featured`) VALUES
(6, 1, 'Md. Raisul Islam LLB', 'raisul@hadeelutful.com', '+880 191 399 7273', 'abc', '1561182495323.jpg', '1512477355224.jpg', 'Director', '<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Mr. Raisul has the privilege to work with different organizations in providing services related to direct tax, international tax, indirect tax, transfer pricing consultancy, secretarial, etc. Before joining HLC, Mr. Raisul worked as Deputy Manager of Taxation &amp; Legal Compliance Department of Howladar Yunus &amp; Co. (HYC), Chartered Accountants, a correspondent firm of Grant Thornton International Ltd. in Bangladesh. He completed articleship from HYC&nbsp; and has completed 1300 marks under ICAB&nbsp;&nbsp; His major working experience lies in computing corporate and individual (local and expatriates) tax, providing guidance on advance tax, withholding tax, deferred tax, providing corporate tax planning, consultancy on transfer pricing, services related to value added tax (VAT), secretarial, etc. He also attended training on Transfer Pricing conducted by GT India in Noida, India. Mr. Raisul also has experience in planning, supervising, co-coordinating, monitoring the auditing and accounting works and was involved in various business advisory services and management consultancy services on various corporate financial matters.</span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"color: #000000;\">As a specialist in direct tax, indirect tax and international tax, Mr. Raisul conducted several in-house and out-house training.</span></p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column \">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">Specialization&nbsp;</span></h4>\r\n\r\n<p><span style=\"font-size: 12pt; font-family: arial, helvetica, sans-serif; color: #000000;\">International Tax, Corporate and Individual Tax, Transfer Pricing, Payroll Tax, VAT, Setting up and Doing Business in Bangladesh, Foreign Investment, Company Matters, etc.</span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column\">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"color: #000000;\">Clients</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"color: #000000;\">Grameen Phone Ltd. (a subsidiary of Telenor, Norway).</span></li>\r\n	<li><span style=\"color: #000000;\">Robi Axiata Limited (a subsidiary of Axiata Group Berhad, Malaysia).</span></li>\r\n	<li><span style=\"color: #000000;\">Airtel Bangladesh Ltd. (a subsidiary of Bharti Airtel Ltd., India).</span></li>\r\n	<li><span style=\"color: #000000;\">edotco Bangladesh&nbsp;Co. Ltd. (a subsidiary of edotco Group).</span></li>\r\n	<li><span style=\"color: #000000;\">Augere Wireless Broadband Bangladesh Limited.</span></li>\r\n	<li><span style=\"color: #000000;\">Bangla Trac Communications Limited (IGW Operator).</span></li>\r\n	<li><span style=\"color: #000000;\">EC Securities Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">IDLC Securities Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">IDLC Investments Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Prime Bank Investments Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">IDLC Finance Limited.</span></li>\r\n	<li><span style=\"color: #000000;\">Gramtech Knit Dyeing Finishing &amp; Garments Industries Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Southend Sweater Company Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">4A Yarn Dyeing Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Brothers Fashion Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Fountain Garments Manfacturing Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Fountain Garments Washing Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Everway Yarn Dyeing Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Team Sourcing Co. Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">El Corte Ingles, S.A.</span></li>\r\n	<li><span style=\"color: #000000;\">Bangla Trac Limited (exclusive dealer of Caterpillar in Bangladesh).</span></li>\r\n	<li><span style=\"color: #000000;\">Acorn Infrastructure Services Ltd. (HFO operated power generation company).</span></li>\r\n	<li><span style=\"color: #000000;\">Bangla Trac Power Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Khoniz Bangladesh Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Bangla Trac Oil &amp; Gas Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">THANE Technology Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Bangla Kapita Limited.</span></li>\r\n	<li><span style=\"color: #000000;\">Btrac Engineering Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Sreepur Agro Industries Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Acorn Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Asian Gate Limited.</span></li>\r\n	<li><span style=\"color: #000000;\">Maxwell Stamp Ltd.</span></li>\r\n	<li><span style=\"color: #000000;\">Accenture Communications Infrastructure Solutions Ltd.</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column \">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"color: #000000;\">Education</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"color: #000000;\">Bachelor of Business Studies (BBS) &amp; Bachelor of Laws (LLB)</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', 1, '#', '#', '#', 'http://www.duvalny.com/', 1, 1),
(7, 1, 'MBM Lutful Hadee FCA LLB', 'hadee@hadeelutful.com', '+88 01711500142', 'mbm-lutful-hadee-fca-llb', '1561182535259.jpg', '1561182535148.jpg', 'Director', '<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 10pt; color: #000000; font-family: arial, helvetica, sans-serif;\">Mr Hadee is the founder and CEO of HLC. He qualified as a Chartered Accountant in 2007 and has 17 years&rsquo; work experience in reputed companies/firm. Before starting own practice, he was Partner-in-Charge of Taxation &amp; Legal Compliance Department of Howladar Yunus &amp; Co. (HYC), Chartered Accountants, from January 2011 to October 2016. HYC is a correspondent firm of Grant Thornton International Ltd. in Bangladesh. The assignments relating to income tax (corporate, personal and expatriates), international tax, tax planning, value added tax (VAT), transfer pricing, customs, setting up and doing business in Bangladesh, company secretarial affairs, regulatory advices (such as BSEC, Bangladesh Bank, BTRC, BIDA/BOI, RJSC, etc.), due diligence, compliance audit, etc., were carried out under the direct supervision of Mr Hadee. Before joining HYC, he worked in Pacific Bangladesh Telecom Limited (Citycell), Grameen Phone Limited (GP) and MFH Financial Services Limited for more than 10 years in the department of Finance, Taxation, Legal, Corporate and Secretarial Affairs. During his tenure with the companies, Mr. Hadee has gained wide range of experiences in corporate tax, VAT, customs, foreign investment, regulatory affairs, secretarial affairs and legal matters. He is also well versed in Auditing, Accounting and Reporting.</span></p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column \">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Specialization&nbsp;</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column \">\r\n<ul>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">International Tax</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Corporate and Individual Tax</span></li>\r\n	<li>VAT</li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Payroll Tax</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Customs</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Setting up and Doing Business in Bangladesh</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Foreign Investment</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Company Matters</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Legal Affairs and Agreement Vetting</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Regulatory Affairs (BSEC, BOI/BIDA, BB, BTRC, RJSC, etc.)</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Name of Major Clients handled&nbsp;</span></h4>\r\n\r\n<ul>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Grameen Phone Ltd. (a subsidiary of Telenor, Norway)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Robi Axiata Limited (a subsidiary of Axiata Group Berhad, Malaysia)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Airtel Bangladesh Ltd. (a subsidiary of Bharti Airtel Ltd., India)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">edotco Bangladesh&nbsp;Co. Ltd. (a subsidiary of edotco Group)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Augere Wireless Broadband Bangladesh Limited</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Mango Teleservices Limited (IIG Operator)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Bangla Trac Communications Limited (IGW Operator)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Beximco Communications Ltd</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Voith Hydro Pvt. Ltd., India (a subsidiary of Voith Hydro, Germany)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">1224 MW (Net) Coal fired power plant in Banshkhali, Chittagong (Project)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">GE India Business Services Pvt. Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Summit Bibiyana II Power Company Ltd. (Gas Fired Combined Cycle Power Plant)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Alstom Bangladesh Branch</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Penspen (UK)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Acorn Infrastructure Services Ltd. (HFO operated power generation company)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Maxwell Stamp Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Jones Lang LaSalle (JLL)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Accenture Communications Infrastructure Solutions Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Lead Soft Bangladesh Limited</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Bangladesh Hardland Ceramics Co. Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">LSV Frontier Markets Equity Fund (USA)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">East Coast Group (one of the fastest growing business conglomerates in Bangladesh)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">MJL Bangladesh Limited (formerly Mobil Jamuna Lubricants Limited)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Bangla Trac Limited (exclusive dealer of Caterpillar in Bangladesh)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Sonali Bank Limited (one of the state owned commercial banks)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Janata Bank Limited (one of the state owned commercial banks)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Citi Bank N.A., Bangladesh (a major International Bank)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Lowe Clothing International Limited (UK)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">IDLC Finance Limited</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Emerging Credit Rating Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">IDLC Investments Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Prime Bank Investments Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">IFIC Securities Ltd</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Southeast Bank Capital Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Management and Resources Development Initiative (MRDI)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Oxfam GB</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">National Bank of Pakistan Limited (Branch of NBP)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Woori Bank (Branch of Woori Bank, Korea)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">El Corte Ingles, S.A. Liaison Office Bangladesh</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Team Sourcing Co. Ltd. (a Joint Venture Buying House)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Gramtech Knit Dyeing Finishing &amp; Garments Industries Ltd.</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Team Group (100% export oriented garments companies)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Crystal Industry Bangladesh Limited (a wholly owned subsidiary of Crystal Group, UK)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Building Technology Ideas Limited (one of the leading Real Estate company in Bangladesh)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Aker Solutions Singapore Pte, Ltd. (a Singaporean Company)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Nestle Bangladesh Limited (a wholly owned Subsidiary of Nestl&eacute; S.A.)</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column\">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">Training/Conferences</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column\">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Building transfer pricing practice in Bangladesh&rdquo; organized by GT India in Noida, India</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;2015 Asia Pacific tax conference and training&rdquo; organized by GTIL in Manila, Philippines</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;2015 Global tax and outsourcing conference and tax training&rdquo; organized by GTIL in Dublin, Ireland</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;2014 APAC tax conference, outsourcing meeting and tax training&rdquo; organized by GTIL in Sydney, Australia</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;2014 Global tax and outsourcing conference and tax training&rdquo; organized by GTIL in Frankfurt, Germany</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Successful Talent Selection&rdquo; organized by GTIL in Bangkok, Thailand</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Audit Procedures and Methodologies&rdquo; organized by GTIL in Kathmandu, Nepal</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;MGI Asia Area Conference&rdquo; organized by MGI in Kuala Lumpur, Malaysia</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Corporate Legal Counsel&rdquo; organized by Marcus Evans in Dubai, UAE</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Corporate Performance and Financial Analysis&rdquo; organized by Euromoney in Singapore</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Problem Solving and Decision Making&rdquo; held in Kathmandu, Nepal</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Managerial Effectiveness&rdquo; conducted by Maps n Grow Consulting Pvt Ltd., India</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Professional IT Training&rdquo; conducted by Informatics Bangladesh Ltd.</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Share Management&rdquo; conducted by ICSMB</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Tax and VAT Management&rdquo; conducted by ICSMB</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Corporate Management&rdquo; conducted by ICSMB</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Tax and VAT&rdquo; conducted by ICMAB</span></li>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">&ldquo;Team Building&rdquo; conducted by Grameen Phone Ltd.</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column\">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Professional qualifications and memberships&nbsp;</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Fellow Member of the Institute of Chartered Accountants of Bangladesh (ICAB)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Member of the Institute of Internal Auditors, Bangladesh (IIAB)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">The Institute of Supply Management &ndash; Bangladesh</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Bachelor of Laws (LLB)</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">VAT Consultant Licensed by NBR</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Dhaka Taxes Bar Association</span></li>\r\n	<li><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Taxation and Corporate Laws Committee of ICAB</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column \">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Education</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"color: #000000; font-family: arial, helvetica, sans-serif;\">Master of Commerce (Accounting).</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', 2, '#', '#', '#', 'http://www.pkdisease.com', 1, 1),
(8, 2, 'MQM Lutful Baki FCA CPA', 'baki@hadeelutful.com', '+61422 121 439', 'mqm-lutful-baki-fca-cpa', '1561182653343.jpg', '', 'Director', '<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Mr Baki is a business minded finance professional with wide breadth of experience in end-to-end financial management in SMEs, large proprietary and ASX listed companies. He is a fellow member of ICAB and CPA Australia. Mr Baki&rsquo;s core strength lies in his abilities to plan, manage and execute all aspects of finance function. These include management reporting on key business drivers, ensuring compliance, streamlining systems and procedures, implementing accounting systems, establishment of appropriate internal controls, identifying cost rationalization opportunities, stakeholder management and communications, budgeting and planning, cash flow and treasury management, building and coaching the finance team, financial proposals, business case development, commercial management, acting as the source of technical and insightful operational advice.</span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Over the last 15 years, he has played key roles in M&amp;A, integration and related change management activities several times (both from buy and sale side). Mr Baki is a professional services sector expert with a passion for ODA project management. He has an in-depth understanding of operational challenges and reporting requirements of major ODA clients like DFAT, World Bank, ADB, DFID, USAID, etc.</span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">&nbsp;</span></p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column \">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"color: #000000;\">Specialization&nbsp;</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column \">\r\n<ul>\r\n	<li><span style=\"color: #000000;\">Full function financial control and management- technical, commercial and operational.</span></li>\r\n	<li><span style=\"color: #000000;\">Financial analysis, budgeting and forecasting, introducing and monitoring KPIs, costing.</span></li>\r\n	<li><span style=\"color: #000000;\">Identifying key business drivers, cost rationalisation, margin and efficiency improvement.</span></li>\r\n	<li><span style=\"color: #000000;\">Foreign currency, local and international compliance risk management.</span></li>\r\n	<li><span style=\"color: #000000;\">Statutory reporting in multi-company and international group structures.</span></li>\r\n	<li><span style=\"color: #000000;\">Managing external audit and other compliance processes including IFRS, company tax, BAS, payroll tax, workers&rsquo; compensation, FBT, international tax and transfer pricing.</span></li>\r\n	<li><span style=\"color: #000000;\">Financial modelling, preparing business cases and proposals, business valuation for M&amp;A.</span></li>\r\n	<li><span style=\"color: #000000;\">Process mapping, improvement, simplification, accounting system implementation (ERPs), internal controls, setting up operations and processes for start-ups, shared services.</span></li>\r\n	<li><span style=\"color: #000000;\">Developing and managing finance team. Providing advice and training to non-accounting peers.</span></li>\r\n	<li><span style=\"color: #000000;\">Project management, monitoring and evaluation in complex international environment.</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column\">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Professional qualifications and memberships&nbsp;</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"color: #000000;\">Fellow Member of the Institute of Chartered Accountants of Bangladesh (ICAB).</span></li>\r\n	<li><span style=\"color: #000000;\">Member of the CPA Australia.</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column \">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Education</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"color: #000000;\">Master of Commerce (Accounting).</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Training/Conferences</span></h4>\r\n\r\n<p class=\"wpb_wrapper\" style=\"text-align: justify;\"><span style=\"color: #000000;\">Mr Baki has travelled extensively in Asia, Pacific and Middle Eastern region for setting up offices, implementing financial reporting system, introducing risk management and control framework, preparing manuals and guidelines, auditing accounts, forensic investigations, training and capacity building of locally engaged support staff. He is widely commended as natural team player and for his ability to overcome the cross cultural and communication barrier and for building rapport with local staff quickly.</span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', 3, '#', '#', '#', 'http://www.cinderellabridez.com', 1, 1),
(9, 1, 'MQM Lutful Baki FCA CPA', 'baki@hadeelutful.com', '+61422 121 439', 'MQM-Lutful-Baki-FCA-CPA-2', '1561182832511.jpg', '1561182832588.jpg', 'Director', '<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Mr Baki is a business minded finance professional with wide breadth of experience in end-to-end financial management in SMEs, large proprietary and ASX listed companies. He is a fellow member of ICAB and CPA Australia. Mr Baki&rsquo;s core strength lies in his abilities to plan, manage and execute all aspects of finance function. These include management reporting on key business drivers, ensuring compliance, streamlining systems and procedures, implementing accounting systems, establishment of appropriate internal controls, identifying cost rationalization opportunities, stakeholder management and communications, budgeting and planning, cash flow and treasury management, building and coaching the finance team, financial proposals, business case development, commercial management, acting as the source of technical and insightful operational advice.</span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Over the last 15 years, he has played key roles in M&amp;A, integration and related change management activities several times (both from buy and sale side). Mr Baki is a professional services sector expert with a passion for ODA project management. He has an in-depth understanding of operational challenges and reporting requirements of major ODA clients like DFAT, World Bank, ADB, DFID, USAID, etc.</span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">&nbsp;</span></p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column \">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"color: #000000;\">Specialization&nbsp;</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column \">\r\n<ul>\r\n	<li><span style=\"color: #000000;\">Full function financial control and management- technical, commercial and operational.</span></li>\r\n	<li><span style=\"color: #000000;\">Financial analysis, budgeting and forecasting, introducing and monitoring KPIs, costing.</span></li>\r\n	<li><span style=\"color: #000000;\">Identifying key business drivers, cost rationalisation, margin and efficiency improvement.</span></li>\r\n	<li><span style=\"color: #000000;\">Foreign currency, local and international compliance risk management.</span></li>\r\n	<li><span style=\"color: #000000;\">Statutory reporting in multi-company and international group structures.</span></li>\r\n	<li><span style=\"color: #000000;\">Managing external audit and other compliance processes including IFRS, company tax, BAS, payroll tax, workers&rsquo; compensation, FBT, international tax and transfer pricing.</span></li>\r\n	<li><span style=\"color: #000000;\">Financial modelling, preparing business cases and proposals, business valuation for M&amp;A.</span></li>\r\n	<li><span style=\"color: #000000;\">Process mapping, improvement, simplification, accounting system implementation (ERPs), internal controls, setting up operations and processes for start-ups, shared services.</span></li>\r\n	<li><span style=\"color: #000000;\">Developing and managing finance team. Providing advice and training to non-accounting peers.</span></li>\r\n	<li><span style=\"color: #000000;\">Project management, monitoring and evaluation in complex international environment.</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column\">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Professional qualifications and memberships&nbsp;</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"color: #000000;\">Fellow Member of the Institute of Chartered Accountants of Bangladesh (ICAB).</span></li>\r\n	<li><span style=\"color: #000000;\">Member of the CPA Australia.</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column \">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Education</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"color: #000000;\">Master of Commerce (Accounting).</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Training/Conferences</span></h4>\r\n\r\n<p class=\"wpb_wrapper\" style=\"text-align: justify;\"><span style=\"color: #000000;\">Mr Baki has travelled extensively in Asia, Pacific and Middle Eastern region for setting up offices, implementing financial reporting system, introducing risk management and control framework, preparing manuals and guidelines, auditing accounts, forensic investigations, training and capacity building of locally engaged support staff. He is widely commended as natural team player and for his ability to overcome the cross cultural and communication barrier and for building rapport with local staff quickly.</span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', 4, '#', '#', '#', 'http://www.bollyarts.com', 1, 1),
(10, 4, 'MQM Lutful Baki FCA CPA', 'baki@hadeelutful.com', '+61422 121 439', 'MQM-Lutful-Baki-FCA-CPA-3', '1561182887979.jpg', '1561182887166.jpg', 'Director', '<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Mr Baki is a business minded finance professional with wide breadth of experience in end-to-end financial management in SMEs, large proprietary and ASX listed companies. He is a fellow member of ICAB and CPA Australia. Mr Baki&rsquo;s core strength lies in his abilities to plan, manage and execute all aspects of finance function. These include management reporting on key business drivers, ensuring compliance, streamlining systems and procedures, implementing accounting systems, establishment of appropriate internal controls, identifying cost rationalization opportunities, stakeholder management and communications, budgeting and planning, cash flow and treasury management, building and coaching the finance team, financial proposals, business case development, commercial management, acting as the source of technical and insightful operational advice.</span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Over the last 15 years, he has played key roles in M&amp;A, integration and related change management activities several times (both from buy and sale side). Mr Baki is a professional services sector expert with a passion for ODA project management. He has an in-depth understanding of operational challenges and reporting requirements of major ODA clients like DFAT, World Bank, ADB, DFID, USAID, etc.</span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">&nbsp;</span></p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column \">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"color: #000000;\">Specialization&nbsp;</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column \">\r\n<ul>\r\n	<li><span style=\"color: #000000;\">Full function financial control and management- technical, commercial and operational.</span></li>\r\n	<li><span style=\"color: #000000;\">Financial analysis, budgeting and forecasting, introducing and monitoring KPIs, costing.</span></li>\r\n	<li><span style=\"color: #000000;\">Identifying key business drivers, cost rationalisation, margin and efficiency improvement.</span></li>\r\n	<li><span style=\"color: #000000;\">Foreign currency, local and international compliance risk management.</span></li>\r\n	<li><span style=\"color: #000000;\">Statutory reporting in multi-company and international group structures.</span></li>\r\n	<li><span style=\"color: #000000;\">Managing external audit and other compliance processes including IFRS, company tax, BAS, payroll tax, workers&rsquo; compensation, FBT, international tax and transfer pricing.</span></li>\r\n	<li><span style=\"color: #000000;\">Financial modelling, preparing business cases and proposals, business valuation for M&amp;A.</span></li>\r\n	<li><span style=\"color: #000000;\">Process mapping, improvement, simplification, accounting system implementation (ERPs), internal controls, setting up operations and processes for start-ups, shared services.</span></li>\r\n	<li><span style=\"color: #000000;\">Developing and managing finance team. Providing advice and training to non-accounting peers.</span></li>\r\n	<li><span style=\"color: #000000;\">Project management, monitoring and evaluation in complex international environment.</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column\">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Professional qualifications and memberships&nbsp;</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"color: #000000;\">Fellow Member of the Institute of Chartered Accountants of Bangladesh (ICAB).</span></li>\r\n	<li><span style=\"color: #000000;\">Member of the CPA Australia.</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column \">\r\n<div class=\"wpb_wrapper\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Education</span></h4>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<ul>\r\n	<li><span style=\"color: #000000;\">Master of Commerce (Accounting).</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<div class=\"wpb_text_column\">\r\n<h4><span style=\"font-family: arial, helvetica, sans-serif; color: #000000;\">Training/Conferences</span></h4>\r\n\r\n<p class=\"wpb_wrapper\" style=\"text-align: justify;\"><span style=\"color: #000000;\">Mr Baki has travelled extensively in Asia, Pacific and Middle Eastern region for setting up offices, implementing financial reporting system, introducing risk management and control framework, preparing manuals and guidelines, auditing accounts, forensic investigations, training and capacity building of locally engaged support staff. He is widely commended as natural team player and for his ability to overcome the cross cultural and communication barrier and for building rapport with local staff quickly.</span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', 5, '#', '#', '#', 'http://www.kathrynmoorelab.com', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE `position` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=enable,2=disable'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`id`, `name`, `status`) VALUES
(1, 'Top Menu', 1),
(2, 'Footer Menu Left', 1),
(3, 'Footer Menu Right', 1);

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `birth_date` date NOT NULL,
  `gender` varchar(255) NOT NULL,
  `address1` text NOT NULL,
  `photo` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `country` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `user_id`, `first_name`, `last_name`, `birth_date`, `gender`, `address1`, `photo`, `phone`, `country`) VALUES
(1, 22, 'Anup Kumar Roy', 'Roy', '1989-11-30', 'Male', 'sdfsdfsdf', '1510644686679.jpg', '2234234', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `sort_order`) VALUES
(1, 'Admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `featured` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `name`, `slug`, `image`, `details`, `sort_order`, `status`, `featured`) VALUES
(1, 'Logistics Services', 'logistics-services-nexcesexpress', '1567865810281.jpg', '<p><img alt=\"\" src=\"http://localhost:8080/nexcesexpress/mediaLibrary/Test/900x300xsea_freight_jpg_pagespee.bci14vn4v-.jpg\" style=\"width: 885px; height: 295px;\" /></p>\r\n\r\n<p><strong>Logistics Services</strong></p>\r\n\r\n<div class=\"col-md-9\">\r\n<p align=\"justify\">Cargo Afrique Logistics Limited provides worldwide ocean freight transportation to all major ports overseas. Through the operation of our NVOCC, we provide customized ocean carriage to meet all our customers&#39; needs. we can provide a total ocean freight solution for almost all types of shipments.</p>\r\n\r\n<p align=\"justify\">The following ocean freight transportation services are available:</p>\r\n\r\n<ul style=\"margin-left: 30px; color:#0D7ABC; \" type=\"square\">\r\n	<li>FCL (Full Container Load)</li>\r\n	<li>LCL (Less-than-Container Load)</li>\r\n	<li>Non-Containerized Cargo &amp; Oversize Loads</li>\r\n	<li>RoRo (Roll-on/Roll-off)</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p align=\"justify\">Cargo Afrique Logistics Limited offers a full array of global ocean freight transportation services. We can handle almost any weight or size shipments, from less-than-container loads to full container loads, special equipment and oversized cargo. And our state-of-the-art eTracking System provides our customers with online critical shipment information for all ocean freight consignments handled by our team.</p>\r\n\r\n<blockquote>\r\n<p align=\"justify\">From almost any origin or destination, Cargo Afrique Logistics Limited can provide modernized freight forwarding to book your cargo, arrange for pickup &amp; delivery and handle the shipping documentation.</p>\r\n</blockquote>\r\n</div>\r\n', 1, 1, 1),
(2, 'Custom Clearance', 'custom-clearance-nexcesexpress', '156786586893.jpg', '<p><img alt=\"\" src=\"http://localhost:8080/nexcesexpress/mediaLibrary/Test/900x300xcustom_brokerage_jpg_pag.r_rzahb1tf.jpg\" style=\"width: 885px; height: 295px;\" /></p>\r\n\r\n<p><strong>Custom Clearance</strong></p>\r\n\r\n<div class=\"col-md-9\">\r\n<p align=\"justify\">With the help of our experienced team of staff and good relationship with stake holders, we handle all type of Customs clearance at the ports of entry and exit within the provided grace period. These include clearance of goods under, home use, Temporally import, Prior release for time sensitive, Transit and transshipment, AOG, bonded goods, etc. We have a dedicated Customer Service team who send out progressive clearance update to customers making it easy for customers to concentrate on their core of business without getting involved in the clearance process.</p>\r\n\r\n<p align=\"justify\">The Uganda Revenue Authority mandates that importers shipping goods which require a formal entry declaration, must have a Customs bond. When importing goods, the shipment must clear customs properly. As a Customs Broker, Cargo Afrique Logistics Limited, can clear the goods through customs and have the shipment delivered to its final destination. In order for Customs clearance, proper documentation is required:</p>\r\n\r\n<ol style=\"margin-left: 30px; color:#0D7ABC; \">\r\n	<li>Bill of Lading or Airway bill</li>\r\n	<li>Commercial invoice with description and value of the goods</li>\r\n	<li>Packing List</li>\r\n	<li>Certificate of Incorporation</li>\r\n	<li>TIN Number( for border clearance into Uganda)</li>\r\n	<li>Letter authorising Cargo Afrique Logistics Limited to clear and transport cargo from Mombasa or Dar sa salaam to Uganda</li>\r\n</ol>\r\n\r\n<p align=\"justify\">Cargo Afrique logistics limited has a team of highly-trained import professionals possess a thorough knowledge of tariff schedules and Customs regulations.</p>\r\n\r\n<p align=\"justify\">Additional documentation may be needed, depending on the commodity, value and origin. Also, shipments can move in-transit to a final country of destination. You need a freight forwarding and Customs Broker organization such as Cargo Afrique logistics limited that has the expertise to get the job done right.</p>\r\n\r\n<p align=\"justify\">When you look at the World the way we do, you can see why we are the preferred Customs Broker in the East African region. <a href=\"http://www.cargoafrique.com/contact.php\">Contact us</a> today</p>\r\n</div>\r\n', 2, 1, 1),
(3, 'Air Freight', 'air-freight-nexcesexpress', '15678671201.jpg', '<p><img alt=\"\" src=\"http://localhost:8080/nexcesexpress/mediaLibrary/Test/air_freight.jpg\" style=\"width: 885px; height: 295px;\" /></p>\r\n\r\n<p><strong>Air Freight</strong><br />\r\nWe provide customer specific airfreight services to shippers and consignees of varied segments. We understand that airfreight being a premium and costly service demands quick transit and timely information about the status of freight along with most competitive rates.&nbsp;<br />\r\n<br />\r\n<strong>Our Air Export Network</strong>&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Arranging inland transportation and local trucking to gateway airports &nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n	<li>Export packaging and labeling &nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n	<li>Airline Insurance &nbsp;&nbsp;&nbsp; &nbsp;</li>\r\n	<li>Proper preparation of export documentation &nbsp;&nbsp;&nbsp; &nbsp;</li>\r\n	<li>Laisoning with AEPC, DGFT, TEXPROCIL &nbsp;&nbsp;&nbsp; &nbsp;</li>\r\n	<li>Communication of shipment status throughout the transportation chain&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n</ul>\r\n\r\n<p><strong>Global Air Import Consolidation</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />\r\nThrough our Import Consolidation Service we provide economical rates along with weekly consolidation from Far East and Europe. Our customer service staff continuously track the shipments from origin to destination through airline systems.</p>\r\n', 3, 1, 1),
(4, 'Inland Freight', 'inland-freight-nexcesexpress', '1567867084978.jpg', '<p><img alt=\"\" src=\"http://localhost:8080/nexcesexpress/mediaLibrary/Test/900x300xroad_freight_jpg_pagespe.u1d9jrboil.jpg\" style=\"width: 886px; height: 295px;\" /></p>\r\n\r\n<p><strong>Inland Freight</strong></p>\r\n\r\n<div class=\"col-md-9\">\r\n<p align=\"justify\">As contract freight haulers, Cargo Afrique Logistics Limited provides door-to-door ground freight transportation covering the entire East, Central and Southern Africa regions with proven performance in Kenya, Uganda, Tanzania, South Sudan, Rwanda and Congo DRC.</p>\r\n\r\n<h4 style=\"color: #0D7ABC\">Door-to-Door:</h4>\r\n\r\n<p align=\"justify\">Most shipments are handled Door-to-Door for business-to-business, but arrangements can be made for residential pick-up or delivery for an additional fee. For shipments traveling within the East, Central and Southern Africa regions, our service is Domestic Door-to-Door..</p>\r\n\r\n<h4 style=\"color: #0D7ABC\">Less-than Truck Load (LTL):</h4>\r\n\r\n<p align=\"justify\">For shipments less-than trailer loads (LTL), the freight is consolidated in our contracted vans and transported efficiently to the recipient&#39;s (consignee&#39;s) door. Cargo Afrique Logistics Limited is one of the most comprehensive and competitive LTL Carrier - servicing the East and Central African region.</p>\r\n\r\n<h4 style=\"color: #0D7ABC\">Full Truck Load (FTL):</h4>\r\n\r\n<p align=\"justify\">For freight consignments that require a Full Truck Load (FTL), we dedicate a van or truck for the entire freight move, from the shipper&#39;s door or warehouse until the freight is received by the recipient at final destination. Equipment hauling type will depend on the shipment&rsquo;s freight type, weight and dimensions. We also handles bulk and oversize shipments, which normally move on flat-beds, flat-racks, low-boys, low-decks and other specialized equipment. These consignments may require special permits, depending on the size and weight of the freight.</p>\r\n\r\n<h4 style=\"color: #0D7ABC\">Intermodal:</h4>\r\n\r\n<p align=\"justify\">Intermodal Freight Transport involves the transportation of freight in a container or vehicle, using multiple modes of transportation (rail, ship, and truck), without any handling of the freight itself when changing modes. Some shipments tendered for other transportation modes, such as air freight and ocean freight, can be combined with our Inland Freight Division in order to provide a complete freight transportation move and to maximize efficiencies in the shipment transaction. By combining different modes of transportation, we are able to expedite your shipments across countries and use more efficient modes of transportation for pick-up, delivery or simply transporting the product to the nearest port, airport, terminal or rail ramp. We co-ordinate Inland Freight throughout North America by truck, rail, ship or air.</p>\r\n\r\n<p align=\"justify\">We maintain excellent contacts with trucking companies and airlines in order to be able to move freight quickly, at competitive rates.</p>\r\n</div>\r\n', 4, 1, 1),
(5, 'Transportation', 'transportation-nexcesexpress', '1567867447583.jpg', '<p><img alt=\"\" src=\"http://localhost:8080/nexcesexpress/mediaLibrary/Test/900x300xcargo_insurance_jpg_page._kvezhytbl.jpg\" style=\"width: 885px; height: 295px;\" /></p>\r\n\r\n<p><strong>Transportation</strong></p>\r\n\r\n<div class=\"col-md-9\">\r\n<p align=\"justify\">As contract freight haulers, Cargo Afrique Logistics Limited provides door-to-door ground freight transportation covering the entire East, Central and Southern Africa regions with proven performance in Kenya, Uganda, Tanzania, South Sudan, Rwanda and Congo DRC.</p>\r\n\r\n<p align=\"justify\"><strong>Less-than Truck Load (LTL)</strong><br />\r\nFor shipments less-than trailer loads (LTL), the freight is consolidated in our contracted vans and transported efficiently to the recipient&#39;s (consignee&#39;s) door. Cargo Afrique Logistics Limited is one of the most comprehensive and competitive LTL Carrier - servicing the East and Central African region.</p>\r\n\r\n<p align=\"justify\"><strong>Full Truck Load (FTL)</strong><br />\r\nFor freight consignments that require a Full Truck Load (FTL), we dedicate a van or truck for the entire freight move, from the shipper&#39;s door or warehouse until the freight is received by the recipient at final destination. Equipment hauling type will depend on the shipment&rsquo;s freight type, weight and dimensions. We also handles bulk and oversize shipments, which normally move on flat-beds, flat-racks, low-boys, low-decks and other specialized equipment. These consignments may require special permits, depending on the size and weight of the freight.</p>\r\n\r\n<p align=\"justify\"><strong>Intermodal</strong><br />\r\nIntermodal Freight Transport involves the transportation of freight in a container or vehicle, using multiple modes of transportation (rail, ship, and truck), without any handling of the freight itself when changing modes. Some shipments tendered for other transportation modes, such as air freight and ocean freight, can be combined with our Inland Freight Division in order to provide a complete freight transportation move and to maximize efficiencies in the shipment transaction. By combining different modes of transportation, we are able to expedite your shipments across countries and use more efficient modes of transportation for pick-up, delivery or simply transporting the product to the nearest port, airport, terminal or rail ramp. We co-ordinate Inland Freight throughout North America by truck, rail, ship or air.</p>\r\n\r\n<p align=\"justify\">We maintain excellent contacts with trucking companies and airlines in order to be able to move freight quickly, at competitive rates.</p>\r\n</div>\r\n', 5, 1, 1),
(6, 'Warehousing', 'warehousing-nexcesexpress', '1567867287332.jpg', '<p><img alt=\"\" src=\"http://localhost:8080/nexcesexpress/mediaLibrary/Test/900x300xwarehousing_jpg_pagespee.tohiuwsmse.jpg\" style=\"width: 885px; height: 295px;\" /></p>\r\n\r\n<p><strong>Warehousing</strong><br />\r\nWe have long term contracts with a number of warehouse owners with good reputation. Our warehousing partners operate both customs bonded and Non customs bonded warehouse. In addition, we are able to manage any free warehouse of your requirement for long term, non customs bonded storage needs.</p>\r\n', 6, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shipping`
--

CREATE TABLE `shipping` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `featured` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shipping`
--

INSERT INTO `shipping` (`id`, `name`, `slug`, `image`, `details`, `sort_order`, `status`, `featured`) VALUES
(1, 'Placing Pick Up Order Online', 'placing-pick-up-order-online', '1561194022897.jpg', '<p><strong>Comprehensive services, unparalleled support</strong><br />\r\nAlthough it is essential to ensure that your critical IT systems can be access at any time for business functions but it could be cost challenging to support IT system with in-house IT staff and your business might end up being out dated. For challenges related to IT management, KHS can always help you handle it. We offer services which can help with the maintenance of the service and network of your PCs, servers, networks and ensure it stay productive at all times. We can help you design the perfect plan that would suit your business optimization needs.</p>\r\n', 1, 1, 1),
(2, 'Order Supplies', 'order-supplies', '1561197166442.jpg', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>\r\n\r\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>\r\n\r\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>\r\n', 2, 1, 1),
(3, 'Volumetric Weight', 'volumetric-weight', '1561197166442.jpg', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>\r\n\r\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>\r\n\r\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>\r\n', 3, 1, 1),
(4, 'Special Restricted Items', 'special-restricted-items', '1561197368644.jpg', '<p><strong>Stronger security, stronger business</strong><br />\r\nFor any business to achieve great success it would require a comprehensive IT security strategy. Its absence can cause a business to be exposed to several security risks. KHS IT solution makes use of the best method of building a comprehensive plan that include mobile protection, cyber security and cloud backup and recovery.</p>\r\n', 4, 1, 1),
(5, 'General Custom Info', 'general-custom-info', '1561197508881.jpg', '<div class=\"fusion-row\">\r\n<div style=\"text-align: left; font-size: 17px; line-height: 28px;\">\r\n<p><strong>Experts in all major technologies</strong></p>\r\n\r\n<p>If you desire to remain relevant in the rapidly-changing business world of today then you would need to be constantly up-to-date on technology. But this could be quite difficult to achieve especially due to level of solutions available to organization. KHS IT solution also provides consulting services and can as well help your company with their project management in order to improve their technology level.</p>\r\n</div>\r\n</div>\r\n', 5, 1, 1),
(6, 'How to Fill out Proforma', 'how-to-fill-out-proforma', '1561194022897.jpg', '<p><strong>Comprehensive services, unparalleled support</strong><br />\r\nAlthough it is essential to ensure that your critical IT systems can be access at any time for business functions but it could be cost challenging to support IT system with in-house IT staff and your business might end up being out dated. For challenges related to IT management, KHS can always help you handle it. We offer services which can help with the maintenance of the service and network of your PCs, servers, networks and ensure it stay productive at all times. We can help you design the perfect plan that would suit your business optimization needs.</p>\r\n', 1, 1, 1),
(7, 'How to fill out AWB', 'how-to-fill-out-awb', '1561197166442.jpg', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>\r\n\r\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>\r\n\r\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>\r\n', 2, 1, 1),
(8, 'Fuel Surcharge', 'fuel-surcharge', '1561197166442.jpg', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>\r\n\r\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>\r\n\r\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>\r\n', 3, 1, 1),
(9, 'FAQ', 'faq', '1561197368644.jpg', '<p><strong>Stronger security, stronger business</strong><br />\r\nFor any business to achieve great success it would require a comprehensive IT security strategy. Its absence can cause a business to be exposed to several security risks. KHS IT solution makes use of the best method of building a comprehensive plan that include mobile protection, cyber security and cloud backup and recovery.</p>\r\n', 4, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `slideshow`
--

CREATE TABLE `slideshow` (
  `id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `image` varchar(100) NOT NULL,
  `button_label` varchar(255) NOT NULL,
  `link` varchar(1000) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `position` varchar(255) NOT NULL DEFAULT 'Photo Gallery',
  `slider_type` varchar(255) NOT NULL,
  `video_url` text NOT NULL,
  `video_source` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slideshow`
--

INSERT INTO `slideshow` (`id`, `title`, `image`, `button_label`, `link`, `sort_order`, `status`, `position`, `slider_type`, `video_url`, `video_source`) VALUES
(1, 'Slider', '1567099582235.jpg', 'Watch Over View Video', 'Slider', 1, 1, 'Home Slider', 'Image Slider', '', 'Youtube'),
(2, 'Slider', '1567099617375.jpg', 'Watch Over View Video', 'Slider', 2, 1, 'Home Slider', 'Image Slider', '', 'Youtube'),
(3, 'Slider', '1511785542743.jpg', 'Slider', 'Slider', 3, 1, 'Home Slider', 'Image Slider', '', 'Youtube'),
(4, 'Slider', '1511785557132.jpg', 'Slider', 'Slider', 4, 1, 'Home Slider', 'Image Slider', '', 'Youtube'),
(5, 'Slider', '1511785557133.jpg', 'Slider', 'Slider', 5, 1, 'Home Slider', 'Image Slider', '', 'Youtube'),
(6, 'The Sheep 2018', '', 'Watch Over View Video', '', 5, 1, 'Home Slider', 'Video Slider', 'https://www.youtube.com/embed/2DoUVrrMGPE', 'Youtube');

-- --------------------------------------------------------

--
-- Table structure for table `team_category`
--

CREATE TABLE `team_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `team_category`
--

INSERT INTO `team_category` (`id`, `name`, `slug`, `status`, `sort_order`) VALUES
(1, 'Director', 'director-nexcessexpress', 1, 1),
(2, 'Manager', 'manager-nexcessexpress', 1, 2),
(3, 'Sr. Consultant', 'sr-consultant-nexcessexpress', 1, 3),
(4, 'Lead Consultant', 'lead-consultant-nexcessexpress', 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `news_date` date NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `featured` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `slug`, `image`, `details`, `news_date`, `sort_order`, `status`, `featured`) VALUES
(1, 'Bhargavi Arrunategui', 'Bhargavi-Arrunategui', '1512471281797.jpg', '<p>I highly recommend the services of KHS to my colleagues who are interested in transitioning to an electronic medical records-based practice. After successfully making this transition, we are pleased with the time-efficiency and cost-savings that have resulted. Clinicians in our practice are able to spend more time with patients and less time on paperwork. Office personnel are pleased with the increased level of efficiency with regards to documentation, communication and workflow processes. I would not hesitate to consult with the knowledgeable and courteous staff at KHS for future projects and therefore wholeheartedly recommend them to my colleagues.</p>\r\n', '2019-06-22', 1, 1, 1),
(2, 'Abdullah Al Ahad', 'Abdullah-Al-Ahad', '1561281685274.jpg', '<div class=\"col-md-12 margin-bott-30\">\r\n<p>KHS IT Solutions provides support across the entire IT spectrum, from optimizing resources to maintaining infrastructure to migrating and managing the public and private cloud.<br />\r\nFrom initial discussion, concept, design, architecture, implementation to ongoing support we provide support across the entire IT spectrum.<br />\r\nOptimizing resources to maintaining infrastructure to migrating and managing the public and private cloud, we are your single point of contact for all your technology needs.</p>\r\n</div>\r\n', '2019-06-23', 2, 1, 1),
(3, 'as', 'as', '1562233889260.png', '<p>as</p>\r\n', '2019-07-04', 3, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `email` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `role` int(11) NOT NULL,
  `email_verified` tinyint(4) NOT NULL DEFAULT '0',
  `verification_code` varchar(400) DEFAULT NULL,
  `forgot_pass_code` varchar(300) NOT NULL,
  `date_of_registration` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `active`, `email`, `first_name`, `last_name`, `role`, `email_verified`, `verification_code`, `forgot_pass_code`, `date_of_registration`, `added_by`) VALUES
(1, 'admin', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 'info@212.com', 'Admin', '', 1, 1, NULL, '', '2017-09-28 16:00:00', 0),
(22, 'anup@coder71.com', '0653a342bcbe488c7a0e74423425678b', 1, 'anup@coder71.com', 'Anup Kumar Roy', 'Roy', 3, 1, 'f72d82b42fa8f5de8d0d4970f3f02b9d', '', '2017-11-07 06:01:21', 0),
(23, 'jalil@coder71.com', '642cb3624b770debc583a27e0abcb688', 1, 'jalil@coder71.com', 'Abdul Jalil', '', 2, 1, '13a261e3b91a0e36626b49e5fa1ea7b3', '', '2017-11-07 06:01:57', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_details`
--
ALTER TABLE `book_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletter_category`
--
ALTER TABLE `newsletter_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletter_client`
--
ALTER TABLE `newsletter_client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_events`
--
ALTER TABLE `news_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_team`
--
ALTER TABLE `our_team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `partner_category`
--
ALTER TABLE `partner_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping`
--
ALTER TABLE `shipping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slideshow`
--
ALTER TABLE `slideshow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_category`
--
ALTER TABLE `team_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_details`
--
ALTER TABLE `book_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `newsletter_category`
--
ALTER TABLE `newsletter_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `newsletter_client`
--
ALTER TABLE `newsletter_client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `news_events`
--
ALTER TABLE `news_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `our_team`
--
ALTER TABLE `our_team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `partner`
--
ALTER TABLE `partner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `partner_category`
--
ALTER TABLE `partner_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `shipping`
--
ALTER TABLE `shipping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `slideshow`
--
ALTER TABLE `slideshow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `team_category`
--
ALTER TABLE `team_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
