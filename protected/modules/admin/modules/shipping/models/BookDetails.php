<?php

/**
 * This is the model class for table "book_details".
 *
 * The followings are the available columns in table 'book_details':
 * @property integer $id
 * @property string $nex_code
 * @property string $sender_attention
 * @property string $sender_company
 * @property string $sender_address
 * @property string $sender_city
 * @property string $sender_zip
 * @property string $sender_country
 * @property string $sender_phone
 * @property string $sender_email
 * @property string $receiver_attention
 * @property string $receiver_company
 * @property string $receiver_address
 * @property string $receiver_city
 * @property string $receiver_zip
 * @property string $receiver_country
 * @property string $receiver_phone
 * @property string $receiver_email
 * @property string $payment_method
 * @property string $shipping_ref
 * @property integer $pices
 * @property integer $values
 * @property string $remarks
 * @property string $billing_weight
 * @property string $item_type
 * @property string $item_description
 * @property string $dimensions
 * @property string $ready_datetime
 * @property string $arrival_datetime
 * @property integer $status
 * @property integer $entry_by
 * @property string $entry_date
 * @property integer $update_by
 * @property string $update_time
 */
class BookDetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'book_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nex_code','required'), 
			array('sender_attention','required','message'=>'Cannot be blank'),
			array('sender_company','required','message'=>'Cannot be blank'),
			// array('sender_address','required','message'=>'Cannot be blank'),
			// array('sender_city', 'required','message'=>'Cannot be blank'),
			// array('sender_zip','required','message'=>'Cannot be blank'), 
			// array('sender_country','required','message'=>'Cannot be blank'),
			// array('sender_phone','required','message'=>'Cannot be blank'), 
			// array('sender_email','required','message'=>'Cannot be blank'),
			// array('receiver_attention','required','message'=>'Cannot be blank'), 
			// array('receiver_company','required','message'=>'Cannot be blank'),
			// array('receiver_address','required','message'=>'Cannot be blank'),
			// array('receiver_city','required','message'=>'Cannot be blank'),
			// array('receiver_zip','required','message'=>'Cannot be blank'), 
			// array('receiver_country','required','message'=>'Cannot be blank'), 
			// array('receiver_phone','required','message'=>'Cannot be blank'), 
			// array('receiver_email','required','message'=>'Cannot be blank'),  
			// array('payment_method','required','message'=>'Cannot be blank'),
			// array('shipping_ref','required','message'=>'Cannot be blank'),
			// array('pices','required','message'=>'Cannot be blank'), 
			// array('values','required','message'=>'Cannot be blank'), 
			// array('remarks','required','message'=>'Cannot be blank'),
			// array('billing_weight','required','message'=>'Cannot be blank'), 
			// array('item_type','required','message'=>'Cannot be blank'),
			// array('item_description','required','message'=>'Cannot be blank'),
			// array('dimensions','required','message'=>'Cannot be blank'),
			// array('entry_by, entry_date', 'required'),
			
			//array('ready_datetime, arrival_datetime, entry_by, entry_date, update_by, update_time', 'required'),
			array('pices, values, status, entry_by, update_by', 'numerical', 'integerOnly'=>true),
			array('nex_code, sender_attention, sender_company, sender_email, receiver_attention, receiver_company, receiver_email, shipping_ref', 'length', 'max'=>255),
			array('sender_city, sender_country, receiver_city, receiver_country, payment_method', 'length', 'max'=>100),
			array('sender_zip, sender_phone, receiver_zip, receiver_phone, item_type', 'length', 'max'=>50),
			array('billing_weight', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nex_code, sender_attention, sender_company, sender_address, sender_city, sender_zip, sender_country, sender_phone, sender_email, receiver_attention, receiver_company, receiver_address, receiver_city, receiver_zip, receiver_country, receiver_phone, receiver_email, payment_method, shipping_ref, pices, values, remarks, billing_weight, item_type, item_description, dimensions, ready_datetime, arrival_datetime, status, entry_by, entry_date, update_by, update_time', 'safe', 'on'=>'search'),
		);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nex_code' => 'Nex Code',
			'sender_attention' => 'Attention',
			'sender_company' => 'Company',
			'sender_address' => 'Address',
			'sender_city' => 'Sender City',
			'sender_zip' => 'Sender Zip',
			'sender_country' => 'Sender Country',
			'sender_phone' => 'Sender Phone',
			'sender_email' => 'Sender Email',
			'receiver_attention' => 'Attention',
			'receiver_company' => 'Company',
			'receiver_address' => 'Address',
			'receiver_city' => 'Receiver City',
			'receiver_zip' => 'Receiver Zip',
			'receiver_country' => 'Receiver Country',
			'receiver_phone' => 'Receiver Phone',
			'receiver_email' => 'Receiver Email',
			'payment_method' => 'Payment Method',
			'shipping_ref' => 'Shipping Ref',
			'pices' => 'Pices',
			'values' => 'Values',
			'remarks' => 'Remarks',
			'billing_weight' => 'Billing Weight',
			'item_type' => 'Item Type',
			'item_description' => 'Item Description',
			'dimensions' => 'Dimensions',
			'ready_datetime' => 'Ready Datetime',
			'arrival_datetime' => 'Arrival Datetime',
			'status' => 'Status',
			'entry_by' => 'Entry By',
			'entry_date' => 'Entry Date',
			'update_by' => 'Update By',
			'update_time' => 'Update Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nex_code',$this->nex_code,true);
		$criteria->compare('sender_attention',$this->sender_attention,true);
		$criteria->compare('sender_company',$this->sender_company,true);
		$criteria->compare('sender_address',$this->sender_address,true);
		$criteria->compare('sender_city',$this->sender_city,true);
		$criteria->compare('sender_zip',$this->sender_zip,true);
		$criteria->compare('sender_country',$this->sender_country,true);
		$criteria->compare('sender_phone',$this->sender_phone,true);
		$criteria->compare('sender_email',$this->sender_email,true);
		$criteria->compare('receiver_attention',$this->receiver_attention,true);
		$criteria->compare('receiver_company',$this->receiver_company,true);
		$criteria->compare('receiver_address',$this->receiver_address,true);
		$criteria->compare('receiver_city',$this->receiver_city,true);
		$criteria->compare('receiver_zip',$this->receiver_zip,true);
		$criteria->compare('receiver_country',$this->receiver_country,true);
		$criteria->compare('receiver_phone',$this->receiver_phone,true);
		$criteria->compare('receiver_email',$this->receiver_email,true);
		$criteria->compare('payment_method',$this->payment_method,true);
		$criteria->compare('shipping_ref',$this->shipping_ref,true);
		$criteria->compare('pices',$this->pices);
		$criteria->compare('values',$this->values);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('billing_weight',$this->billing_weight,true);
		$criteria->compare('item_type',$this->item_type,true);
		$criteria->compare('item_description',$this->item_description,true);
		$criteria->compare('dimensions',$this->dimensions,true);
		$criteria->compare('ready_datetime',$this->ready_datetime,true);
		$criteria->compare('arrival_datetime',$this->arrival_datetime,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('entry_by',$this->entry_by);
		$criteria->compare('entry_date',$this->entry_date,true);
		$criteria->compare('update_by',$this->update_by);
		$criteria->compare('update_time',$this->update_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function makeLink($id){
        //return Yii::app()->createAbsoluteUrl('//product/' . $id, array('name' => self::model()->findByPk($id)->name));
        return Yii::app()->createAbsoluteUrl('//shipping/' . self::model()->findByPk($id)->slug);
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BookDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getBarCode($orderNumber, $width = 100, $fontsize = 13, $height = 60, $containerstyle = 'float:right;clear:both') {
        return "<table style='" . $containerstyle . "'><tr><td><img width='" . $width . "' height='" . $height . "' src='" . Yii::app()->params->SERVER_HOSTS . "barcodegenerator/generatebarcode?code=" . $orderNumber . "'/></td></tr><tr><td style='text-align:center; font-size:16px;font-weight:bold; color:#000;'>" . $orderNumber . "</td></tr></table>";
    }

    public function sendRegistrationSuccessMail($id) {
        $MailSettings = SiteSettings::model()->find();
        
        $model = self::model()->findByPk($id);
        $mail = new YiiMailer('withBooking', array('code' => $model->verification_code,'firstName'=>$model->first_name,'model'=>$model));
        $mail->setLayout('mail');
        
        $mail->setFrom($MailSettings->email, $MailSettings->name);
        //$mail->setFrom('info@99handicraft.com', 'Iam99');
        $mail->setSubject('New Customer registration - '.$MailSettings->name);
        $mail->setTo($model->email);
        $mail->send();
    }
}
