<?php //Yii::app()->params->currencySymbol = strtoupper($order->currency).' ';?>
<style>
    .ordertbl table{width:100%!important; text-align: center;}
</style>
<div class="container" style="padding: 20px 0px;">
<div class="row"><div class="col-md-12"><?php echo Yii::app()->easycode->showNotification(); ?></div></div>
<div class="row open_page hi">
    
    <!---Action button-->
    
    <a href="<?= Yii::app()->createUrl('//admin/shipping');?>"><button style='padding-top:4px;padding-bottom:4px' type="button" class="btn btn-warning"><i class="fa fa-reply"></i> Back</button></a>
    <?php
    if ($this->action->id != 'print') {
        if($order->status!='Shipped')
            //$this->renderPartial('_' . $order->status . 'ActionButtons', array('type' => 'button', 'order' => $order),false, false);
        ?>
        
        <div class="btn-group">
        <button data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle" type="button"><i class='fa fa-pagelines'></i> Invoice
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul role="menu" class="dropdown-menu">
            <li><a href="#" data-url="<?= $this->createUrl('sendInvoice', array('id' => $order->id)) ?>" onclick="sendInvoice($(this))" class="btn btn-sm" style="text-align:left; font-weight: normal"><i class="fa fa-envelope"></i> Send Invoice</a></li>
        
            <li><a href="<?= $this->createUrl('downloadInvoice', array('id' => $order->id)) ?>" data-url="<?= $this->createUrl('downloadInvoice', array('id' => $order->id)) ?>" onclick="//downloadInvoice($(this));" target="_blank" class="btn btn-sm" style="text-align:left; font-weight: normal"><i class="fa fa-download"></i> Download Invoice</a></li>
        </ul>
        </div>
        <a target="_blank" href="<?php echo Yii::app()->createUrl('//admin/order/print/' . $order->id) ?>"><button style='padding-top:4px;padding-bottom:4px' type="button" class="btn btn-primary"><i class="fa fa-print"></i> Print</button></a>
        
                <?php if ($order->status != 'Delivered'){?>
                <!-- <a href="<?php //echo Yii::app()->createUrl('//admin/order/update/' . $order->id) ?>"><button style='padding-top:4px;padding-bottom:4px' type="button" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</button></a> -->
                <?php } ?>
        <hr>
    <?php } ?>
    <!---Action button-->

    <script>
        function sendInvoice($this) {
            if (confirm('Are your sure to send invoice to customer?')) {
                $('.sload').html('<i class="fa fa-circle-o-notch fa-spin"></i> Please Wait...');
                $.post($this.attr('data-url'), {}, function (data) {
                    if (data == '1') {
                        alert('Invoice sent successfully.');
                        $('.sload').html('<i class="fa fa-envelope"></i> Send Invoice');
                    } else {
                        alert('Invoice not sent.');
                        $('.sload').html('<i class="fa fa-envelope"></i> Send Invoice');
                    }
                });
            }
        }
        function downloadInvoice($this) {
            $('.dload').html('<i class="fa fa-circle-o-notch fa-spin"></i> Please Wait...');
            $.post($this.attr('data-url'), {}, function (data) {
                if (data !== '') {
                    $('.dload').html('<i class="fa fa-download"></i> Download Invoice');
                } else {
                    $('.dload').html('<i class="fa fa-download"></i> Download Invoice');
                }
            });
        }
    </script>
    <?php $storeSetting = SiteSettings::model()->find(); ?>
    <div class="row custom-page-header myprint">
        <div class="col-md-4">
            <a class="navbar-brand" href="<?php echo Yii::app()->createUrl('//admin/') ?>"><img src="<?php echo $this->siteLogo; ?>" height="40"/></a>
            <div style="clear:both;margin-left: 15px;">
                <strong>Email:</strong> <?php echo $storeSetting->email; ?><br>
                <!-- <strong>Web:</strong> <a href="<?//= Yii::app()->params->weblink?>"><?//= Yii::app()->params->web?></a> -->
            </div>
        </div>
        <style>
            .ststusStyle{
                /*border:1px solid #ccc;*/
                text-align: center;
                padding: 10px 0px!important; 
                background-color: #31B0D5; 
                color: #fff;
                border-radius: 4px;
            }
            .paymentStatusDue{
               /*border:1px solid #ccc;*/
               text-align: center;
               padding: 10px 0px!important; 
               background-color: #C9302C;
               color: #fff;
               border-radius: 4px;
            }
            .paymentStatusPaid{
               /*border:1px solid #ccc;*/
               text-align: center;
               padding: 10px 0px!important;
               background-color: #449D44; 
               color: #fff;
            }
        </style>
        <div class="col-md-4">
            <div class="col-md-6">
                <h2 class="ststusStyle"><?php echo $order->status; ?></h2>
            </div>
            <div class="col-md-6">
                
            </div>
        </div>
        <div class="col-md-4 action-button" style="text-align: right">
            <div class="ordertbl"><?php //echo Order::model()->getBarCode($order->order_number, '250px', 13) ?></div>
            <div style="text-align:center;">
                <strong>Date:</strong> <?php if ($order->status=='Pending' || $order->status=='Canceled'){
                    
                    echo date('d-m-Y', strtotime($order->order_date)); 
                }else{
                    //echo date('d-m-Y', strtotime($order->confirmed_date));
                }
                ?><br>
                <!--<strong>Order Status:</strong> <?php //echo $order->status; ?>-->
            </div>

        </div>
    </div>
</div>
<div class="row">
      
    
</div>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">Product Info</div>
        <div class="panel-body verticalTop">
            <table class="table checkout-table gu12" style="margin-bottom:0px;">
                <thead>
                    <tr>
                        <th width="70">Image</th>
                        <th class="name">Product Name</th>
                        <th class="price" width="160" style="text-align: right;">Qty.</th>
                        <th class="price" style="text-align: right;">Price</th>
                        <th class="price" width="130" style="text-align: right;">Total</th>
                        <th style="width: 1%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    
                        <tr>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th align="right" style="text-align:right;" colspan="4" class="price">Total:</th>
                        <th style="text-align: right;" class="price grandTotal"></th>
                    </tr>
                    
                    <tr>
                        <th align="right" style="text-align:right;" colspan="4" class="price">Shipping Cost:</th>
                       
                    </tr>

                  
                        

                    <tr>
                        <th align="right" style="text-align:right;" colspan="4" class="price">Grand Total:</th>
                       
                    </tr>                                            
                </tfoot>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            
        </div>
        <div class="col-md-6">
           
        </div>
    </div>
    
    
    <!---Action button-->
    <hr>
   
        
    <!---Action button-->
</div>

</div>
<style>
    .custom-data-table tbody tr td{vertical-align: middle}
    .right-align{text-align: right}
    center-align{text-align: center}
</style>


<!-- manual payment form -->
<div class="modal fade" id="manualPaymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Payment Information</h4>
            </div>
            <div class="modal-body">

                <div role="alert" class="alert alert-warning alert-dismissible fade in" style="display: none">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
                    <strong></strong>
                </div>

                <form>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Reference Number:</label>
                        <input type="text" class="form-control" id="refno">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Payment Method:</label>
                        <!-- <?//= CHtml::dropDownList('method', 2, CHtml::listdata(PaymentMethods::model()->findAll(), id, name), array('class' => 'form-control', 'id' => 'method')) ?> -->
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <i class="fa fa-cog fa-spin" id="manualPaymentLoading" style="display:none"></i>
                <button type="button" class="btn btn-primary" id="saveManualPayment">Save</button>
            </div>
        </div>
    </div>
</div>
</div>


<!-- manual payment form -->