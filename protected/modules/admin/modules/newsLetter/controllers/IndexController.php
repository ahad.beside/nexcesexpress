<?php

class IndexController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'index', 'view', 'admin','sendNewsletter', 'delete','getClientInformation'),
                'roles' => array('Admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

/**
 * Displays a particular model.
 * @param integer $id the ID of the model to be displayed
 */
public function actionGetClientInformation() {
if (isset($_POST['type'])) {
    //echo $_POST['type'];
if($_POST['type']=='All'){
$products = NewsLetterClient::model()->findAll();
$selected='selected';
}elseif($_POST['type']=='Selected'){
   $products = NewsLetterClient::model()->findAll();
   $selected='';
}else{
$products = NewsLetterClient::model()->findAll(array(
'condition' => 'category_id=:category_id',
'params' => array(':category_id' => $_POST['type']),
));
$selected='selected';
}
echo "<option value=''>Select Client</option>";
foreach ($products as $item):
echo "<option ".$selected." value='" . $item->email . "'>" . $item->name . "</option>";
endforeach;
}
Yii::app()->end();
}
	public function actionSendNewsletter() {
        $this->pageTitle = 'Newsletter';
        $model= new Newsletter;
        if($_POST['Newsletter']){
            //print_r($_POST['Newsletter']);exit;
        $subject = $_POST['Newsletter']['subject'];
        $mail = new YiiMailer('withHtmlNewsletter', array('text' => $_POST['Newsletter']['message']));
        $mail->setLayout('mail');
        $mail->setFrom($this->adminMail, $this->siteName);
        $mail->setSubject($subject);
        //$mail->setBody($_POST['Newsletter']['message']);
        $mail->setTo($this->adminMail);
        if($_POST['Newsletter']['client_type']==7){
        	$mail->AddBCC($_POST['Newsletter']['custom_client']);
        }else{
        	for($i=0;$i<sizeof($_POST['Newsletter']['client']);$i++){
                $mail->AddBCC($_POST['Newsletter']['client'][$i]);
            }
        }
            
        if($mail->send())
            Yii::app()->user->setFlash('success', $this->pageTitle . ' ' .'Send successfully');

        }
        $this->render('newsletter', array(
            'model' => $model,
        ));
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->pageTitle='New NewsLetterClient';
		$model=new NewsLetterClient;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['NewsLetterClient']))
		{
			$model->attributes=$_POST['NewsLetterClient'];

			if($model->save()){
				Yii::app()->user->setFlash('success', "Success: NewsLetter Client created successfully");
                    $this->redirect(array('index'));
			}else{
				Yii::app()->user->setFlash('warning', "Warning: Please check the form carefully for errors!");
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'data'=>$data,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->pageTitle='Update NewsLetter Client';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['NewsLetterClient']))
		{
			$model->attributes=$_POST['NewsLetterClient'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Success: NewsLetterClient created successfully");
                    $this->redirect(array('index'));
			}else{
				Yii::app()->user->setFlash('warning', "Warning: Please check the form carefully for errors!");
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'data'=>$data,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		// $dataProvider=new CActiveDataProvider('OurTeam');
		// $this->render('index',array(
		// 	'dataProvider'=>$dataProvider,
		// ));
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->pageTitle='Newsletter Client';
		$model=new NewsLetterClient('search');
		$data['networkCategory']=NetworkCategory::model()->findAll("status=1 order by sort_order");
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['NewsLetterClient']))
			$model->attributes=$_GET['NewsLetterClient'];
		//if(isset($_GET['NewsLetterClient']['category_id']) && $_GET['NewsLetterClient']['category_id']!='All')
			//$model->category_id=intval($_GET['NewsLetterClient']['category_id']);
		$this->render('admin',array(
			'model'=>$model,
			'data'=>$data,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return OurTeam the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=NewsLetterClient::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param OurTeam $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='our-team-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
