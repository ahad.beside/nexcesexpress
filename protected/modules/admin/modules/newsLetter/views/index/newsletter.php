<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1><?= $this->pageTitle ?></h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        <div class="row">
                            <!-- <div class="col-md-3">
                                <?//= $this->renderPartial('sidebar',false,true)?>
                            </div> -->
                            <div class="col-md-12">
                                <div class="portlet box blue">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i><?= $this->pageTitle?> </div>
                                    </div>
                                    <div class="portlet-body" style="display: block;">
                                        <?php
                                        $form = $this->beginWidget('CActiveForm', array(
                                            'id' => 'settings-form',
                                            //'action'=>$this->createUrl('//settings#genarel'),
                                            'enableAjaxValidation' => false,
                                            'enableClientValidation' => true,
                                            'clientOptions' => array(
                                                'validateOnSubmit' => true,
                                                'validateOnChange' => true,
                                            ),
                                            'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form', 'enctype' => 'multipart/form-data'),
                                        ));
                                        ?>
                                        <div class="form-body">
                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'client_type', array('class' => 'col-md-3 control-label')); 
                                                ?>
                                                <div class="col-md-9">

                                                    <select class="form-control" name="Newsletter[client_type]" id="Newsletter_client_type">
                                                    <option value="">All</option>
                                                    <option value="Selected">Selected</option>
                                                    <option value="7">Custom</option>
                                                    <?php 
                                                    $newsLetter=NewsLetterCategory::model()->findAll();
                                                    foreach($newsLetter as $rowCat):
                                                    ?>
                                                    <option value="<?= $rowCat->id?>"><?= $rowCat->name?></option>
                                                <?php endforeach;?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <?php echo $form->labelEx($model, 'client', array('class' => 'col-md-3 control-label')); 
                                                $clientEmail=NewsLetterClient::model()->findAll();
                                                //echo count($clientEmail);
                                                foreach($clientEmail as $rowEmail):
                                                    $emailselect[]=$rowEmail->email;
                                                    endforeach;
                                                ?>
                                                <div class="col-md-9 clientAll">
                                                    <?php echo CHtml::dropDownList('Newsletter[client]', $emailselect,CHtml::listData(NewsLetterClient::model()->findAll(),'email','name'), array('class' => 'form-control select2','multiple'=>'multiple')); ?>
                                                </div>

                                                <div class="col-md-9 clientCustom">
                                                    <?php echo $form->textField($model, 'custom_client', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <?php echo $form->labelEx($model, 'subject', array('class' => 'col-md-3 control-label')); ?>
                                                <div class="col-md-9">
                                                    <?php echo $form->textField($model, 'subject', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <?php echo $form->labelEx($model, 'sender_name', array('class' => 'col-md-3 control-label')); ?>
                                                <div class="col-md-9">
                                                    <?php echo $form->textField($model, 'sender_name', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <?php echo $form->labelEx($model, 'sender_email', array('class' => 'col-md-3 control-label')); ?>
                                                <div class="col-md-9">
                                                    <?php echo $form->textField($model, 'sender_email', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <?php echo $form->labelEx($model, 'message', array('class' => 'col-md-3 control-label')); ?>
                                                <div class="col-md-9">
                                                    <?php echo $form->textArea($model, 'message', array('row' => 6, 'col' => 50, 'class' => 'form-control','id'=>'editorDes')); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">Send</button>
                                                </div>
                                            </div>
                                        </div>
                                        <?php $this->endWidget(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->
    </div>
</div>

<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/global/plugins/select2/css/select2.min.css'); ?>

<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/global/plugins/select2/css/select2-bootstrap.min.css'); ?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/global/scripts/components-select2.min.js', CClientScript::POS_END); ?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/global/plugins/select2/js/select2.full.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js', CClientScript::POS_END);?>

<script>
$(document).ready(function(){
    //$('#Newsletter_client_type').prepend('<option value="Selected">Selected</option>');
    $('.clientCustom').hide();
});
    $('#Newsletter_client_type').change(function(){
        var type=$(this).val();
        if(type==7){
            $('.clientAll').hide();
            $('.clientCustom').show();
        }else{
            $('.clientAll').show();
            $('.clientCustom').hide();
        if(type==''){
            type='All';
        }else{
            type=type;
        }
        $.post("<?= Yii::app()->createUrl('//admin/newsLetter/index/getClientInformation') ?>", {type: type}, function (data) {
            //alert(data);
            $('#Newsletter_client').html(data);
            $('#Newsletter_client').trigger("chosen:updated");
            //$('#PrescribeDiagnosis_day').trigger('change');
            });
    }
    });
</script>
<script> 
var roxyFileman = '<?php echo Yii::app()->request->baseUrl ?>/fileman/index.html'; 
$(function(){
   CKEDITOR.replace( 'editorDes',{filebrowserBrowseUrl:roxyFileman,
    filebrowserImageBrowseUrl:roxyFileman+'?type=image',
    removeDialogTabs: 'link:upload;image:upload'}); 
});
 </script>
 <style type="text/css" media="screen">
     .select2-selection {
    height: auto!important;
    padding: 5px 10px;
}
 </style>
