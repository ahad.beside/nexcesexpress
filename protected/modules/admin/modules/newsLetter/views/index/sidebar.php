<div class="list-group">
    <?php 
    $conActionId=Yii::app()->controller->action->id;
    $conId=Yii::app()->controller->id;

    if(strtolower($conId)=='index' && strtolower($conActionId)=='index')
        $newsletter='active';
    else
        $newsletter='';

    if(strtolower($conId)=='newslettercategory' && strtolower($conActionId)=='index')
        $newsletterCategory='active';
    else
        $newsletterCategory='';

    if(strtolower($conId)=='index' && strtolower($conActionId)=='sendnewsletter')
        $sendnewsletter='active';
    else
        $sendnewsletter='';
    
    ?>

    <?= CHtml::link('Newsletter Client',$this->createUrl('//admin/newsLetter/index'), array('class'=>'list-group-item '.$newsletter,'data-tag'=>'genarel')) ?>

    <?= CHtml::link('Newsletter Category',$this->createUrl('//admin/newsLetter/newsLetterCategory'), array('class'=>'list-group-item '.$newsletterCategory,'data-tag'=>'genarel')) ?>

    <?= CHtml::link('Send Newsletter',$this->createUrl('//admin/newsLetter/index/sendNewsLetter'), array('class'=>'list-group-item '.$sendnewsletter,'data-tag'=>'genarel')) ?>

</div>
<style>
    .countColor{
        color:green;
    }
    .countColorP{
        color:#FF9900;
    }
    .countColorR{
        color:red;
    }
</style>
<script type="text/javascript">
    /*$(document).ready(function(){
        $(".list-group-item").removeClass('active');
        $(".list-group-item[data-tag='"+window.location.hash.substr(1)+"']").addClass('active');
    });*/
</script>