<?php

class IndexController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'index', 'view', 'admin', 'delete'),
                'roles' => array('Admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->pageTitle='New News';
		$model=new NewsEvents;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['NewsEvents']))
		{
			$model->attributes=$_POST['NewsEvents'];
			$model->slug = Yii::app()->easycode->makeSlug('NewsEvents','slug',trim($model->name));
			$model->sort_order=Yii::app()->easycode->getLastSortingNumber('NewsEvents', 'sort_order');
			$model->news_date=date('Y-m-d',strtotime($_POST['NewsEvents']['news_date']));
			$uploadedFile = CUploadedFile::getInstance($model, "image");
                if ($uploadedFile) {
                    $fileName = Yii::app()->easycode->genFileName($uploadedFile->extensionName);
                    $model->image = $fileName;
                }
			if($model->save()){
				if ($uploadedFile) {
				$uploadedFile->saveAs(UPLOAD . Yii::app()->params->newsDir . $fileName);
			}
				Yii::app()->user->setFlash('success', "Success: News created successfully");
                    $this->redirect(array('index'));
			}else{
				Yii::app()->user->setFlash('warning', "Warning: Please check the form carefully for errors!");
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->pageTitle='Update News';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$preImage = $model->image;
		$preName = $model->name;
		if(isset($_POST['NewsEvents']))
		{
			$model->attributes=$_POST['NewsEvents'];
			$uploadedFile = CUploadedFile::getInstance($model, "image");
                if ($uploadedFile) {
                    $fileName = Yii::app()->easycode->genFileName($uploadedFile->extensionName);
                    $model->image = $fileName;
                }else{
                	$model->image = $preImage;
                }
                $model->news_date=date('Y-m-d',strtotime($_POST['NewsEvents']['news_date']));
                if($preName!=$_POST['NewsEvents']['name'])
            		$model->slug = Yii::app()->easycode->makeSlug('NewsEvents','slug',trim($model->name));
			if($model->save()){
				if ($uploadedFile) {
				$uploadedFile->saveAs(UPLOAD . Yii::app()->params->newsDir . $fileName);
				Yii::app()->easycode->deleteFile($preImage,'news');
			}
				Yii::app()->user->setFlash('success', "Success: NewsEvents Updated successfully");
                    $this->redirect(array('index'));
			}else{
				Yii::app()->user->setFlash('warning', "Warning: Please check the form carefully for errors!");
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		// $dataProvider=new CActiveDataProvider('NewsEvents');
		// $this->render('index',array(
		// 	'dataProvider'=>$dataProvider,
		// ));
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->pageTitle='News';
		$model=new NewsEvents('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['NewsEvents']))
			$model->attributes=$_GET['NewsEvents'];
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return NewsEvents the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=NewsEvents::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param NewsEvents $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='our-team-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
