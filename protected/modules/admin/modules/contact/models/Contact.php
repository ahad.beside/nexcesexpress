<?php

/**
 * This is the model class for table "contact_us".
 *
 * The followings are the available columns in table 'contact_us':
 * @property integer $id
 * @property string $title
 * @property string $name
 * @property string $description
 * @property string $site_map
 * @property string $slug
 * @property string $meta_keyword
 * @property string $meta_description
 * @property integer $status
 * @property integer $entry_by
 * @property string $entry_date
 * @property integer $update_by
 * @property string $update_time
 * @property string $attachment_title
 * @property string $attachment
 */
class Contact extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contact';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, name, description, site_map, slug, meta_keyword, meta_description, entry_by, entry_date, update_by, attachment_title, attachment', 'required'),
			array('status, entry_by, update_by', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>300),
			array('meta_keyword, meta_description', 'length', 'max'=>500),
			array('attachment_title, attachment', 'length', 'max'=>255),
			array('update_time', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, name, description, site_map, slug, meta_keyword, meta_description, status, entry_by, entry_date, update_by, update_time, attachment_title, attachment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'name' => 'Name',
			'description' => 'Description',
			'site_map' => 'Site Map',
			'slug' => 'Slug',
			'meta_keyword' => 'Meta Keyword',
			'meta_description' => 'Meta Description',
			'status' => 'Status',
			'entry_by' => 'Entry By',
			'entry_date' => 'Entry Date',
			'update_by' => 'Update By',
			'update_time' => 'Update Time',
			'attachment_title' => 'Attachment Title',
			'attachment' => 'Attachment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('site_map',$this->site_map,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('meta_keyword',$this->meta_keyword,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('entry_by',$this->entry_by);
		$criteria->compare('entry_date',$this->entry_date,true);
		$criteria->compare('update_by',$this->update_by);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('attachment_title',$this->attachment_title,true);
		$criteria->compare('attachment',$this->attachment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function makeLink($id){
        //return Yii::app()->createAbsoluteUrl('//product/' . $id, array('name' => self::model()->findByPk($id)->name));
        return Yii::app()->createAbsoluteUrl('//contact/' . self::model()->findByPk($id)->slug);
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ContactUs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
