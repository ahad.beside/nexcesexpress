<div class="page-wrapper-row full-height">
		<div class="page-wrapper-middle">
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<!-- BEGIN PAGE HEAD-->
					<div class="page-head">
						<div class="container">
							<!-- BEGIN PAGE TITLE -->
							<div class="page-title">
								<h1><?= $this->pageTitle ?>
								</h1>
							</div>
							<!-- END PAGE TITLE -->
							<div class="page-action-buttons">
								<a class="btn btn-sm btn-primary" href="javascript:void(0)" onclick="javascript:$('.grid-search-portlet').toggle('slow')" title="Search"> <i class="fa fa-search"></i> Search</a>
								<a class="btn btn-sm btn-primary" href="<?= $this->createUrl('create') ?>" title="Add Member"> <i class="fa fa-plus"></i> Add Member</a>
							</div>
						</div>
					</div>
					<!-- END PAGE HEAD-->
					<!-- BEGIN PAGE CONTENT BODY -->
					<div class="page-content">
						<div class="container">
							<div class="row">
								<div class="col-md-2">
									<?php $this->renderPartial('sidebar',array('data'=>$data)) ?>
								</div>
								<div class="col-md-10">
									<?php $this->renderPartial('_search', array('model' => $model)) ?>
									<div class="portlet box">
										<div class="portlet-body" style="display: block;">
											<?php
											$this->widget('zii.widgets.grid.CGridView', array(
												'id' => 'member-view-data',
												'htmlOptions' => array('class' => 'data-grid table-responsive'),
												'itemsCssClass' => 'table table-striped table-bordered table-hover',
												'template' => '{items}{pager}',
												'dataProvider' => $model->search(),
												'filter' => $model,
                            					'filterCssClass' => 'filter-inputs',
												'columns' => array(
//                array(
//                    'class' => 'CCheckBoxColumn',
//                    'value' => '$data->id',
//                    'selectableRows' => '2',
//                    'id' => 'actionCheck'
//                ),
													array(
														'class' => 'IndexColumn',
														'header' => '#',
														'headerHtmlOptions' => array('style' => 'width:20px;'),
													),

													array(
						                            'name'=>'image',
						                            'type'=>'raw',
						                            'value'=>'Yii::app()->easycode->showImage($data->image, 80, 80,true,true,Yii::app()->params->teamDir)',
						                            'filter'=>false,
						                        	),
													array(
													'name'=>'category_id',
													'value'=>'$data->category->name',
													'filter'=>CHtml::dropdownlist("OurTeam[category_id]",$model->category_id,CHtml::listData(TeamCategory::model()->findAll("status=1"),id,name),array("class"=>'form-control','empty'=>'All')),
													),
													array(
														'name'=>'name',
														'value'=>'$data->name',
														'filter'=>CHtml::textField("OurTeam[name]",$model->name,array("class"=>'form-control')),
													),
													array(
														'name'=>'email',
														'value'=>'$data->email',
														'filter'=>CHtml::textField("OurTeam[email]",$model->email,array("class"=>'form-control')),
													),
													array(
														'name'=>'phone',
														'value'=>'$data->phone',
														'filter'=>CHtml::textField("OurTeam[phone]",$model->phone,array("class"=>'form-control')),
													),
													array(
														'name'=>'designation',
														'value'=>'$data->designation',
														'filter'=>CHtml::textField("OurTeam[designation]",$model->designation,array("class"=>'form-control')),
													),
                                                    array(
                                                    'name'=>'status',
                                                    'value'=>'Yii::app()->easycode->getStatus($data->status)',
                                                    'type'=>'raw',
                                                    'filter'=>CHtml::dropdownlist("OurTeam[status]",$model->status,array("1"=>"Enabled","0"=>"Disabled"),array("class"=>'form-control','empty'=>'All')),
                                                ),
												array(
                                                'name'=>'featured',
                                                'value'=>'Yii::app()->easycode->getFeatured($data->featured)',
                                                'type'=>'raw',
                                                'filter'=>CHtml::dropdownlist("OurTeam[featured]",$model->featured,array("1"=>"Yes","0"=>"No"),array("class"=>'form-control','empty'=>'All')),
                                                ),
													array(
														'name'=>'sort_order',
														'value'=>'$data->sort_order',
														'filter'=>CHtml::textField("OurTeam[sort_order]",$model->sort_order,array("class"=>'form-control')),
													),
													array(
														'class' => 'CButtonColumn',
														'header' => 'Action',
														'template' => '{update}&nbsp;&nbsp;{delete}',
                                                    //'deleteConfirmation' => false,
														'buttons' => array(
															'update' => array(
																'label' => '<span class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></span>',
                                                            //'url'=>'Yii::app()->createUrl("//admin/settings/updateCity/",array("id"=>$data->id))',
																'imageUrl' => false,
																'options' => array('title' => 'Update'),
															),
															'delete' => array(
																'label' => '<span class="btn btn-xs btn-danger"><i class="fa fa-times"></i></span>',
                                                            //'url'=>'Yii::app()->createUrl("//admin/settings/deleteCity/",array("id"=>$data->id))',
																'imageUrl' => false,
																'options' => array(
																	'title' => 'Delete',
                                                                //'class' => 'delConfirm',
                                                                //'data-toggle'=>'confirmation',
																),
															),
														),
													),
												),
											));
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END PAGE CONTENT BODY -->
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->

			</div>
			<!-- END CONTAINER -->
		</div>
	</div>


