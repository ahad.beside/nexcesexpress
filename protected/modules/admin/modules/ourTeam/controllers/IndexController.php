<?php

class IndexController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'index', 'view', 'admin', 'delete'),
                'roles' => array('Admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->pageTitle='New Team Member';
		$data['teamCategory']=TeamCategory::model()->findAll("status=1 order by sort_order");
		$model=new OurTeam;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['OurTeam']))
		{
			$model->attributes=$_POST['OurTeam'];
			$model->slug = Yii::app()->easycode->makeSlug('OurTeam','slug',trim($model->name));
			
			$uploadedFile = CUploadedFile::getInstance($model, "image");
                if ($uploadedFile) {
                    $fileName = Yii::app()->easycode->genFileName($uploadedFile->extensionName);
                    $model->image = $fileName;
                }

            $uploadedFileCover = CUploadedFile::getInstance($model, "cover_photo");
                if ($uploadedFileCover) {
                    $fileNameCover = Yii::app()->easycode->genFileName($uploadedFileCover->extensionName);
                    $model->cover_photo = $fileNameCover;
                }
			if($model->save()){
				if ($uploadedFile) {
				$uploadedFile->saveAs(UPLOAD . Yii::app()->params->teamDir . $fileName);
			}
			if ($uploadedFileCover) {
				$uploadedFileCover->saveAs(UPLOAD . Yii::app()->params->teamDir . $fileNameCover);
			}
				Yii::app()->user->setFlash('success', "Success: Team Member created successfully");
                    $this->redirect(array('index','OurTeam[category_id]'=>'All'));
			}else{
				Yii::app()->user->setFlash('warning', "Warning: Please check the form carefully for errors!");
			}
		}
		$model->sort_order=Yii::app()->easycode->getLastSortingNumber('OurTeam', 'sort_order');
		$this->render('create',array(
			'model'=>$model,
			'data'=>$data,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->pageTitle='Update Team Member';
		$data['teamCategory']=TeamCategory::model()->findAll("status=1 order by sort_order");
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$preImage = $model->image;
		$preCover = $model->cover_photo;
		$preName = $model->name;
		if(isset($_POST['OurTeam']))
		{
			$model->attributes=$_POST['OurTeam'];
			$uploadedFile = CUploadedFile::getInstance($model, "image");
                if ($uploadedFile) {
                    $fileName = Yii::app()->easycode->genFileName($uploadedFile->extensionName);
                    $model->image = $fileName;
                }else{
                	$model->image = $preImage;
                }

            $uploadedFileCover = CUploadedFile::getInstance($model, "cover_photo");
                if ($uploadedFileCover) {
                    $fileNameCover = Yii::app()->easycode->genFileName($uploadedFileCover->extensionName);
                    $model->cover_photo = $fileNameCover;
                }else{
                	$model->cover_photo = $preCover;
                }
                if($preName!=$_POST['OurTeam']['name'])
            		$model->slug = Yii::app()->easycode->makeSlug('OurTeam','slug',trim($model->name));
			if($model->save()){
				if ($uploadedFile) {
				$uploadedFile->saveAs(UPLOAD . Yii::app()->params->teamDir . $fileName);
				Yii::app()->easycode->deleteFile($preImage,'team');
			}
			if ($uploadedFileCover) {
				$uploadedFileCover->saveAs(UPLOAD . Yii::app()->params->teamDir . $fileNameCover);
				Yii::app()->easycode->deleteFile($preCover,'team');
			}
				Yii::app()->user->setFlash('success', "Success: Team Member created successfully");
                    $this->redirect(array('index','OurTeam[category_id]'=>'All'));
			}else{
				Yii::app()->user->setFlash('warning', "Warning: Please check the form carefully for errors!");
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'data'=>$data,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		// $dataProvider=new CActiveDataProvider('OurTeam');
		// $this->render('index',array(
		// 	'dataProvider'=>$dataProvider,
		// ));
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->pageTitle='Team Member';
		$model=new OurTeam('search');
		$data['teamCategory']=TeamCategory::model()->findAll("status=1 order by sort_order");
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['OurTeam']))
			$model->attributes=$_GET['OurTeam'];
		if(isset($_GET['OurTeam']['category_id']) && $_GET['OurTeam']['category_id']!='All')
			$model->category_id=intval($_GET['OurTeam']['category_id']);
		$this->render('admin',array(
			'model'=>$model,
			'data'=>$data,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return OurTeam the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=OurTeam::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param OurTeam $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='our-team-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
