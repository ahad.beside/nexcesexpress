<?php
class IndexController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'index', 'view', 'admin', 'delete'),
                'roles' => array('Admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->pageTitle='New Client';
		$data['clientCategory']=ClientCategory::model()->findAll("status=1 order by sort_order");
		$model=new Client;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Client']))
		{
			$model->attributes=$_POST['Client'];
			$model->slug = Yii::app()->easycode->makeSlug('Client','slug',trim($model->name));
			$model->sort_order=Yii::app()->easycode->getLastSortingNumber('Client', 'sort_order');
			$uploadedFile = CUploadedFile::getInstance($model, "image");
                if ($uploadedFile) {
                    $fileName = Yii::app()->easycode->genFileName($uploadedFile->extensionName);
                    $model->image = $fileName;
                }

			if($model->save()){
				if ($uploadedFile) {
				$uploadedFile->saveAs(UPLOAD . Yii::app()->params->clientDir . $fileName);
				}
				Yii::app()->user->setFlash('success', "Success: Client created successfully");
                    $this->redirect(array('index','Client[category_id]'=>'All'));
			}else{
				Yii::app()->user->setFlash('warning', "Warning: Please check the form carefully for errors!");
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'data'=>$data,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->pageTitle='Update Client';
		$data['clientCategory']=ClientCategory::model()->findAll("status=1 order by sort_order");
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$preImage = $model->image;
		$preName = $model->name;
		if(isset($_POST['Client']))
		{
			$model->attributes=$_POST['Client'];
			$uploadedFile = CUploadedFile::getInstance($model, "image");
                if ($uploadedFile) {
                    $fileName = Yii::app()->easycode->genFileName($uploadedFile->extensionName);
                    $model->image = $fileName;
                }else{
                	$model->image = $preImage;
                }
                if($preName!=$_POST['Client']['name'])
            		$model->slug = Yii::app()->easycode->makeSlug('Client','slug',trim($model->name));
			if($model->save()){
				if ($uploadedFile) {
				$uploadedFile->saveAs(UPLOAD . Yii::app()->params->clientDir . $fileName);
				Yii::app()->easycode->deleteFile($preImage,'client');
				}
				Yii::app()->user->setFlash('success', "Success: Client created successfully");
                    $this->redirect(array('index','Client[category_id]'=>'All'));
			}else{
				Yii::app()->user->setFlash('warning', "Warning: Please check the form carefully for errors!");
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'data'=>$data,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		// $dataProvider=new CActiveDataProvider('OurTeam');
		// $this->render('index',array(
		// 	'dataProvider'=>$dataProvider,
		// ));
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->pageTitle='Client';
		$model=new Client('search');
		$data['clientCategory']=ClientCategory::model()->findAll("status=1 order by sort_order");
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Client']))
			$model->attributes=$_GET['Client'];
		if(isset($_GET['Client']['category_id']) && $_GET['Client']['category_id']!='All')
			$model->category_id=intval($_GET['Client']['category_id']);
		$this->render('admin',array(
			'model'=>$model,
			'data'=>$data,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return OurTeam the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Client::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param OurTeam $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='our-team-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
