<div class="list-group">
    <?php 
    $conId=$_GET['Client']['category_id'];

    if(strtolower($conId)=='all')
        $jobListActive='active';
    else
        $jobListActive='';
    
    ?>

    <?= CHtml::link('All Client',$this->createUrl('//admin/client/index',array('Client[category_id]'=>'All')), array('class'=>'list-group-item '.$jobListActive,'data-tag'=>'genarel')) ?>

    <?php 
    foreach($data['clientCategory'] as $rowCategory):
        if($_GET['Client']['category_id']==$rowCategory->id)
            $teamCategoryActive='active';
        else
            $teamCategoryActive='';
    ?>
    <?= CHtml::link($rowCategory->name,$this->createUrl('//admin/client/index',array('Client[category_id]'=>$rowCategory->id)), array('class'=>'list-group-item '.$teamCategoryActive,'data-tag'=>'genarel')) ?>
<?php endforeach;?>

</div>
<style>
    .countColor{
        color:green;
    }
    .countColorP{
        color:#FF9900;
    }
    .countColorR{
        color:red;
    }
</style>
<script type="text/javascript">
    /*$(document).ready(function(){
        $(".list-group-item").removeClass('active');
        $(".list-group-item[data-tag='"+window.location.hash.substr(1)+"']").addClass('active');
    });*/
</script>