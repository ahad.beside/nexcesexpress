<?php
/**
 * This is the model class for table "our_team".
 *
 * The followings are the available columns in table 'our_team':
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $slug
 * @property string $link
 * @property string $designation
 * @property string $details
 * @property integer $sort_order
 * @property string $fb_url
 * @property string $twitter_url
 * @property string $linkedin_url
 * @property string $web_url
 * @property integer $status
 */
class Partner extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'partner';
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, name, slug, link, details', 'required'),
			array('sort_order, image,featured', 'safe'),
			array('category_id, sort_order, status', 'numerical', 'integerOnly'=>true),
			array('name, slug', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, category_id, name, slug, link, details, sort_order,image, status,featured', 'safe', 'on'=>'search'),
		);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'PartnerCategory', 'category_id'),
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Category',
			'name' => 'Name',
			'slug' => 'Slug',
			'link' => 'Link',
			'details' => 'Details',
			'sort_order' => 'Sort Order',
			'status' => 'Status',
			'image'=>'Image',
			'featured'=>'Featured',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		if($this->category_id=='All')
			$criteria->condition=('category_id!=0');
		else
			$criteria->compare('category_id',$this->category_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('slug',$this->link,true);
		$criteria->compare('details',$this->details,true);
		$criteria->compare('sort_order',$this->sort_order);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('featured',$this->featured);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>Yii::app()->params->pagesize,
			),
		));
	}
	public static function makeLink($id){
        //return Yii::app()->createAbsoluteUrl('//product/' . $id, array('name' => self::model()->findByPk($id)->name));
        return Yii::app()->createAbsoluteUrl('//partner/' . self::model()->findByPk($id)->slug);
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OurTeam the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
