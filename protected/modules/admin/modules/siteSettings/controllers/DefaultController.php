<?php
class DefaultController extends Controller
{
   public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index'),
                'roles' => array('Admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
	public function actionIndex()
	{
		$this->pageTitle = 'Settings';
        if (SiteSettings::model()->exists()) {
            $model = SiteSettings::model()->find();
            $preData = SiteSettings::model()->find();
        } else
            $model = new SiteSettings;

        if (isset($_POST['SiteSettings'])) {
            $model->attributes = $_POST['SiteSettings'];
            //$model->entry_by = Yii::app()->user->userId;
            //$model->entry_time = date('Y-m-d H:i:s');
            //$model->update_by = Yii::app()->user->userId;
            $model->update_time = date('Y-m-d H:i:s');

            $uploadedFile = CUploadedFile::getInstance($model, "logo");
            if ($uploadedFile) {
                $fileName = Yii::app()->easycode->genFileName($uploadedFile->extensionName);
                $model->logo = $fileName;
            } else {
                $model->logo = $preData->logo;
            }

            $uploadedSiteLogo = CUploadedFile::getInstance($model, "site_logo");
            if ($uploadedSiteLogo) {
                $fileNameSiteLogo = Yii::app()->easycode->genFileName($uploadedSiteLogo->extensionName);
                $model->site_logo = $fileNameSiteLogo;
            } else {
                $model->site_logo = $preData->site_logo;
            }

            $uploadedLoginBanner= CUploadedFile::getInstance($model, "login_banner");
            if ($uploadedLoginBanner) {
                $fileNameLoginBanner = Yii::app()->easycode->genFileName($uploadedLoginBanner->extensionName);
                $model->login_banner= $fileNameLoginBanner;
            } else {
                $model->login_banner= $preData->login_banner;          }

            if ($model->save()) {
                if ($uploadedFile) {
                    $uploadedFile->saveAs(UPLOAD . Yii::app()->params->logoDir . $fileName);
                    Yii::app()->session['siteLogo'] = Yii::app()->request->baseUrl . '/upload' . Yii::app()->params->logoDir . $fileName;
                    //delete pre logo
                    Yii::app()->easycode->deleteFile($preData->logo, Yii::app()->params->logoDir);
                }
                if ($uploadedSiteLogo) {
                    $uploadedSiteLogo->saveAs(UPLOAD . Yii::app()->params->logoDir . $fileNameSiteLogo);
                    //Yii::app()->session['siteLogo'] = Yii::app()->request->baseUrl . '/upload' . Yii::app()->params->logoDir . $fileNameSiteLogo;
                    //delete pre logo
                    Yii::app()->easycode->deleteFile($preData->site_logo, Yii::app()->params->logoDir);
                }
                if ($uploadedLoginBanner) {
                    $uploadedLoginBanner->saveAs(UPLOAD . Yii::app()->params->logoDir . $fileNameLoginBanner);
                    //Yii::app()->session['siteLogo'] = Yii::app()->request->baseUrl . '/upload' . Yii::app()->params->logoDir . $fileNameLoginBanner;
                    //delete pre logo
                    Yii::app()->easycode->deleteFile($preData->login_banner, Yii::app()->params->logoDir);
                }

                Yii::app()->session['siteName'] = $model->name;

                if ($model->isNewRecord)
                    Yii::app()->user->setFlash('success', $this->pageTitle . 'created successfully');
                else
                    Yii::app()->user->setFlash('success', $this->pageTitle . 'updated successfully');
                $this->redirect(array('index'));
            } else {
                Yii::app()->user->setFlash('warning','warning');
            }
        }

        $this->render('index', array(
            'model' => $model,
        ));
	}
}