<?php
/* @var $this IndexController */
/* @var $model OurTeam */

$this->breadcrumbs=array(
	'Our Teams'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List OurTeam', 'url'=>array('index')),
	array('label'=>'Create OurTeam', 'url'=>array('create')),
	array('label'=>'Update OurTeam', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete OurTeam', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OurTeam', 'url'=>array('admin')),
);
?>

<h1>View OurTeam #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'category_id',
		'name',
		'slug',
		'designation',
		'details',
		'sort_order',
		'fb_url',
		'twitter_url',
		'linkedin_url',
		'web_url',
		'status',
	),
)); ?>
