<?php
/* @var $this IndexController */
/* @var $model OurTeam */

$this->breadcrumbs=array(
	'Our Teams'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List OurTeam', 'url'=>array('index')),
	array('label'=>'Manage OurTeam', 'url'=>array('admin')),
);
?>

<h1>Create OurTeam</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>