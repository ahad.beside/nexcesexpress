<?php
/* @var $this IndexController */
/* @var $model OurTeam */

$this->breadcrumbs=array(
	'Our Teams'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List OurTeam', 'url'=>array('index')),
	array('label'=>'Create OurTeam', 'url'=>array('create')),
	array('label'=>'View OurTeam', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage OurTeam', 'url'=>array('admin')),
);
?>

<h1>Update OurTeam <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>