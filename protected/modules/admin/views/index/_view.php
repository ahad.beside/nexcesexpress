<?php
/* @var $this IndexController */
/* @var $data OurTeam */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('category_id')); ?>:</b>
	<?php echo CHtml::encode($data->category_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('slug')); ?>:</b>
	<?php echo CHtml::encode($data->slug); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('designation')); ?>:</b>
	<?php echo CHtml::encode($data->designation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('details')); ?>:</b>
	<?php echo CHtml::encode($data->details); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sort_order')); ?>:</b>
	<?php echo CHtml::encode($data->sort_order); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('fb_url')); ?>:</b>
	<?php echo CHtml::encode($data->fb_url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('twitter_url')); ?>:</b>
	<?php echo CHtml::encode($data->twitter_url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('linkedin_url')); ?>:</b>
	<?php echo CHtml::encode($data->linkedin_url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('web_url')); ?>:</b>
	<?php echo CHtml::encode($data->web_url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	*/ ?>

</div>