<?php
/* @var $this IndexController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Our Teams',
);

$this->menu=array(
	array('label'=>'Create OurTeam', 'url'=>array('create')),
	array('label'=>'Manage OurTeam', 'url'=>array('admin')),
);
?>

<h1>Our Teams</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
