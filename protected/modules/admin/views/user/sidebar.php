<script type="text/javascript">
    $(document).ready(function (){
       getCountTotal(); 
    });
    function getCountTotal(){
        $.post('<?php echo Yii::app()->createUrl('//admin/user/getUserCount')?>',function (data){
            data = JSON.parse(data);
            $('.tPendingWriter').html('['+data.writerPending+']');
            $('.tPending').html('['+data.writer+']');
            $('.tApproved').html('['+data.member+']');
        });
    }
</script>
<div class="list-group">
    <?= CHtml::link('Pending Writer <span class="countColor tPendingWriter"></span>',$this->createUrl('//admin/user/index?User[role]=3&User[active]=0'), array('class'=>'list-group-item','data-tag'=>'genarel')) ?>
    
    <?= CHtml::link('Approve Writer <span class="countColor tPending"></span>',$this->createUrl('//admin/user/index?User[role]=3&User[active]=1'), array('class'=>'list-group-item','data-tag'=>'genarel')) ?>
    
    <?= CHtml::link('Member <span class="countColorP tApproved"></span>',$this->createUrl('//admin/user/index?User[role]=4'), array('class'=>'list-group-item','data-tag'=>'genarel')) ?>
    
</div>
<style>
    .countColor{
        color:green;
    }
    .countColorP{
        color:#FF9900;
    }
    .countColorR{
        color:red;
    }
</style>
<script type="text/javascript">
    /*$(document).ready(function(){
        $(".list-group-item").removeClass('active');
        $(".list-group-item[data-tag='"+window.location.hash.substr(1)+"']").addClass('active');
    });*/
</script>