<style>
    .userApproveOrder{
        color:green;
    }
</style>


<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1><?= $this->pageTitle ?></h1>
                        </div>
                        <!-- END PAGE TITLE -->

                        <div class="page-action-buttons">
                            <a class="btn btn-sm btn-primary" href="javascript:void(0)" onclick="javascript:$('.grid-search-portlet').toggle('slow')" title="Search"> <i class="fa fa-search"></i> Search</a>
                            <a class="btn btn-sm btn-primary" href="<?= $this->createUrl('create') ?>" title="Create Menu"> <i class="fa fa-plus"></i> Create</a>
                        </div>
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <?= $this->renderPartial('sidebar',false,true)?>
                            </div>
                            <div class="col-md-9">
                                <?php $this->renderPartial('_search', array('model' => $model)) ?>
                                <div class="portlet box">
                                    <div class="portlet-body" style="display: block;">
                                        
                                        <section class="content_section" id="login_section">
  <div class="relatives">
    <div class="container">
        <div class="login_page" style="width: 700px!important">
<?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'user-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'htmlOptions' => array('class' => 'inner-form', 'role' => 'form', 'enctype' => 'multipart/form-data'),
 ));
?> 
                <div class="form-group" style="display: none;">
                    <label for="exampleInputPassword1">User Type</label><br/>
                <input class="form-group" type="radio" name="User[role]" value="4"> Member
                <input class="form-group" type="radio" name="User[role]" value="3" checked="checked"> Writer
                    <?php echo $form->error($model, 'role'); ?>
              </div>
             <h2>Section 1</h2>   
             <hr>
              
              <div class="form-group">
                        <label for="firstName">Choose your area of Writing</label>
                        <div class="mt-checkbox-list">
                            <?php foreach ($data['category'] as $categoryName): ?>
                    <label class="mt-checkbox">
                        <input type="checkbox" id="inlineCheckbox3" name="cat_id[]" value="<?= $categoryName->id ?>"> <?= $categoryName->name ?>
                        <span></span>
                    </label>

                            <?php endforeach; ?>

                        </div>
                    </div>
                <div class="form-group">
                        <label for="firstName">Full Name</label>
                        <?php echo $form->textField($model, 'first_name', array('class' => 'form-control')); ?> 
                            <?php echo $form->error($model, ' first_name'); ?>
                    </div>
               
                <div class="form-group">
                        <label for="firstName">Email Address</label>
                        <?php echo $form->textField($model, 'email', array('class' => 'form-control')); ?> 
                        <span id="errorEmail" style="color: red;"></span>
                            <?php echo $form->error($model, ' email'); ?>
                    </div>
                     <div class="form-group">
                        <label for="firstName">Password</label>
                        <?php echo $form->passwordField($model, 'password', array('class' => 'form-control')); ?> 
                        <span id="errorEmail" style="color: red;"></span>
                            <?php echo $form->error($model, ' password'); ?>
                    </div>
               
            <?php echo CHtml::submitButton('Create', array('class' => 'btn btn-primary width100')); ?>
              
            <?php $this->endWidget(); ?>
            
        </div>
    </div>
    
  </div>
</section>
<script>
                                        $('#User_email').blur(function () {
                                            //chkUserExistence();
                                            var email = $('#User_email').val().trim();
                                            //alert(email);
                                                $.post('<?= $this->createUrl('//user/checkExistenceOfUser') ?>', {email: email}, function (data) {
                                                    //alert(email);
                                                    if (data == 0) {
                                                        //$('#User_email_em_').show();
                                                        $('#errorEmail').html('Email "' + email + '" has already been taken');
                                                    }
                                                });
                                        });
                                    </script>
                                    
                                    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/../admin71n/assets/global/plugins/select2/css/select2.min.css'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/../admin71n/assets/global/css/components.min.css'); ?>

<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/../admin71n/assets/global/plugins/select2/css/select2-bootstrap.min.css'); ?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/../admin71n/assets/global/scripts/components-select2.min.js', CClientScript::POS_END); ?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/../admin71n/assets/global/plugins/select2/js/select2.full.min.js', CClientScript::POS_END); ?>

<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/../admin71n/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css'); ?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/../admin71n/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js', CClientScript::POS_END); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->
    </div>
</div>