<?php
class OurTeamController extends Controller {

    public $layout = '//layouts/main';

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'umayalsolike','viewTest'),
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('rating','addToWishList','delFromWishList'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionIndex(){
        $this->redirect(array('//teamCategory/all'));
    }

    public function actionView($name){
        $name=urlencode($name);
        $model = OurTeam::model()->find('slug=:slug',array(':slug'=>$name));
        $data['otherMember'] = OurTeam::model()->findAll("id!=".$model->id." and category_id=".$model->category_id);
        $data['categoryName']=$model->category->name;
        $data['catUrl']=TeamCategory::model()->makeLink($model->category_id);
        $this->pageTitle =  $model->name.' - '.Yii::app()->name;
        $this->render('/teamCategory/teamDetails', array(
            'model' => $model,
            'data' => $data,
        ));
    }
}
