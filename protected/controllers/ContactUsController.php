<?php
class ContactUsController extends Controller {
    public $layout = '//layouts/main';
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules(){
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'umayalsolike','viewTest'),
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('rating','addToWishList','delFromWishList'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionIndex(){
        $this->pageTitle='Product -'.Yii::app()->name;
        
        $data['product']=ContactUs::model()->findAll("status=1 order by sort_order");
        $this->render('product',array(
            'data'=>$data
        ));
    }

    public function actionView($name) {
        $this->layout = '//layouts/main_service';
        $name=urlencode($name);
        $model = ContactUs::model()->find('slug=:slug',array(':slug'=>$name));
        $this->pageTitle =  $model->name.' - '.Yii::app()->name;
        $data['id'] = $model->id;
        $data['allContact']=ContactUs::model()->findAll("id!=".$data['id']." and status=1 order by sort_order");
        $this->render('contactDetails', array(
            'model' => $model,
            'data' => $data,
        ));
    }

}
