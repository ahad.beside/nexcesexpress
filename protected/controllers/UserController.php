<?php
class UserController extends Controller {

    public $layout = '//layouts/login';

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'changePassword','changePasswordSuccess', 'personalInfo','dashboard','educationInfo','experienceInfo','appliedList','transactionHistory','complain','getDistrict','getCity','profileView','getUserCategory','profileConfirmed','profileCreate','profileUpdate','review'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('index','forgotPasswordLink','forgotPassword','forgotPasswordSuccess','checkExistenceOfUser'),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    public function actionReview(){
        $model=new Review;
        if($_POST['Review']){
           $model->attributes=$_POST['Review'];
           $model->user_id=Yii::app()->user->userId;
           $model->entry_date=date('Y-m-d');
           $model->status='Pending';
           if($model->save()){
            echo 1;
           }else{
            echo 0;
           }

        }
    }

    public function actionDashboard() {
        $this->pageTitle = 'Dashboard';
        $data['pendingOrder']=Order::model()->count("user_id_fk=".Yii::app()->user->userId." and status='Pending'");
        $data['confirmedOrder']=Order::model()->count("user_id_fk=".Yii::app()->user->userId." and status='Confirmed'");
        $data['canceledOrder']=Order::model()->count("user_id_fk=".Yii::app()->user->userId." and status='Canceled'");
        $data['shippedOrder']=Order::model()->count("user_id_fk=".Yii::app()->user->userId." and status='Shipped'");
        $this->render('dashboard', array('data'=>$data));
    }
    public function actionProfileCreate() {
        $this->pageTitle = 'New Profile';
        $model=new Profile;
        $userModel=User::model()->findByPk(Yii::app()->user->userId);
        if($_POST['Profile']){
            $model->attributes=$_POST['Profile'];
            $model->user_id = Yii::app()->user->userId;
            $model->birth_date = date('Y-m-d',strtotime($_POST['Profile']['birth_date']));
            $model->first_name = $_POST['User']['first_name'];
            $model->last_name = $_POST['User']['last_name'];
            $uploadedFile = CUploadedFile::getInstance($model, "photo");
                if ($uploadedFile) {
                    $fileName = Yii::app()->easycode->genFileName($uploadedFile->extensionName);
                    $model->photo = $fileName;
                }
            if($model->save()){
                User::model()->updateByPk(Yii::app()->user->userId,array('first_name'=>$_POST['User']['first_name'],'last_name'=>$_POST['User']['last_name']));
                if($uploadedFile)
                    $uploadedFile->saveAs(UPLOAD . Yii::app()->params->userDir . $fileName);
                $this->redirect(array('dashboard'));
            }else{
                print_r($model->getErrors());exit;
            }
        }
        $this->render('profile', array('model'=>$model,'userModel'=>$userModel));
    }

    public function actionProfileUpdate($id) {
        $this->pageTitle = 'Update Profile';
        $model=Profile::model()->findByPk($id);
        $preData=Profile::model()->findByPk($id);
        //$userModel=User::model()->findByPk(Yii::app()->user->userId);
        if($_POST['Profile']){
            $model->attributes=$_POST['Profile'];
            $model->user_id = Yii::app()->user->userId;
            $model->birth_date = date('Y-m-d',strtotime($_POST['Profile']['birth_date']));
            $uploadedFile = CUploadedFile::getInstance($model, "photo");
                if ($uploadedFile) {
                    $fileName = Yii::app()->easycode->genFileName($uploadedFile->extensionName);
                    $model->photo = $fileName;
                }else{
                    $model->photo = $preData->photo;
                }
            if($model->save()){
                User::model()->updateByPk(Yii::app()->user->userId,array('first_name'=>$_POST['Profile']['first_name'],'last_name'=>$_POST['Profile']['last_name']));
                if($uploadedFile){
                    $uploadedFile->saveAs(UPLOAD . Yii::app()->params->userDir . $fileName);
                    Yii::app()->easycode->deleteFile($preData->photo,'user');
                }
                $this->redirect(array('dashboard'));
            }else{
                //print_r($model->getErrors());exit;
            }
        }
        $this->render('profile', array('model'=>$model,'userModel'=>$userModel));
    }
    public function actionCheckExistenceOfUser(){
        if ($_POST['email']) {
            $email = Yii::app()->easycode->safeReadFrom($_POST['email']);
            if (User::model()->exists('email=:email', array(':email' => $email))) {
                echo 0;
            } else {
                echo 1;
            }
        }
    }

    public function actionProfileView(){
        $this->pageTitle='Personal Details';
        $data['personal']=Profile::model()->find("user_id=".Yii::app()->user->userId);
        $modelEdu=new Education('search');
        $modelEdu->unsetAttributes();  // clear any default values
        if(isset($_GET['Education']))
            $modelEdu->attributes=$_GET['Education'];
            $modelEdu->user_id=Yii::app()->user->userId;
        $modelExp=new Experience('search');
        $modelExp->unsetAttributes();  // clear any default values
        if(isset($_GET['Experience']))
            $modelExp->attributes=$_GET['Experience'];
            $modelExp->user_id=Yii::app()->user->userId;
        //print_r($data['personal']);
        $this->render('personalDetails',array('data'=>$data,'modelEdu'=>$modelEdu,'modelExp'=>$modelExp));
    }
 
    public function sendForgotPasswordMail($email) {
        $model = User::model()->find('email=:email',array(':email'=>$email));
        if ($model->email != '') {
            if($model->first_name=='' && $model->last_name==''){
                $name = 'User';
            }else{
                $name = $model->first_name.' '.$model->last_name;
            }
            
            $MailSettings = SiteSettings::model()->find();
            
            $mail = new YiiMailer('forgotPassword', array('name' => $name, 'email'=>md5(Yii::app()->params->md5Key.$model->email)));
            $mail->setLayout('mail');
            $mail->setFrom($MailSettings->email, $MailSettings->name);
            $mail->setSubject('Forgot Password');
            $mail->setTo($model->email);
            $mail->send();
        }
    }
    
    public function actionForgotPasswordLink(){
        if(isset($_POST['email'])){
            $data = array();
            $email = trim($_POST['email']);
            if(User::model()->exists('email=:email',array(':email'=>$email))){
                User::model()->updateAll(array('forgot_pass_code'=>md5(Yii::app()->params->md5Key.$email)),'email=:email',array(':email'=>$email));
                $this->sendForgotPasswordMail($email);
                $data['msg']='Please check your mail to get forgot password link.';
                $data['status']=1;
            }else{
                $data['msg']='Your have given wrong email address';
                $data['status']=0;
            }
            echo json_encode($data);
        }
    }

    public function actionForgotPasswordSuccess(){
        $this->pageTitle =  'Forgot Password';
        $this->render('changePasswordSuccess');
    }

    public function actionForgotPassword(){
        
        $this->pageTitle =  'Forgot Password ';
        if (isset($_POST['Password'])) {
            $post = $_POST['Password'];

            if ($post['new'] == '' or $post['re'] == '') {
                Yii::app()->user->setFlash('error', "All filed must be filled!");
                $this->redirect(array('forgotPassword','link'=> $post['forgot_pass_code']));
            }
            if ($post['new'] != $post['re']) {
                Yii::app()->user->setFlash('error', "New password and re-password not matched!");
                $this->redirect(array('forgotPassword','link'=> $post['forgot_pass_code']));
            }
            if (User::model()->exists('forgot_pass_code=:link', array(':link' => $post['forgot_pass_code']))) {
                $data = User::model()->find('forgot_pass_code=:link', array(':link' => $post['forgot_pass_code']));
                User::model()->updateByPk($data->id,array('password' => md5($post['new']),'forgot_pass_code'=>''));
                Yii::app()->user->setFlash('success', "Password changed successfully");
               // $this->sendChangePasswordMail($data->id);//chng password mail
                //$this->redirect(array('//site/login'));
                $this->redirect(array('forgotPasswordSuccess'));
            } else {
                Yii::app()->user->setFlash('error', "Invalid Request!");
                $this->redirect(array('forgotPassword','link'=> $post['forgot_pass_code']));
            }
        }
        $this->render('forgotPassword');
    }
    public function sendChangePasswordMail($id) {
        $model = User::model()->findByPk($id);
        if ($model->email != '') {
            
            if($model->first_name=='' && $model->last_name==''){
                $name = 'User';
            }else{
                $name = $model->first_name.' '.$model->last_name;
            }
            
            $MailSettings = SiteSettings::model()->find();
            
            $mail = new YiiMailer('changePassword', array('name' => $name, 'email'=>$model->email));
            $mail->setLayout('mail');
            $mail->setFrom($MailSettings->email, $MailSettings->name);
            $mail->setSubject('Change Password');
            $mail->setTo($model->email);
            $mail->send();
        }
    }
    public function actionChangePasswordSuccess(){
        $this->pageTitle =  'Change Password';
        $this->render('changePasswordSuccess');
    }

    public function actionChangePassword(){
        $this->pageTitle =  'Change Password';
        if (isset($_POST['Password'])) {
            $post = $_POST['Password'];

            if ($post['current'] == '' or $post['new'] == '' or $post['re'] == '') {
                Yii::app()->user->setFlash('error', "All filed must be filled!");
                $this->redirect(array('changePassword'));
            }

            if ($post['new'] != $post['re']) {
                Yii::app()->user->setFlash('error', "New password and re-password not matched!");
                $this->redirect(array('changePassword'));
            }

            if (User::model()->exists('id=:id and password=:pass', array(':id' => Yii::app()->user->userId, ':pass' => md5($post['current'])))) {
                User::model()->updateByPk(Yii::app()->user->userId, array('password' => md5($post['new'])));
                Yii::app()->user->setFlash('success', "Password changed successfully");
                //$this->sendChangePasswordMail(Yii::app()->user->userId);//chng password mail
                $this->redirect(array('changePasswordSuccess'));
                //$this->render('changePasswordSuccess');
            } else {
                Yii::app()->user->setFlash('error', "Invalid current password!");
                $this->redirect(array('changePassword'));
            }
        }
        $this->render('changePassword');
    }
}