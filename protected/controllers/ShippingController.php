<?php
class ShippingController extends Controller {
    public $layout = '//layouts/main';
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules(){
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'umayalsolike','viewTest','create','onlineBooking','printBooking','print','downloadBooking','sendInvoice'),
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('rating','addToWishList','delFromWishList'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionOnlineBooking() {
        $this->layout = '//layouts/main_service';

        $model = new BookDetails;
        $title="Online Booking";
        $this->pageTitle =  $title.' - '.Yii::app()->name;

        $code='NX';     
        $challan_posted=BookDetails::model()->findAll();
        $challan_count=count($challan_posted);
        $data['challan_number']=$code.date("Ymd").$challan_count;
        $model->nex_code=$data['challan_number'];

        if(isset($_POST['BookDetails']))
        {
            $model->attributes=$_POST['BookDetails'];
            if (isset(Yii::app()->user->userId) && Yii::app()->user->userId !='') 
                $model->entry_by=Yii::app()->user->userId;
            else
                $model->entry_by=0;
            date_default_timezone_set('Asia/Dhaka');
            $model->entry_date=date('Y-m-d H:i:s');

            if($model->save()){
                Yii::app()->user->setFlash('success', "Success: Challan created.");
                //$model->sendRegistrationSuccessMail($model->id);

                $this->redirect(array('shipping/sendInvoice/'.$model->id)); 
                $this->redirect(array('shipping/printBooking/'.$model->id));
                return;
            }
            else{
              //print_r($model->getErrors());exit;
              Yii::app()->user->setFlash('error', "Incorrect Online Order");
            }
        }
        $this->render('bookDetails', array(
            'model' => $model,
            'data' => $data,
        ));

    }
    public function actionPrintBooking($id)
    {
        $this->layout = '//layouts/main_service';
        $cid = $id;
        $data['bookingInfo']=BookDetails::model()->find('id=:cid', [':cid'=>$cid]);        
        $this->pageTitle='Print Booking';
        $this->render('printBooking',array(
            'data'=>$data,
        ));
    }

    public function actionPrint($id) 
    {
        $this->layout = '//layouts/blank';
        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4', 0, '', 5, 5, 5, 5);

        /* if (isset($_GET['from']) && $_GET['from'] == 'view_invoice') 
        {
          $printCount = TempOrders::model()->findByPk($id)->print_count;
          TempOrders::model()->updateByPk($id, array('print_count' => ((int) $printCount + 1)));
          if ($printCount > 1) 
          {
            $mPDF1->SetWatermarkText('DUPLICATE');
            $mPDF1->showWatermarkText = true;
            $mPDF1->watermarkTextAlpha = 0.1;
          }
        } */

        if (isset($id) && $id != '') 
        {
            $model = BookDetails::model()->findByPk($id);
            $mPDF1->WriteHTML($this->render('print', array('bookDetails' => $model), true));
            $fileName = "BookDetails #" . $model->nex_code . ".pdf";

            //$mPDF1->Output($fileName, 'D');
            $mPDF1->Output($fileName, 'I');

            /* $this->render('view', array(
              'model' => $this->loadModel($id),
              'print_button' => false,
              'admin_button' => false,
              'print' => true,
              )); */
        }
    }

    public function actionDownloadBooking($id) 
    {
        //$id = $_GET['id'];
        $this->pageTitle = 'Booking View - ' . Yii::app()->name;
        $this->layout = '//layouts/flash';

        $booking = BookDetails::model()->find("id='" . $id . "'");
        if (count($booking) < 1)
            throw new CHttpException(404, 'You are not permitted to do this');

        //$this->render('invoicepdf', array('order' => $order));        
        //$this->render('invoicepdf', array('order' => $order));exit();

        $bookingInfo = BookDetails::model()->findByPk($id);
        $userInfo = User::model()->findByPk($orderInfo->user_id_fk);

        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4', 0, '', 5, 5, 5, 5);

        $mPDF1->WriteHTML($this->renderPartial('print', array('bookDetails' => $booking), true));
        //$fileName = $booking->nex_code . ".pdf";
        $fileName = $bookingInfo->nex_code . ".pdf";
        $path = INVOICE . '/' . $fileName;
        $mPDF1->Output($fileName, 'D');
    }

    public function actionSendInvoice($id) 
    {
        //$id = $_GET['id'];

        $this->pageTitle = 'Order View - ' . Yii::app()->name;
        $this->layout = '//layouts/flash';

        $booking = BookDetails::model()->find("id='" . $id . "'");

        if (count($booking) < 1)
            throw new CHttpException(404, 'You are not permitted to do this');

        $bookingInfo = BookDetails::model()->findByPk($id);
        //$userInfo = User::model()->findByPk($orderInfo->user_id_fk);

        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4', 0, '', 5, 5, 5, 5);

        $mPDF1->WriteHTML($this->renderPartial('print', array('bookDetails' => $booking), true));
        $fileName = $bookingInfo->nex_code . ".pdf";
        $path = INVOICE . '/' . $fileName;
        $mPDF1->Output($path, 'F');

        $storeMailSettings = StoreMailSettings::model()->find('store_id=:storeid', array(':storeid' => STORE_ID));
        $storeSettings = StoreSettings::model()->findByPk(STORE_ID);
        $button = "<a target='_blank' href='https://www.uscraft.com/payment/payNow/order/success/id/$order->id'><b>Pay</b></a>";
        $text = <<<TEXT
                <strong>Dear Customer,</strong><br>
                Please find the attached Booking of Order Track# $bookingInfo->nex_code <br><br>
                For payment please click here $button<br>
                Thanks,<br>
                USCraft.
TEXT;
        $subject = 'Booking of Order Track# ' . $bookingInfo->nex_code . ' | SGD ' . $bookingInfo->billing_weight;

        $mail = new YiiMailer('withHtml', array('bookDetails' => $booking));
        $mail->setLayout('mail');
        $mail->setFrom($storeMailSettings->mailer_mail, $storeMailSettings->mailer_name);
        $mail->setSubject($subject . ' - ' . $storeSettings->store_name);
        $mail->setAttachment($path);
        $mail->setTo($userInfo->email);
        $mail->setCc($storeMailSettings->mailer_mail);

        $MailSettings = SiteSettings::model()->find();
        
        $model = self::model()->findByPk($id);
        $mail = new YiiMailer('withBooking', array('code' => $model->verification_code,'firstName'=>$model->first_name,'model'=>$model));
        $mail->setLayout('mail');
        
        $mail->setFrom($MailSettings->email, $MailSettings->name);
        //$mail->setFrom('info@99handicraft.com', 'Iam99');
        $mail->setSubject('New Customer registration - '.$MailSettings->name);
        $mail->setTo($model->email);
        $mail->send();
        if ($mail->send()) {
            echo 1;
        } else {
            echo 0;
        }
//        $this->render('invoicepdf', array(
//            'order' => $order,
//        ));
    }

    public function actionIndex(){
        $this->pageTitle='Product -'.Yii::app()->name;
        
        $data['Shipping']=Shipping::model()->findAll("status=1 order by sort_order");
        $this->render('product',array(
            'data'=>$data
        ));
    }

}
