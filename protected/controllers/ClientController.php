<?php
class ClientController extends Controller {

    public $layout = '//layouts/main';

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'umayalsolike','viewTest'),
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('rating','addToWishList','delFromWishList'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionIndex(){
        $this->redirect(array('//clientCategory/all'));
    }

    public function actionView($name){
        $name=urlencode($name);
        $model = Client::model()->find('slug=:slug',array(':slug'=>$name));
        $this->pageTitle =  $model->name.' - '.Yii::app()->name;
        $this->render('/clientCategory/clientDetails', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    

    public function loadModel($id) {
        $model = Products::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    

}
