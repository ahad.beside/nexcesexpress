<?php

/**
 * This is the model class for table "order_details".
 *
 * The followings are the available columns in table 'order_details':
 * @property integer $id
 * @property integer $shipping_id
 * @property integer $contact_id
 * @property integer $order_type
 * @property string $account_number
 * @property string $company_name
 * @property string $company_location
 * @property string $collection_address
 * @property string $delivery_address
 * @property string $ready_datetime
 * @property string $arrival_datetime
 * @property integer $value
 * @property integer $weight
 * @property string $dimensions
 * @property integer $number_of_pieces
 * @property string $email
 * @property string $contact_number
 * @property string $note
 * @property integer $status
 * @property integer $entry_by
 * @property string $entry_date
 * @property integer $update_by
 * @property string $update_time
 */
class OrderDetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('shipping_id, contact_id, order_type, account_number, company_name, company_location, collection_address, delivery_address, value, weight, dimensions, number_of_pieces, email, contact_number, note, entry_by', 'required'),
			array('contact_id, value, weight, number_of_pieces, status, entry_by, update_by', 'numerical', 'integerOnly'=>true),
			array('account_number, company_location, dimensions, email, contact_number', 'length', 'max'=>50),
			array('company_name', 'length', 'max'=>100),
			array('ready_datetime, arrival_datetime, update_time', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, shipping_id, contact_id, order_type, account_number, company_name, company_location, collection_address, delivery_address, ready_datetime, arrival_datetime, value, weight, dimensions, number_of_pieces, email, contact_number, note, status, entry_by, entry_date, update_by, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'shipping_id' => 'Shipping',
			'contact_id' => 'Contact',
			'order_type' => 'Order',
			'account_number' => 'Account Number',
			'company_name' => 'Company Name',
			'company_location' => 'Company Location',
			'collection_address' => 'Collection Address',
			'delivery_address' => 'Delivery Address',
			'ready_datetime' => 'Ready Datetime',
			'arrival_datetime' => 'Arrival Datetime',
			'value' => 'Value',
			'weight' => 'Weight',
			'dimensions' => 'Dimensions',
			'number_of_pieces' => 'Number Of Pieces',
			'email' => 'Email',
			'contact_number' => 'Contact Number',
			'note' => 'Note',
			'status' => 'Status',
			'entry_by' => 'Entry By',
			'entry_date' => 'Entry Date',
			'update_by' => 'Update By',
			'update_time' => 'Update Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('shipping_id',$this->shipping_id);
		$criteria->compare('contact_id',$this->contact_id);
		$criteria->compare('order_type',$this->order_type);
		$criteria->compare('account_number',$this->account_number,true);
		$criteria->compare('company_name',$this->company_name,true);
		$criteria->compare('company_location',$this->company_location,true);
		$criteria->compare('collection_address',$this->collection_address,true);
		$criteria->compare('delivery_address',$this->delivery_address,true);
		$criteria->compare('ready_datetime',$this->ready_datetime,true);
		$criteria->compare('arrival_datetime',$this->arrival_datetime,true);
		$criteria->compare('value',$this->value);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('dimensions',$this->dimensions,true);
		$criteria->compare('number_of_pieces',$this->number_of_pieces);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('contact_number',$this->contact_number,true);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('entry_by',$this->entry_by);
		$criteria->compare('entry_date',$this->entry_date,true);
		$criteria->compare('update_by',$this->update_by);
		$criteria->compare('update_time',$this->update_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
