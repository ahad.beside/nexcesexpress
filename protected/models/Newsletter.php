<?php
/**
 * This is the model class for table "accounts_head".
 *
 * The followings are the available columns in table 'accounts_head':
 * @property integer $id
 * @property string $name
 * @property integer $entry_by
 * @property string $entry_time
 * @property integer $update_by
 * @property string $update_time
 *
 * The followings are the available model relations:
 * @property AccountsSubHead[] $accountsSubHeads
 */
class Newsletter extends CFormModel
{

	public $subject,$message,$client,$sender_name,$sender_email,$client_type,$custom_client;


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subject, message, client,sender_name,sender_email,custom_client', 'required'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'subject' => 'Subject',
			'message' => 'Message',
			'client' => 'Client',
			'client_type' => 'Client Type',
			'sender_name' => 'Sender Name',
			'sender_email' => 'Sender Email'
		);
	}
}
