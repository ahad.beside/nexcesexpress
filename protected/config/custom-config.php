<?php
return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'import' => array(
        'application.extensions.easyPaypal.*',
        'application.models.*',
        'application.modules.admin.modules.slider.models.*',
        'application.modules.admin.modules.siteSettings.models.*',
        'application.modules.admin.modules.menu.models.*',
        'application.modules.admin.modules.page.models.*',
        'application.modules.admin.modules.ourTeam.models.*',
        'application.modules.admin.modules.portfolio.models.*',
        'application.modules.admin.modules.service.models.*',
        'application.modules.admin.modules.contact.models.*',
        'application.modules.admin.modules.shipping.models.*',
        'application.modules.admin.modules.client.models.*',
        'application.modules.admin.modules.partner.models.*',
        'application.modules.admin.modules.newsEvents.models.*',
        'application.components.*',
        'ext.YiiMailer.YiiMailer',
    	),
    	'modules' => array(
        'admin' => array(
            'modules' => array(
                'slider',
                'siteSettings' =>[
                    'defaultController' => 'default',
                ],
                'menu' =>[
                    'defaultController' => 'index',
                ],
                'page' =>[
                    'defaultController' => 'index',
                ],
                'portfolio' =>[
                    'defaultController' => 'index',
                ],
                'ourTeam' =>[
                    'defaultController' => 'index',
                ],
                'service' =>[
                    'defaultController' => 'index',
                ],
                'contact' =>[
                    'defaultController' => 'index',
                ],
                'shipping' =>[
                    'defaultController' => 'index',
                ],
                'client' =>[
                    'defaultController' => 'index',
                ],
                'partner' =>[
                    'defaultController' => 'index',
                ],
                'newsEvents' =>[
                    'defaultController' => 'index',
                ],
            ),
            'defaultController' => 'dashboard',
        ),
    	),
		'components'=>array(
			'fixture'=>array(
				'class'=>'system.test.CDbFixtureManager',
			),
			/* uncomment the following to provide test database connection
			'db'=>array(
				'connectionString'=>'DSN for test database',
			),
			*/
			'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'caseSensitive' => true,
            'urlSuffix' => '.html',
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => array('<controller>/view'),

                '<controller:\w+>/<id:\d+>' => array('<controller>/view'),
                '<controller:\w+>/<action:\w+>/<id:\d+>' => array('<controller>/<action>'),
                '<controller:\w+>/<action:\w+>' => array('<controller>/<action>'),

                'contact/<action:\w+>/<name>' => 'contact/<action>',
                'contact/<action:\w+>' => 'contact/<action>',
                'contact/<name>' => 'contact/view',

                'shipping/<action:\w+>/<name>' => 'shipping/<action>',
                'shipping/<action:\w+>' => 'shipping/<action>',
                'shipping/<name>' => 'shipping/view',

                'testimonial/<action:\w+>/<name>' => 'testimonial/<action>',
                'testimonial/<action:\w+>' => 'testimonial/<action>',
                'testimonial/<name>' => 'testimonial/view',

                'service/<action:\w+>/<name>' => 'service/<action>',
                'service/<action:\w+>' => 'service/<action>',
                'service/<name>' => 'service/view',

                'category/<action:\w+>/<name>' => 'category/<action>',
                'category/<action:\w+>' => 'category/<action>',
                'category/<name>' => 'category/view',
                
                'serviceCategory/<action:\w+>/<name>' => 'serviceCategory/<action>',
                'serviceCategory/<action:\w+>' => 'serviceCategory/<action>',
                'serviceCategory/<name>' => 'serviceCategory/view',

                'client/<action:\w+>/<name>' => 'client/<action>',
                'client/<action:\w+>' => 'client/<action>',
                'client/<name>' => 'client/view',
                
                'clientCategory/<action:\w+>/<name>' => 'clientCategory/<action>',
                'clientCategory/<action:\w+>' => 'clientCategory/<action>',
                'clientCategory/<name>' => 'clientCategory/view',

                'ourTeam/<action:\w+>/<name>' => 'ourTeam/<action>',
                'ourTeam/<action:\w+>' => 'ourTeam/<action>',
                'ourTeam/<name>' => 'ourTeam/view',
                
                'teamCategory/<action:\w+>/<name>' => 'teamCategory/<action>',
                'teamCategory/<action:\w+>' => 'teamCategory/<action>',
                'teamCategory/<name>' => 'teamCategory/view',


                'newsEvents/<action:\w+>/<name>' => 'newsEvents/<action>',
                'newsEvents/<action:\w+>' => 'newsEvents/<action>',
                'newsEvents/<name>' => 'newsEvents/view',

                'page/<action:\w>' => 'page/<action>',
                'page/<name>' => 'page/view',
                'cart/<action:\w+>/<id:\d+>' => 'cart/<action>',
                'cart/<action:\w+>' => 'cart/<action>',
                'cart/<name>' => 'cart/view',
                'site/<action:\w+>' => 'site/<action>',
                'site/<name>' => 'site/view',
                'user/<action:\w+>' => 'user/<action>',
                'user/<name>' => 'user/view',
                '<module:\w+>/<controller:\w+>/<id:\d+>' => array('<module>/<controller>/view'),
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => array('<module>/<controller>/<action>'),
                '<module:\w+>/<controller:\w+>/<action:\w+>' => array('<module>/<controller>/<action>'),
            //'<controller:\w+>/<id:\d+>' => '<controller>/view',
            //'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
            //'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            //'<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
            ),
        ),
		),
		'params' => array(
        'SERVER_HOST' => 'http://nexcesexpress.com/projects/nexpress/',
        'SERVER_HOSTS' => 'http://nexcesexpress.com/',
        'SITE_URL' => 'http://nexcesexpress.com',
        'companyLogo' => '/images/logo.png',
        'decimalPoint'=>2,
        'md5Key' => 2441139,
        'logoDir' => '/logo/',
        'albumDir' => '/album/',
        'categoryDir' => '/category/',
        'serviceCategoryDir' => '/serviceCategory/',
        'userDir' => '/user/',
        'productDir' => '/product/',
        'serviceDir' => '/service/',
        'sliderDir' => '/slider/',
        'cvDir'=>'/cv/',
        'teamDir'=>'/team/',
        'newsDir'=>'/news/',
        'clientDir'=>'/client/',
        'videoImg'=>'/videoImage/',
        'networkDir'=>'/network/',
        'advertisementDir'=>'/advertisement/',
        'pageDir'=>'/page/',
        'bestImgSize'=>'(1000px X 800px) or same ratio is recommended for image',
        'bestImgSizeBanner'=>'(1000px X 800px) or same ratio is recommended for image',
        'serviceImgSize'=>'(358px X 209px) or same ratio is recommended for image',
        'clientImgSize'=>'Any Size',
        'teamImgSize'=>'(260px X 330px) or same ratio is recommended for image',
        'teamCoverImgSize'=>'(1350px X 405px) or same ratio is recommended for image',
        'galleryImgSize'=>'Any Size',
        'latestNewsImgSize'=>'Any Size',
        'albumImgSize'=>'Any Size',
        'sliderImgSize'=>'Any Size',
        'pagesize'=>'50',
    	),
	)
);
