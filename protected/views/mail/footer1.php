<!-- module 7 -->
<table data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
<tr>
<td data-bgcolor="bg-module" bgcolor="#eaeced">
<table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
  <tr>
    <td class="footer" style="padding:0 0 10px;">
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr class="table-holder">
          <th class="tfoot" width="400" align="left" style="vertical-align:top; padding:0;">
            <table width="100%" cellpadding="0" cellspacing="0">
              <tr>
                <td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px;">
                  Copyright &copy; <?=date('Y');?> &nbsp; <?= Yii::app()->name;?> All Rights Reserved
                </td>
              </tr>
            </table>
          </th>
          <th class="thead" width="200" align="left" style="vertical-align:top; padding:0;">
            <!-- <table class="center" align="right" cellpadding="0" cellspacing="0">
              <tr>
                <td class="btn" valign="top" style="line-height:0; padding:3px 0 0;">
                  <a target="_blank" style="text-decoration:none;" href="#"><img src="images/ico-facebook.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="6" height="13" alt="fb" /></a>
                </td>
                <td width="20"></td>
                <td class="btn" valign="top" style="line-height:0; padding:3px 0 0;">
                  <a target="_blank" style="text-decoration:none;" href="#"><img src="images/ico-twitter.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="13" height="11" alt="tw" /></a>
                </td>
                <td width="19"></td>
                <td class="btn" valign="top" style="line-height:0; padding:3px 0 0;">
                  <a target="_blank" style="text-decoration:none;" href="#"><img src="images/ico-google-plus.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="19" height="15" alt="g+" /></a>
                </td>
                <td width="20"></td>
                <td class="btn" valign="top" style="line-height:0; padding:3px 0 0;">
                  <a target="_blank" style="text-decoration:none;" href="#"><img src="images/ico-linkedin.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="13" height="11" alt="in" /></a>
                </td>
              </tr>
            </table> -->
          </th>
        </tr>
      </table>
    </td>
  </tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<!-- fix for gmail -->
<tr>
<td style="line-height:0;"><div style="display:none; white-space:nowrap; font:15px/1px courier;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td>
</tr>
</table>
</body>
</html>