
<table width="100%">
	<tbody>
		<tr>
			<td width="50%">
				<table width="100%">
					<tbody>
						<tr>
							<td colspan="2"><b>Shipper Details</b></td>
						</tr>
						<tr>
							<td>Attn</td>
							<td>CHECK,</td>
						</tr>
						<tr>
							<td>Company</td>
							<td> CHECK,</td>
						</tr>
						<tr>
							<td valign="top">Address</td>
							<td>CHECKCHECKCHECK</td>
						</tr>
						<tr>
							<td>City</td>
							<td>DHAKA,</td>
						</tr>
						<tr>
							<td>Zip</td>
							<td>1209,</td>
						</tr>
						<tr>
							<td>Country</td>
							<td>BANGLADESH,</td>
						</tr>
						<tr>
							<td>Contact No</td>
							<td>01912345678,</td>
						</tr>
						<tr>
							<td>Email</td>
							<td>
								<a href="mailto:bemegroupint@gmail.com" target="_blank">bemegroupint@gmail.com</a>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="50%">
				<table width="100%">
					<tbody>
						<tr>
							<td colspan="2"><b>Consignee Details</b></td>
						</tr>
						<tr>
							<td>Attn</td>
							<td>CHECK,</td>
						</tr>
						<tr>
							<td>Company</td>
							<td>CHECK,</td>
						</tr>
						<tr>
							<td>Address</td>
							<td valign="top">CHECKCHECKCHECK</td>
						</tr>
						<tr>
							<td>City</td>
							<td>DHAKA,</td>
						</tr>
						<tr>
							<td>Zip</td>
							<td>1209,</td>
						</tr>
						<tr>
							<td>Country</td>
							<td>BANGLADESH,</td>
						</tr>
						<tr>
							<td>Contact No</td>
							<td>01912345678,</td>
						</tr>
						<tr>
							<td>Email</td>
							<td>01912345678</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2"><b>Consignment Details</b></td>
		</tr>
		<tr>
			<td width="50%">
				<table width="100%">
					<tbody>
						<tr>
							<td>Items</td>
							<td>SPX </td>
						</tr>
						<tr>
							<td>Weight</td>
							<td>11.11 KG</td>
						</tr>
						<tr>
							<td>Shipper Ref</td>
							<td>AHAD</td>
						</tr>
						<tr>
							<td>Shipment Mode</td>
							<td>pp</td>
						</tr>
						<tr>
							<td>Remarks</td>
							<td>AS</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="50%">
				<table width="100%">
					<tbody>
						<tr>
							<td>Content</td>
							<td>AS</td>
						</tr>
						<tr>
							<td>Dimention</td>
							<td>11 X 11 X 11 = 0.27 KG</td>
						</tr>
						<tr>
							<td>Pcs</td>
							<td>1 Pcs</td>
						</tr>
						<tr>
							<td>Booking Date</td>
							<td>2019-09-18</td>
						</tr>
						<tr>
							<td>Value</td>
							<td>0 USD</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
	